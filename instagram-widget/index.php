<?php
  include dirname(__FILE__).'/settings.php';
  $cache_folder = dirname(__FILE__).'/cache';
  $cache_user = $cache_folder.'/'.$user_name;
  $cache_file = $cache_user.'/cache.json';
  function create_url($string) {
    if (substr(substr($string, strlen($_SERVER['DOCUMENT_ROOT'])), 0, 1) != '/') {
      $res = '/'.substr($string, strlen($_SERVER['DOCUMENT_ROOT']));
    } else {
      $res = substr($string, strlen($_SERVER['DOCUMENT_ROOT']));
    }
    return $res;
  }
  if (file_exists($cache_file) && (filemtime($cache_file) > (time() - (60 * 60 * 24) ))) {
     $info = file_get_contents($cache_file);
  } else {
    file_get_contents('https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token='.$token);
    $get = file_get_contents('https://graph.instagram.com/me/media?fields=permalink,media_type,media_url,thumbnail_url,caption&access_token='.$token);
    if (!file_exists($cache_folder)) {
       mkdir($cache_folder);
     }
     if (!file_exists($cache_user)) {
       mkdir($cache_user);
     }
     $get_arr = json_decode($get);
     foreach ($get_arr->data as $elem) {
       if ($elem->media_type !== 'VIDEO') {
         $media_name = substr(basename($elem->media_url), 0, strpos(basename($elem->media_url), '?'));
         if (!file_exists($cache_user.'/'.$media_name)) {
           $media = file_get_contents($elem->media_url);
           file_put_contents($cache_user.'/'.$media_name, $media);
         }
         $elem->media_url = create_url($cache_user.'/'.$media_name);
       } else {
         $thumb_name = substr(basename($elem->thumbnail_url), 0, strpos(basename($elem->thumbnail_url), '?'));
         if (!file_exists($cache_user.'/'.$thumb_name)) {
           $thumb = file_get_contents($elem->thumbnail_url);
           file_put_contents($cache_user.'/'.$thumb_name, $thumb);
         }
         $elem->thumbnail_url = create_url($cache_user.'/'.$thumb_name);
       }
     }
     $info = json_encode($get_arr);
     file_put_contents($cache_file, $info);
  }
  if (isset($_GET['get_user_name'])){
    echo $user_name;
  } else {
    echo $info;
  }
?>
