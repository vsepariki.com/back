<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Салон париков в Спб");
$APPLICATION->SetPageProperty("description", "В нашем салоне париков около 200 моделей в наличии, доставим по России. Продаем парики европейского качества, неотличить от своих волос, надежная фиксация. Подберем стиль в отдельной примерочной.");
$APPLICATION->SetTitle("Салон париков");
?><div class="block-wrap block-wrap_wrap">

	<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
		<p>Центр дизайна волос — интернет-магазин и бутик париков. Салон элитных париков создали в 2002 году, выполняя миссию: помочь людям с облысением, шрамами, повреждениями кожи головы вернуть естественный вид, объем волос незаметно для окружающих. Также продаем модели париков для смены имиджа и маскировки неудачной стрижки.</p>
		<h2>Что мы предлагаем</h2>
		<p>Основа ассортимента магазина — <a href="/catalog/pariki/">парики</a> немецкого бренда Ellen Wille. Производитель изготавливает изделия с невидимой фронтальной линией, естественной укладкой, не теряющие форму под дождем, снегопадом и после купания в бассейне или на море.</p>
		<p>Парики Ellen Wille — это:</p>
		<ul class="list-marked">
			<li>мягкая, приятная на ощупь основа, которая не вызывает аллергию и зуд,</li>
			<li>вставки, имитирующие кожу головы,</li>
			<li>модели из натуральных и искусственных волос из волокна, которое не отличить от естественного на вид и на ощупь,</li>
			<li>легкие изделия: не жарко летом,</li>
			<li>дизайнерская окраска: впечатление салонного ухода,</li>
			<li>плотная посадка на голове: облегают голову, не болтаются и не давят, размер регулируется липучками.</li>
		</ul>
		<p>Подбираем изделия по материалу волос, категории длины, структуре, стрижке, стилю, оттенку и его особенностям, размеру, основе. В наличии детские, мужские и женские модели.</p>
		<p>В салоне планировка помещения продумана для удобства и сохранения конфиденциальности клиентов. Предусмотрен отдельный вход, закрытый кабинет для примерки изделий. С каждым клиентом работает мастер-стилист и консультант: подбирают модель, средства по уходу, корректируют стрижку на парике, правильно укладывают волосы.</p>
		<h2>Шиньоны и накладки</h2>
		<p>Магазин продает <a href="/catalog/shinony/">шиньоны</a>, <a href="/catalog/nakladki/">накладки</a> на макушку, теменную зону, пробор, чтобы увеличить объем, длину, скорректировать редеющие участки, сменить имидж. Подбираем накладки по категории длины, материалу, структуре волос, категории размера, способу крепления, типу основы, стилю.</p>
		<h2>Головные уборы</h2>
		<p>Предлагаем коллекции <a href="/catalog/golovnye-ubory/">головных уборов</a> Ellen Wille: банданы, косынки, береты, повязки, кепки, шапочки, тюрбаны, шляпки. Головные уборы подходят вместо парика (например, в сочетании с накладной полоской волос), в качестве аксессуара. Созданы из натуральных, гипоаллергенных, легких, приятных на ощупь тканей с модными принтами. Подбираем по сезону, расцветкам, фасону.</p>
	</div>
	<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width6">
		<p>При необходимости мы&nbsp;выполним следующие услуги:</p>
		<ul class="list-marked">
			<li>Адаптация изделия по&nbsp;размерам (возможна не&nbsp;для всех видов основы парика)</li>
			<li>Коррекция стрижки парика/накладки</li>
				<li>Тонирование волос на&nbsp;парике/накладке</li>
		</ul>
		<p>Все эти услуги оказывается после консультации с&nbsp;мастером. Возможны ограничения, связанные с&nbsp;индивидуальными особенностями конкретного изделия.</p>
		<p>
			Адрес <br>
			г. Санкт-Петербург <br>
			наб. Обводного канала д. 108
		</p>
		<p>
			Режим работы:<br>
			будни: 10:00 – 21:00<br>
			выходные и праздники: 10:00 – 19:00
		</p>
		<p>
			Телефоны <br>
			<span class="comagic_phone">+7 (812) 413-99-06</span> <br>
			+7 (921) 571-01-24
		</p>
	</div>

</div>

<div class="block-wrap block-wrap_wrap">

	<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
		<h2>Другие направления</h2>
		<p>При редких волосах можно использовать <a href="http://zagustitelvolos.com/">загустители</a> для увеличения объема и использовать лечебную косметику.</p>
		<p>Другой выход — это <a href="http://www.hairdesign.ru/">безоперационное замещение волос</a> или системы волос. Они изготавливаются на заказ, имеют форму головы клиента и фиксируется на срок от одного дня до нескольких недель. В них занимаются спортом, ходят в баню и сауну, спят.</p>
		<p>Со временем мы научились изготавливать <a href="http://volosy24.ru/">волосы для наращивания</a>, теперь нам доверяют оптовые покупатели.</p>
		<p>В одном центре мы объединили разные способы решения проблем, связанных с волосами.	</p>
	</div>

	<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width6">
		<br>
		<p><a href="http://salonvolos.spb.ru/" target="_blank">Салон красоты</a></p>
		<p><a href="http://www.hairdesign.ru/" target="_blank">Замещение волос</a></p>
		<p><a href="http://triholog.org/" target="_blank">Лечебная косметика для волос</a></p>
		<p><a href="http://zagustitelvolos.com/" target="_blank">Загустители волос Toppik</a></p>
		<p><a href="http://volosy24.ru/" target="_blank">Волосы и инструменты для наращивания</a></p>
	</div>

</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
