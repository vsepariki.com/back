<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Купить парик с доставкой по России, парики заказать почтой в Санкт-Петербурге");
$APPLICATION->SetPageProperty("description", "Доставка париков по России транспортной компанией, по Санкт-Петербургу курьером. Оплата картой, электронными деньгами, через терминалы, телефоном, наличными в салонах связи.");
$APPLICATION->SetTitle("Доставка и оплата");
?><div class="block-wrap ">
	<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6">
		<p>
 <a href="#delivery-spb" style="margin-right: 24px">Санкт-Петербург</a><a href="#delivery-moscow" style="margin-right: 24px">Москва</a><a href="#delivery-other">Другие города России</a>
		</p>
		<h2 id="delivery-spb">Санкт-Петербург</h2>
		<h3> Самовывоз из магазина - бесплатно </h3>
		<p>
			 Адрес: набережная Обводного канала - 108
		</p>
		<p>
			 Время работы: ежедневно с 10 до 21.
		</p>
		<p>
 <a href="/o-magazine/kontakty/">Как добраться?</a>
		</p>
		<p>
			 В магазине представлены все парики из наличия. У нас есть отдельные примерочные, в которых подберем парик.
		</p>
		<h3>Курьером по городу Санкт-Петербург и&nbsp;Ленинградской&nbsp;области</h3>
		<p>
			 Доставляем до двери курьерской службой "Dostavista".
		</p>
		<p>
			 Сроки - на следующий день или в тот же день. Доставка возможна в день заказа, при оплате до 18:00. Точные сроки доставки обговариваются при подтверждении заказа.
		</p>
		<h4>Стоимость</h4>
		<p>
			 по Санкт-Петербургу - 600 рублей. <br>
			 по Ленинградской области - 1100 руб.
		</p>
		<h2 id="delivery-moscow"> Москва </h2>
		<p>
			 Доставка в Москву осуществляется курьерской службой "Курьер сервис экспресс".
		</p>
		<p>
			 Cрок доставки 2-3 рабочих дня.
		</p>
		<p>
 <strong>Стоимость - 700 руб.</strong>
		</p>
		<h2 id="delivery-other">Доставка по России</h2>
		<p>
			 Отправляем транспортной компанией КСЭ.
		</p>
		<h3>Доставка по адресу курьером КСЭ</h3>
		<p>
 <b>Стоимость: от 700 до 1500 руб.</b> Стоимость и скорость доставки зависит от удаленности города от Санкт-Петербурга.
		</p>
		<p>
			 Срок доставки и стоимость рассчитывается автоматически в корзине при оформлении заказа. Для расчета укажите ваш город и выберите способ доставки “Транспортной компанией (курьер)”.
		</p>
		<p>
			 В день доставки курьер позвонит и согласует время.
		</p>
		<h2 class="h1">Способы оплаты</h2>
		<h3 class="h2">В магазине: наличными или банковской картой.</h3>
		<p>
			 При самовывозе из магазина принимаем к оплате наличные или банковские карты.
		</p>
		<h3 class="h2">Онлайн оплата на сайте</h3>
	</div>
</div>
<div class="block-wrap block-wrap_wrap">
	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
		<div class="payment-methods">
			<h4 class="payment-methods__name">Банковские карты</h4>
			<div class="payment-methods__list">
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/mir.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/mc.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/visa.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/applepay.svg" itemprop="contentUrl">
				</div>
			</div>
		</div>
	</div>
	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
		<div class="payment-methods">
			<h4 class="payment-methods__name">Электронные деньги</h4>
			<div class="payment-methods__list">
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/yandexdengi.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/webmoney.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/qiwi.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/elexnet.svg" itemprop="contentUrl">
				</div>
			</div>
		</div>
	</div>
	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
		<div class="payment-methods">
			<h4 class="payment-methods__name">Терминалы</h4>
			<div class="payment-methods__list">
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/elexnet.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/qiwi.svg" itemprop="contentUrl">
				</div>
			</div>
		</div>
	</div>
	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
		<div class="payment-methods">
			<h4 class="payment-methods__name">Интернет-банкинг</h4>
			<div class="payment-methods__list">
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/alfabank.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/russtand.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/vtb24.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/psb.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/handy.svg" itemprop="contentUrl">
				</div>
			</div>
		</div>
	</div>
	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
		<div class="payment-methods">
			<h4 class="payment-methods__name">Баланс телефона</h4>
			<div class="payment-methods__list">
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/beeline.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/megafon.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/mts.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/tele2.svg" itemprop="contentUrl">
				</div>
			</div>
		</div>
	</div>
	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
		<div class="payment-methods">
			<h4 class="payment-methods__name">Наличными в салонах связи</h4>
			<div class="payment-methods__list">
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/svyaznoy.svg" itemprop="contentUrl">
				</div>
				<div class="payment-methods__item" itemscope itemtype="http://schema.org/ImageObject">
 <img src="/images/payment/euroset.svg" itemprop="contentUrl">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="block-wrap ">
	<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6">
		<div class="payment-safety">
			<div>
				 Безопасность проведения оплаты на сайте гарантирует платежный сервис Robokassa
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
