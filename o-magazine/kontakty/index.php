<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Адрес магазина париков, где купить парики адрес салона");
$APPLICATION->SetPageProperty("description", "Адрес магазина париков в Спб: телефоны, почта, skype, схема проезда от метро, информация о парковке.");
$APPLICATION->SetTitle("Контакты");
$hideRecentlyViewedProducts = true;
?>
<div
    class="block-wrap  block-wrap_wrap "
    itemscope itemtype="http://schema.org/Organization"
>
	<div class="block-wrap__item block-wrap__item_xl-width5 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">

        <meta itemprop="name" content="Магазин париков Центр Дизайна волос">

	  <div
          class="contacts-item"
          itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"
      >

          <meta itemprop="streetAddress" content="наб. Обводного канала д. 108">
          <meta itemprop="postalCode" content="196084">
          <meta itemprop="addressLocality" content="Санкт-Петербург">

		 <h2 class="contacts-item__header">Адрес</h2>
		 <p class="contacts-item__big">
			г. Санкт-Петербург
			<br>наб. Обводного канала д. 108
		 </p> 

	  </div>
	  <div class="contacts-item">
		 <h2 class="contacts-item__header">Телефоны</h2>
		 <p class="contacts-item__big"><span class="comagic_phone">+7 (812) 413-99-06</span></p>
		 <p class="contacts-item__big">+7 (921) 571-01-24</p>
      <div class="block-wrap  ">
        <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width4 block-wrap__item_s-width2">
          <div class="link-icons"><a class="link-icons__link link-icons__link--viber" href="viber://chat?number=79215710124" target="_blank"></a><a class="link-icons__link link-icons__link--whatsapp" href="whatsapp://send?phone=79215710124&text=" target="_blank"></a><a class="link-icons__link link-icons__link--telegram" href="https://t.me/hair_design_spb" target="_blank"></a>
          </div>
        </div>
        <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width8 block-wrap__item_s-width4">
          <p>Напишите нам в Viber, WhatsApp или Telegram, добавив наш номер <strong style="white-space: nowrap">+7 (921) 571 01 24</strong> в список контактов.</p>
        </div>
      </div>

      <meta itemprop="telephone" content="+7 (812) 413-99-06">
      <meta itemprop="telephone" content="+7 (921) 571-01-24">
      <meta itemprop="email" content="mail@vsepariki.com">
      <br>
      <? /* <p><strong>График работы в новогодние праздники</strong>:
			<br> 30&nbsp;декабря&nbsp;&mdash; 10:00&nbsp;&mdash; 19:00 <br> с&nbsp;31&nbsp;декабря по&nbsp;3&nbsp;января&nbsp;&mdash; выходной 
        <br> с&nbsp;4&nbsp;января по&nbsp;6&nbsp;января&nbsp;&mdash; 10:00&nbsp;&mdash; 19:00
        <br> 7&nbsp;января&nbsp;&mdash; выходной
        <br> 8&nbsp;января&nbsp;&mdash; 10:00&nbsp;&mdash; 19:00
        <br> 10&nbsp;января&nbsp;&mdash; 10:00&nbsp;&mdash; 15:00
			</p> */?>
	  </div>
	  <div class="contacts-item">
		 <h2 class="contacts-item__header">Почта</h2>
		 <p class="contacts-item__big"><a href="mailto:mail@vsepariki.com">mail@vsepariki.com</a>
		 </p>
	  </div>
	  <? /* <div class="contacts-item">
		 <h2 class="contacts-item__header">Instagram</h2>
		 <p class="contacts-item__big"><a href="https://www.instagram.com/hair_design_spb">hair_design_spb</a>
		 </p>
	  </div> */ ?>
	</div>
	<div class="block-wrap__item block-wrap__item_xl-width7 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
	  <div class="contacts-map">
      <script type="text/javascript" charset="utf-8" data-skip-moving="true" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A9df9fa03e5ad1d4ecbf1c29c7d7a832f05fc16065206a604260854b0a0b8f5e3&amp;width=100%25&amp;height=370&amp;lang=ru_RU&amp;scroll=true"></script>
      <h2 class="contacts-map__subheader">Где припарковаться на машине</h2>
      <p>У входа в салон есть удобная бесплатная парковка. Вторая парковка на набережной Обводного канала, напротив входа  в салон.</p>
      <h2 class="contacts-map__subheader">Для тех, кто на метро</h2>
      <p>Ближайшие станции Балтийская и Технологический институт. Пешком 10-15 минут.</p>
      <? /* <p>Ближайшие станции  Балтийская и Фрунзенская. Пешком 10-15 минут.</p> */?>
	  </div>
	</div>
	<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
	  <figure class="image-desc contacts-entrance" itemscope itemtype="http://schema.org/ImageObject"><img class="image-desc__img" src="/images/contacts/entrance.jpg" alt="Вход в центре здания со стороны набережной" title="Вход в центре здания со стороны набережной" itemprop="contentUrl"/>
		 <figcaption class="image-desc__desc" itemprop="caption">Вход в центре здания со стороны набережной</figcaption>
	  </figure>
	</div>

</div>
<h2>3D-тур</h2>
<iframe src="/3dtur/index.html?media-index=1" name="Виртуальный тур" width="100%" height="600" frameborder="0" allow="fullscreen; accelerometer; gyroscope; magnetometer; vr; xr; xr-spatial-tracking; autoplay; camera; microphone" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" oallowfullscreen="true" msallowfullscreen="true"></iframe>
<h2>Видео 360 градусов</h2>
<video class="video-js vjs-default-skin" playsinline id="video360" controls preload="auto" poster="/video/3dtur.jpg">
  <source src="/video/3dtur.webm" type="video/webm">
</video>
<h2>Реквизиты</h2>
<p>
  ООО «Центр Дизайна Волос»
  <br>ИНН 7839080306
  <br>КПП 783901001
</p>
<p>
  ОКАТО 40262565000
  <br>ОКПО 06947226
  <br>ОКОПФ 12300
  <br>ОКФС 16
  <br>ОКВЭД 47.91
  <br>ОКОГУ 4210014
</p>
<p>
  Юридический адрес: 196084, СПб, набережная Обводного канала, дом 108, лит. А, пом 30Н
  <br>Банк:р/с 40702810232060006187 ФИЛИАЛ «Санкт-Петербургский» АО «АЛЬФА-БАНК»
  <br>БИК 044030786
  <br>к/с 30101810600000000786
  <br>Генеральный директор Лейтес Павел Борисович
</p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
