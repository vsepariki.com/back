<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Изготовление постижерных изделий | Центр дизайна волос");
$APPLICATION->SetTitle("Изготовление постижерных изделий");
$APPLICATION->SetPageProperty("description", "Изготовление постижерных изделий. Работаем с 2002 года. Доставка по России. Конфиденциальность, отдельная примерочная.");
$hideRecentlyViewedProducts = true;
?>
<h2><small>Накладка на&nbsp;заколках.</small></h2>
<p>Часто их&nbsp;используют в&nbsp;качестве альтернативы наращиванию волос. Их&nbsp;можно надевать и&nbsp;снимать по&nbsp;необходимости. Испоьзуют как для создания дополнительной густоты, так и&nbsp;для удлинения собственных волос, или для того и&nbsp;другого вместе. Изготавливается из&nbsp;ручного тресса. Поэтому на&nbsp;ощупь это все очень тоненькое и&nbsp;аккуратное. Густоту и&nbsp;ширину накладок можно регулировать по&nbsp;запросу клиента. Также количество таких накладок можно делать разным&nbsp;&mdash; от&nbsp;1&nbsp;до&nbsp;8.</p> 
<p>Стоимость работ от&nbsp;5000&nbsp;р.</p>
<p>Можно выполнять как из&nbsp;волос клиента, так и&nbsp;из&nbsp;наших окрашенных или неокрашенных волос. </p>
<div class="gallery">
  <div class="gallery__item">
    <img src="/images/o-magazine/izgotovlenie-postizhernykh-izdeliy/nakladki-na-zakolkah.jpg" alt="Накладки на заколках" title="Накладки на заколках"/>
  </div>
</div>
<p><a href="/o-magazine/izgotovlenie-nakladok-na-volosy/">Подробнее</a></p>
<h2><small>Хвост на&nbsp;ленте.</small></h2>
<p>Густота, объем, вес&nbsp;&mdash; по&nbsp;индивидуальному запросу клиента. В&nbsp;качестве крепления используется лента и&nbsp;минизажим. </p>
<p>Стоимость работ от&nbsp;5000&nbsp;руб.</p>
<p>Можно выполнять как из&nbsp;волос клиента, так и&nbsp;из&nbsp;наших окрашенных или неокрашенных волос. </p>
<div class="gallery">
  <div class="gallery__item">
    <img src="/images/o-magazine/izgotovlenie-postizhernykh-izdeliy/hvost-na-lente.jpg" alt="Хвост на ленте" title="Хвост на ленте" style="max-width: 100%; max-height:21.3125rem" />
  </div>
  <div class="gallery__item">
    <img src="/images/o-magazine/izgotovlenie-postizhernykh-izdeliy/hvost-na-lente-2.jpg" alt="Хвост на ленте" title="Хвост на ленте"  style="max-width: 100%; max-height:21.3125rem" />
  </div>
</div>

<h2><small>Хвост-шиньон.</small></h2>
<p>Для затылочной области для создания объема. Фиксируется шпильками к&nbsp;кичке из&nbsp;собственных волос. Основа шиньона&nbsp;&mdash; ручные трессы и&nbsp;эластичное кружево (сетка)</p>
<p>Стоимость работ от&nbsp;7000&nbsp;руб. </p>
<p>Можно выполнять как из&nbsp;волос клиента, так и&nbsp;из&nbsp;наших окрашенных или неокрашенных волос. </p>
<div class="gallery">
  <div class="gallery__item">
    <img src="/images/o-magazine/izgotovlenie-postizhernykh-izdeliy/hvost-shinon.jpg" alt="Хвост-шиньон" title="Хвост-шиньон" />
  </div>
</div>
<p> <a href="/o-magazine/izgotovlenie-shinonov/">Подробнее</a> </p>

<h2><small>Накладная коса (жгуты).</small></h2>
<p>Для создания прически в&nbsp;греческом стиле. </p>

<p>Стоимость работ от&nbsp;4000&nbsp;руб.</p>
<p>Можно выполнять как из&nbsp;волос клиента, так и&nbsp;из&nbsp;наших окрашенных или неокрашенных волос. </p>
<div class="gallery">
  <div class="gallery__item">
	  <img src="/images/o-magazine/izgotovlenie-postizhernykh-izdeliy/nakladnaya-kosa-zhguty-dlya-sozdaniya-pricheski.jpg" alt="Накладная коса (жгуты)" title="Накладная коса (жгуты)" style="max-width: 100%; max-height:21.3125rem" />
  </div>
  <div class="gallery__item">
    <img src="/images/o-magazine/izgotovlenie-postizhernykh-izdeliy/nakladnaya-kosa-zhguty-dlya-sozdaniya-pricheski-2.jpg" alt="Накладная коса (жгуты)" title="Накладная коса (жгуты)" />
  </div>
</div>

<h2><small>Постижерные украшения из&nbsp;волос</small></h2>
<p>Для праздничных и&nbsp;свадебных причесок.</p>
<p>Стоимость работ от&nbsp;1500&nbsp;руб.</p>
<div class="gallery">
  <div class="gallery__item">
	  <img src="/images/o-magazine/izgotovlenie-postizhernykh-izdeliy/kreativnaya-nakladka-na-zakolke.jpg" alt="Креативная Накладка на заколке" title="Креативная Накладка на заколке" style="max-width: 100%; max-height:21.3125rem" />
  </div>
  <div class="gallery__item">
    <img src="/images/o-magazine/izgotovlenie-postizhernykh-izdeliy/postizhernye-ukrasheniya-iz-volos-cvety.jpg" alt="Постижерные украшения из волос (цветы)" title="Постижерные украшения из волос (цветы)" style="max-width: 100%; max-height:21.3125rem" />
  </div>
</div>
<p> <a href="/o-magazine/izgotovlenie-postizhernykh-ukrasheniy/">Подробнее</a> </p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
