<?php

namespace Ajax;

use CCatalogSku;
use CFile;
use CIBlockElement;
use CModule;
use CPrice;
use DOMDocument;

class ChangeXmlAfter
{


    private static $arrFields = [];
    private static $arProps = [];
    private static $arrProduct = [];

    private static $baseUrl = 'https://vsepariki.com';



    private static $price;
    private static $old_price;
    private static $arrParent;
    private static $currentTime;
    private static $idFilter;
    private static $idIBlockSection = '';


    /**
     * @throws DOMException
     */
    public static function createXmlFile()

    {
        $xmlDoc = new DOMDocument('1.0', 'utf-8');
        unlink($_SERVER['DOCUMENT_ROOT'] . '/import/yandex_market.xml');

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/import/yandex_market.xml') === false) {
            // Создаем новый XML документ
            date_default_timezone_set('Europe/Moscow');

            self::$currentTime = date('Y-m-d\TH:i:sP');
// Создаем корневой элемент yml_catalog
            $ymlCatalog = $xmlDoc->createElement('yml_catalog');
            $ymlCatalog->setAttribute('date', self::$currentTime);
            $xmlDoc->appendChild($ymlCatalog);

// Создаем элемент shop
            $shop = $xmlDoc->createElement('shop');
            $ymlCatalog->appendChild($shop);

// Создаем и добавляем элементы в shop
            $name = $xmlDoc->createElement('name', 'Vsepariki.com');
            $shop->appendChild($name);

            $company = $xmlDoc->createElement('company', 'Центр Дизайна волос');
            $shop->appendChild($company);

            $url = $xmlDoc->createElement('url', 'https://vsepariki.com/');
            $shop->appendChild($url);

            $platform = $xmlDoc->createElement('platform', 'BSM/Yandex/Market');
            $shop->appendChild($platform);

            $version = $xmlDoc->createElement('version', '2.8');
            $shop->appendChild($version);
            $categoriesElement = $xmlDoc->createElement('categories');
            $shop->appendChild($categoriesElement);

// Добавляем категории
            $categories = ['Парики', 'Накладки', 'Шиньоны', 'Головные уборы',];
            foreach ($categories as $categoryName) {
                $category = $xmlDoc->createElement('category', $categoryName);
                $categoriesElement->appendChild($category);
                if ($categoryName == 'Шиньоны') {
                    $category->setAttribute('id','17');
                    $categoriesElement->appendChild($category);
                }

                if ($categoryName == 'Парики') {
                    $category->setAttribute('id','16');
                    $shop->appendChild($category);
                    $categoriesElement->appendChild($category);

                }

                if ($categoryName == 'Накладки') {
                    $category->setAttribute('id','18');
                    $shop->appendChild($category);
                    $categoriesElement->appendChild($category);

                }

                if ($categoryName == 'Головные уборы') {
                    $category->setAttribute('id','19');
                    $shop->appendChild($category);
                    $categoriesElement->appendChild($category);

                }

            }

            $enableAutoDiscounts = $xmlDoc->createElement('enable_auto_discounts', 'true');
            $shop->appendChild($enableAutoDiscounts);

// Создаем элемент currencies и добавляем в него currency
            $currencies = $xmlDoc->createElement('currencies');
            $shop->appendChild($currencies);

            $currency = $xmlDoc->createElement('currency');
            $currency->setAttribute('id', 'RUR');
            $currency->setAttribute('rate', '1');
            $currencies->appendChild($currency);
            // Создаем элемент offers и добавляем в него offer
            $offers = $xmlDoc->createElement('offers');
            $shop->appendChild($offers);
            if (CModule::IncludeModule('iblock')) {


                $arSelect = array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "DETAIL_TEXT", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "NAME", "PROPERTY_139");
                $arFilter = ["IBLOCK_ID" => 9, 'ACTIVE' => 'Y'];
                $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

                while ($ob = $res->GetNextElement()) {
                    self::$arProps[] = $ob->GetProperties();
                    self::$arrFields[] = $ob->GetFields();
                }


            }

            foreach (self::$arrFields as $key => $field) {
                self::$arrProduct[] = $field['ID'];


            }
            // получаем offers
            $arrOffers = CCatalogSKU::getOffersList(
                self::$arrProduct, // массив ID товаров
                $iblockID = 9,
                $skuFilter = array('>CATALOG_QUANTITY' => 0),
                $fields = array('NAME'),
                $propertyFilter = array()

            );
            foreach ($arrOffers as $offer) {
                foreach ($offer as $key => $value) {
                    self::$arrParent['COLOR'][] = $value['NAME'] . ',' . $value['PARENT_ID'];
                    self::$idFilter[] = $value['PARENT_ID'];

                }

            }


            foreach (self::$arrFields as $key => $field) {

                foreach (self::$arProps as $prop) {
                    if ($prop['ID']['VALUE'] == $field['ID'] && in_array($field['ID'],self::$idFilter)) {

                        $offer = $xmlDoc->createElement('offer');
                        $offer->setAttribute('id', $field['ID']);
                        $offer->setAttribute('available', 'true');
                        $tmpDesc = strip_tags($field['DETAIL_TEXT']);
                        $tmpDesc = str_replace('&mdash;', '&#8212;', $tmpDesc);
                        $tmpDesc = str_replace('&laquo;', '&#171;', $tmpDesc);
                        $tmpDesc = str_replace('&raquo;', '&#171;', $tmpDesc);
                        $tmpDesc = str_replace('&nbsp;', '&#160;', $tmpDesc);
                        $tmpDesc = str_replace('&times;', '×', $tmpDesc);
                        $tmpDesc = str_replace(' ', '&#160;', $tmpDesc);
                        $tmpDesc = str_replace("\n", ' ', $tmpDesc);
                        $tmpDesc = str_replace("\r", ' ', $tmpDesc);

                        if (!empty($prop['CUSTOM_DISCOUNT']['VALUE']) && $prop['CUSTOM_DISCOUNT']['VALUE'] > 0 ){
                            self::$price = CPrice::GetBasePrice($field['ID']);

                            $base_price = (int)self::$price['PRICE'];
                            $full_price = $prop['CUSTOM_DISCOUNT']['VALUE'];
                            self::$price = $full_price;
                            self::$old_price = $base_price;
                        }else{
                            self::$price = CPrice::GetBasePrice($field['ID']);
                            $full_price = (int)self::$price['PRICE'];
                            self::$price = $full_price;
                            self::$old_price = $full_price + ($full_price * 0.2);
                        }

                        $delivery = $xmlDoc->createElement('delivery', 'true');
                        $offer->appendChild($delivery);

                        if (!empty($field['DETAIL_PICTURE'])) {
                            $src = CFile::GetFileArray($field['DETAIL_PICTURE']);
                            $picture = $xmlDoc->createElement('picture', self::$baseUrl . $src['SRC']);
                            $offer->appendChild($picture);


                        }

                        if (!empty($field['DETAIL_PAGE_URL'])) {
                            $url = $xmlDoc->createElement('url', self::$baseUrl . $field['DETAIL_PAGE_URL']);
                            $offer->appendChild($url);


                        }
                        $paramElement = $xmlDoc->createElement('currencyId', 'RUB');
                        $offer->appendChild($paramElement);



                        if (!empty($prop['BRAND']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('vendor', $prop['BRAND']['VALUE']);
                            $offer->appendChild($paramElement);
                        }

                        if ($field['IBLOCK_SECTION_ID'] == '16') {
                            $paramElement = $xmlDoc->createElement('categoryId', '16');
                            $offer->appendChild($paramElement);
                        }

                        if ($field['IBLOCK_SECTION_ID'] == '17') {
                            $paramElement = $xmlDoc->createElement('categoryId', '17');
                            $offer->appendChild($paramElement);
                        }

                        if ($field['IBLOCK_SECTION_ID'] == '18') {
                            $paramElement = $xmlDoc->createElement('categoryId', '18');
                            $offer->appendChild($paramElement);
                        }

                        if ($field['IBLOCK_SECTION_ID'] == '19') {
                            $paramElement = $xmlDoc->createElement('categoryId', '19');
                            $offer->appendChild($paramElement);
                        }



                        if (!empty(self::$price)) {
                            $paramElement = $xmlDoc->createElement('price', $full_price);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty(self::$old_price)) {
                            $paramElement = $xmlDoc->createElement('oldprice', (int)self::$old_price);
                            $offer->appendChild($paramElement);
                        }


                        if (!empty($prop['TYPE'])) {
                            $paramType = $xmlDoc->createElement('param', $prop['TYPE']['VALUE']);
                            $paramType->setAttribute('name', $prop['TYPE']['NAME']);
                            $offer->appendChild($paramType);
                        }
                        foreach (self::$arrParent['COLOR'] as $item) {
                            $parts = explode(',', $item);
                            if ($parts[1] == $field['ID']) {
                                $paramElement = $xmlDoc->createElement('param', $parts[0]);
                                $paramElement->setAttribute('name', 'Цвет');
                                $offer->appendChild($paramElement);
                            }

                        }
                        if (!empty($prop['STRUCTURE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['STRUCTURE']['VALUE']);
                            $paramElement->setAttribute('name', $prop['STRUCTURE']['NAME']);
                            $offer->appendChild($paramElement);


                        }
                        if (!empty($prop['HAIRCUT']['VALUE'][0])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['HAIRCUT']['VALUE'][0]);
                            $paramElement->setAttribute('name', $prop['HAIRCUT']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['HAIRCUT']['VALUE'][1])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['HAIRCUT']['VALUE'][1]);
                            $paramElement->setAttribute('name', $prop['HAIRCUT']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['HAIRCUT']['VALUE'][2])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['HAIRCUT']['VALUE'][2]);
                            $paramElement->setAttribute('name', $prop['HAIRCUT']['NAME']);
                            $offer->appendChild($paramElement);
                        }


                        if (!empty($prop['STYLE']['VALUE'][0])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['STYLE']['VALUE'][0]);
                            $paramElement->setAttribute('name', $prop['STYLE']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['STYLE']['VALUE'][1])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['STYLE']['VALUE'][1]);
                            $paramElement->setAttribute('name', $prop['STYLE']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['STYLE']['VALUE'][2])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['STYLE']['VALUE'][2]);
                            $paramElement->setAttribute('name', $prop['STYLE']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        if (!empty($prop['FEATURES']['VALUE'][0])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FEATURES']['VALUE'][0]);
                            $paramElement->setAttribute('name', $prop['FEATURES']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        if (!empty($prop['FEATURES']['VALUE'][1])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FEATURES']['VALUE'][1]);
                            $paramElement->setAttribute('name', $prop['FEATURES']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        if (!empty($prop['FEATURES']['VALUE'][2])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FEATURES']['VALUE'][2]);
                            $paramElement->setAttribute('name', $prop['FEATURES']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        if (!empty($prop['FEATURES_COLOR']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FEATURES_COLOR']['VALUE']);
                            $paramElement->setAttribute('name', $prop['FEATURES_COLOR']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        if (!empty($prop['MATERIAL']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['MATERIAL']['VALUE']);
                            $paramElement->setAttribute('name', $prop['MATERIAL']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        if (!empty($prop['LENGTH']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['LENGTH']['VALUE']);
                            $paramElement->setAttribute('name', $prop['LENGTH']['NAME']);
                            $offer->appendChild($paramElement);
                        }


                        if (!empty($prop['FIT']['VALUE'][0])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FIT']['VALUE'][0]);
                            $paramElement->setAttribute('name', $prop['FIT']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['FIT']['VALUE'][1])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FIT']['VALUE'][1]);
                            $paramElement->setAttribute('name', $prop['FIT']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['FIT']['VALUE'][2])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FIT']['VALUE'][2]);
                            $paramElement->setAttribute('name', $prop['FIT']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        if (!empty($prop['WEIGHT']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('weight', $prop['WEIGHT']['VALUE']);
                            $offer->appendChild($paramElement);
                        }


                        if (!empty($prop['YANDEX_FEED_NAME']['VALUE']['TEXT'])) {
                            $name = $xmlDoc->createElement('name', $prop['YANDEX_FEED_NAME']['VALUE']['TEXT']);

                        }
                        /* Накладки начало */

                        // Тип

                        if (!empty($prop['TYPE_COVER']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['TYPE_COVER']['VALUE']);
                            $paramElement->setAttribute('name', $prop['TYPE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        // Длина

                        if (!empty($prop['LENGTH_COVER']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['LENGTH_COVER']['VALUE']);
                            $paramElement->setAttribute('name', $prop['LENGTH_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        // Материал

                        if (!empty($prop['MATERIAL_NATURE_COVER']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['MATERIAL_NATURE_COVER']['VALUE']);
                            $paramElement->setAttribute('name', $prop['MATERIAL_NATURE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        //основа

                        if (!empty($prop['BASE_COVER']['VALUE'][0])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['BASE_COVER']['VALUE'][0]);
                            $paramElement->setAttribute('name', $prop['BASE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['BASE_COVER']['VALUE'][1])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['BASE_COVER']['VALUE'][1]);
                            $paramElement->setAttribute('name', $prop['BASE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['PURPOSE_COVER']['VALUE'][2])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['PURPOSE_COVER']['VALUE'][2]);
                            $paramElement->setAttribute('name', $prop['PURPOSE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        //назначение

                        if (!empty($prop['PURPOSE_COVER']['VALUE'][0])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['PURPOSE_COVER']['VALUE'][0]);
                            $paramElement->setAttribute('name', $prop['PURPOSE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['PURPOSE_COVER']['VALUE'][1])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['PURPOSE_COVER']['VALUE'][1]);
                            $paramElement->setAttribute('name', $prop['PURPOSE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['BASE_COVER']['VALUE'][2])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['PURPOSE_COVER']['VALUE'][2]);
                            $paramElement->setAttribute('name', $prop['BASE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        //бренд


                        if (!empty($prop['BRAND_COVER']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('vendor', $prop['BRAND_COVER']['VALUE']);
                            $offer->appendChild($paramElement);
                        }
                        // особенности

                        if (!empty($prop['FEATURE_COVER']['VALUE'][0])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FEATURE_COVER']['VALUE'][0]);
                            $paramElement->setAttribute('name', $prop['FEATURE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['FEATURE_COVER']['VALUE'][1])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FEATURE_COVER']['VALUE'][1]);
                            $paramElement->setAttribute('name', $prop['PURPOSE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['FEATURE_COVER']['VALUE'][2])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FEATURE_COVER']['VALUE'][2]);
                            $paramElement->setAttribute('name', $prop['FEATURE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        //крепление
                        if (!empty($prop['ATTACH_COVER']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['ATTACH_COVER']['VALUE']);
                            $paramElement->setAttribute('name', $prop['ATTACH_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }


                        //структура
                        if (!empty($prop['STRUCTURE_COVER']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['STRUCTURE_COVER']['VALUE']);
                            $paramElement->setAttribute('name', $prop['STRUCTURE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }






                        if (!empty($prop['MATERIAL_TYPE_COVER']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['MATERIAL_TYPE_COVER']['VALUE']);
                            $paramElement->setAttribute('name', $prop['MATERIAL_TYPE_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        //вес
                        if (!empty($prop['WEIGHT_STRING_COVER']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('weight', $prop['WEIGHT_STRING_COVER']['VALUE']);
                            $offer->appendChild($paramElement);
                        }

                        //длина
                        if (!empty($prop['LENGTH_STRING_COVER']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['LENGTH_STRING_COVER']['VALUE']);
                            $paramElement->setAttribute('name', $prop['LENGTH_STRING_COVER']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        /* Накладки конец */



                        /* Шифоны начало */

                        // Материал

                        if (!empty($prop['MATERIAL_SHINONY']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['MATERIAL_SHINONY']['VALUE']);
                            $paramElement->setAttribute('name', $prop['MATERIAL_SHINONY']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        //вес
                        if (!empty($prop['WEIGHT_SHINONY']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('weight', $prop['WEIGHT_SHINONY']['VALUE']);
                            $offer->appendChild($paramElement);
                        }
                        // Стиль

                        if (!empty($prop['STYLE_SHINONY']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['STYLE_SHINONY']['VALUE']);
                            $paramElement->setAttribute('name', $prop['STYLE_SHINONY']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        // Тип

                        if (!empty($prop['TYPE_SHINONY']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['TYPE_SHINONY']['VALUE']);
                            $paramElement->setAttribute('name', $prop['TYPE_SHINONY']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        // Длина

                        if (!empty($prop['LENGTH_STRING_SHINONY']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['LENGTH_STRING_SHINONY']['VALUE']);
                            $paramElement->setAttribute('name', $prop['LENGTH_STRING_SHINONY']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        // Место
                        if (!empty($prop['POSITION_SHINONY']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['POSITION_SHINONY']['VALUE']);
                            $paramElement->setAttribute('name', $prop['POSITION_SHINONY']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        // Бренд

                        if (!empty($prop['BRAND_SHINONY']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('vendor', $prop['BRAND_SHINONY']['VALUE']);
                            $offer->appendChild($paramElement);
                        }
                        // Место крепления
                        if (!empty($prop['FIXING_SHINONY']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FIXING_SHINONY']['VALUE']);
                            $paramElement->setAttribute('name', $prop['FIXING_SHINONY']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        /* Конец Шифоны */


                        /* Головные уборы */

                        // Материал

                        if (!empty($prop['MATERIAL_STRING_HAT']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['MATERIAL_STRING_HAT']['VALUE']);
                            $paramElement->setAttribute('name', $prop['MATERIAL_STRING_HAT']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        //Вид

                        if (!empty($prop['FASION_HAT']['VALUE'][0])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FASION_HAT']['VALUE'][0]);
                            $paramElement->setAttribute('name', $prop['FASION_HAT']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['FASION_HAT']['VALUE'][1])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FASION_HAT']['VALUE'][1]);
                            $paramElement->setAttribute('name', $prop['FASION_HAT']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['FASION_HAT']['VALUE'][2])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FASION_HAT']['VALUE'][2]);
                            $paramElement->setAttribute('name', $prop['FASION_HAT']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        // сезон
                        if (!empty($prop['SEASON_HAT']['VALUE'][0])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['SEASON_HAT']['VALUE'][0]);
                            $paramElement->setAttribute('name', $prop['SEASON_HAT']['NAME']);
                            $offer->appendChild($paramElement);
                        }
                        if (!empty($prop['SEASON_HAT']['VALUE'][1])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['SEASON_HAT']['VALUE'][1]);
                            $paramElement->setAttribute('name', $prop['SEASON_HAT']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        if (!empty($prop['SEASON_HAT']['VALUE'][2])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['SEASON_HAT']['VALUE'][2]);
                            $paramElement->setAttribute('name', $prop['SEASON_HAT']['NAME']);
                            $offer->appendChild($paramElement);
                        }

                        //для кого
                        if (!empty($prop['FOR_WHOM_HAT']['VALUE'])) {
                            $paramElement = $xmlDoc->createElement('param', $prop['FOR_WHOM_HAT']['VALUE']);
                            $paramElement->setAttribute('name', $prop['FOR_WHOM_HAT']['NAME']);
                            $offer->appendChild($paramElement);
                        }


                        if (!empty($tmpDesc)) {
                            $desc = $xmlDoc->createElement('description', "<![CDATA[".$tmpDesc."]]>");
                            $offer->appendChild($desc);
                        }

                        $offer->appendChild($name);
                        $offers->appendChild($offer);
                        $xmlDoc->preserveWhiteSpace = false;
                        $xmlDoc->formatOutput = true; // Включаем форматирование
                        $xmlDoc->save($_SERVER['DOCUMENT_ROOT'] . '/import/yandex_market.xml');

                    }

                }


            }




        }


    }
}
