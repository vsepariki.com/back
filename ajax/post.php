<?php
// URL для запроса
$url = 'https://vsepariki.com/ajax.php';

// Данные для отправки
$data = json_encode(['status' => 'ok']);

// Инициализация cURL
$ch = curl_init($url);

// Установка параметров cURL
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Возвращать ответ как строку
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Возвращать ответ как строку

curl_setopt($ch, CURLOPT_POST, true); // Устанавливаем метод POST
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json', // Устанавливаем заголовок Content-Type
]);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // Устанавливаем данные для отправки

// Выполнение запроса
$response = curl_exec($ch);

// Проверка на ошибки
if (curl_errno($ch)) {
    echo 'Ошибка cURL: ' . curl_error($ch);
} else {
    // Обработка ответа
    $result = json_decode($response, true);
    print_r($result); // Выводим результат
}

// Закрытие cURL
curl_close($ch);
?>
