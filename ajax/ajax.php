<?php
// Включаем отображение ошибок PHP
require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

require  $_SERVER["DOCUMENT_ROOT"] . '/ajax/ChangeXmlAfter.php';

use Ajax\ChangeXmlAfter;

// Проверяем, что запрос был методом POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Получаем данные из тела запроса
    $data = json_decode(file_get_contents('php://input'), true);

    // Проверяем наличие ключа "status" в данных
    if (isset($data['status']) && $data['status'] === 'ok') {
        // Если ключ "status" равен 'ok', выполняем нужные действия
        // Например, вызываем метод класса

        // Вызываем статический метод createXmlFile() напрямую через класс
        $result = ChangeXmlAfter::createXmlFile();

        // Отправляем результат как JSON
        header('Content-Type: application/json');
        echo json_encode(['result' => $result]);
    } else {
        // Если ключ "status" не равен 'ok', возвращаем ошибку
        header("HTTP/1.1 400 Bad Request");
        echo json_encode(['error' => 'Неверный статус']);
    }
} else {
    // Если запрос не был методом POST, возвращаем ошибку
    header("HTTP/1.1 405 Method Not Allowed");
    echo json_encode(['error' => 'Метод не разрешен']);
}
?>
