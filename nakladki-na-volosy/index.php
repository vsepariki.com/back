<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Накладки для волос в спб, купить накладку на голову недорого");
$APPLICATION->SetPageProperty("description", "Накладки для волос любых размеров и цветов в Спб. Европейское качество по низкое цене. Купить накладку на голову из натуральных и искусственных волос недорого.");
$APPLICATION->SetTitle("Накладки для волос");?><div class="block-wrap  block-wrap_wrap ">
            <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6">
              <p>Продаем готовые накладки из натуральных и искусственных волос.</p>
              <p>Накладки, как правило, используют для того, чтоб скрыть проблемную зону. Они легче, чем парик, и позволяют сочетать добавленные волосы со своими собственными. При этом у накладки чаще уже задана определенная форма, и вам не надо тратить время на укладку. </p>
          </div>
            <div class="block-wrap__item block-wrap__item_xl-width12 block-wrap__item_l-width12 block-wrap__item_m-width12 block-wrap__item_s-width6">
          <div class="block-wrap  block-wrap_wrap ">
            <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3">
              <figure class="image-desc"><img class="image-desc__img" src="/images/overlays/just-nature.jpg" alt="Светлые короткие накладки – Just Nature" title="Светлые короткие накладки – Just Nature"/>
                <figcaption class="image-desc__desc">Светлые короткие накладки – Just Nature</figcaption>
              </figure>
            </div>
            <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3">
              <figure class="image-desc"><img class="image-desc__img" src="/images/overlays/fill-in.jpg" alt="Темные короткие накладки – Fill in" title="Темные короткие накладки – Fill in"/>
                <figcaption class="image-desc__desc">Темные короткие накладки – Fill in</figcaption>
              </figure>
            </div>
            <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3">
              <figure class="image-desc"><img class="image-desc__img" src="/images/overlays/matrix.jpg" alt="Светлые длинные накладки – Matrix" title="Светлые длинные накладки – Matrix"/>
                <figcaption class="image-desc__desc">Светлые длинные накладки – Matrix</figcaption>
              </figure>
            </div>
            <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3">
              <figure class="image-desc"><img class="image-desc__img" src="/images/overlays/effect.jpg" alt="Темные длинные накладки – Effect" title="Темные длинные накладки – Effect"/>
                <figcaption class="image-desc__desc">Темные длинные накладки – Effect</figcaption>
              </figure>
            </div>
          </div>
          </div>
            <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6">
              <p>В большинстве случаев для фиксации накладки к собственным волосам используются миниатюрные зажимы, которых совершенно не видно в волосах.</p>
              <div class="text-highlighted">
                <p>В салоне Центр Дизайна Волос представлен большой выбор накладок, а также <a href="/khvosty-i-shinony/">накладных хвостов</a> различных видов, фасонов, длин, текстур и цветов.
                </p>
              </div>
            </div>
          </div>
          <figure class="image-desc"><img alt="Накладка Ideal volume от NJ Creation" src="/images/overlays/ideal-volume.jpg" class="image-desc__img" title="Накладка Ideal volume от NJ Creation">
            <figcaption class="image-desc__desc">Модель накладки Ideal volume (в наличии) от NJ Creation включает 16 цветов.</figcaption>
          </figure>

          <div class="block-wrap  block-wrap_wrap ">
            <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6">
              <div class="video-responsive">
                <div class="video-responsive__iframe">
                  <iframe src="https://www.youtube.com/embed/RWE6urLtRoA" allowfullscreen="allowfullscreen"></iframe>
                </div>
                <div class="video-responsive__desc">
                  Посмотрите как преобразится Ольга, одев накладку.
                </div>
              </div>
            </div>
            <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6">
              <div class="video-responsive">
                <div class="video-responsive__iframe">
                  <iframe src="https://www.youtube.com/embed/KeMFbIN9OP4" allowfullscreen="allowfullscreen"></iframe>
                </div>
                <div class="video-responsive__desc">
                  Небольшая накладка из натуральных волос на вехнюю часть головы. Служит для дополнительного объема в зоне пробора и затылка, маскирует отросшие корни.
                </div>
              </div>
            </div>
          </div>
          <div class="block-wrap  block-wrap_wrap ">
            <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6">
              <h2>Приглашаем примерить и купить шиньоны в магазин в Санкт-Петербурге</h2>
              <p>Приглашаем всех заинтересованных на бесплатную консультацию по постижерным изделиям. Наш адрес: г Санкт-Петербург, набережная обводного канала 108. Магазин шиньонов и хвостов открыт с 10 утра до 9 вечера, ежедневно.</p>
              <p>Звоните, проконсультируем по наличию цветов и размеров шиньонов:<br><strong>+7 (812) 413-99-06</strong></p><script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=0jFSeud2k6YiSEPpq1L9FIjWP8F6PbZr&amp;height=370&amp;lang=ru_RU&amp;sourceType=constructor"></script> 
            </div>
          </div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>