<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width5 block-wrap__item_m-width6 block-wrap__item_s-width6">

	<form class="home-filter-form">
		<fieldset class="home-filter-form__fieldset">

			<legend class="home-filter-form__header">Подобрать парик</legend>

			<div class="home-filter-form__types">

                <div class="home-filter-form__type">
                    <div class="home-filter-form__subheader">Наличие</div>
                    <div class="home-filter-form__row">
                        <label class="home-filter-form__input">
                            <input type="checkbox" name="availability"> В наличии
                        </label>
                    </div>
                </div>

                <div class="home-filter-form__type">
					<div class="home-filter-form__subheader">Тип</div>
					<div class="home-filter-form__row">
						<label class="home-filter-form__input">
							<input type="radio" name="type" value="muzhskoy"> Мужской
						</label>
						<label class="home-filter-form__input">
							<input type="radio" name="type" value="zhenskiy"> Женский
						</label>
						<label class="home-filter-form__input">
							<input type="radio" name="type" value="detskiy"> Детский
						</label>
					</div>
				</div>

				<div class="home-filter-form__type">
					<div class="home-filter-form__subheader">Длина</div>
					<div class="home-filter-form__row">
						<label class="home-filter-form__input">
							<input type="radio" name="length_filter" value="korotkiy"> Короткий
						</label>
						<label class="home-filter-form__input">
							<input type="radio" name="length_filter" value="sredniy"> Средний
						</label>
						<label class="home-filter-form__input">
							<input type="radio" name="length_filter" value="dlinnyy"> Длинный
						</label>
					</div>
				</div>

				<div class="home-filter-form__type">
					<div class="home-filter-form__subheader">Материал</div>
					<div class="home-filter-form__row block-wrap_wrap">
						<label class="home-filter-form__input">
							<input type="radio" name="material" value="naturalnyy"> Натуральный
						</label>
						<label class="home-filter-form__input">
							<input type="radio" name="material" value="iskusstvennyy"> Моноволокно <br>&nbsp;&nbsp;&nbsp; (не отличить от натурального)
						</label>
					</div>
				</div>

				<div class="home-filter-form__type">
					<div class="home-filter-form__subheader">Структура</div>
					<div class="home-filter-form__row">
						<label class="home-filter-form__input">
							<input type="radio" name="structure" value="volnistyy"> Волнистый
						</label>
						<label class="home-filter-form__input">
							<input type="radio" name="structure" value="pryamoy"> Прямой
						</label>
						<label class="home-filter-form__input">
							<input type="radio" name="structure" value="kudryavyy"> Кудрявый
						</label>
					</div>
				</div>


			</div>

			<div class="home-filter-form__row">
				<button class="home-filter-form__submit btn" type="submit">Подобрать</button>
				<a class="home-filter-form__link" href="/catalog/pariki/">Перейти в расширенный подбор</a>
			</div>

		</fieldset>
	</form>

</div> <!-- Подобрать парик -->
