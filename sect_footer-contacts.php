<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width12 block-wrap__item_s-width6">
	<div class="block-wrap footer-main-contacts block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width12 block-wrap__item_l-width12 block-wrap__item_m-width7 block-wrap__item_s-width6">
			<div class="footer-main-contacts__phone"><span class="comagic_phone">+7 (812) 413-99-06</span></div>
			<div class="footer-main-contacts__copyright xl-hidden l-hidden s-hidden">© Магазин париков "Центр Дизайна волос" 2006 - 2016</div>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width12 block-wrap__item_l-width12 block-wrap__item_m-width4 block-wrap__item_s-width6">
			<div class="footer-main-contacts__address">
				г. Санкт-Петербург, <br>
				наб. Обводного канала, 108А <br>
				Работаем ежедневно
				<br>Предварительная запись по телефону обязательна
				<br>пн-пт: с 10 до 21
				<br>сб-вс: с 10 до 19
				<br><a class="link-primary" href="/o-magazine/kontakty/">Как добраться</a>
			</div>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width12 block-wrap__item_l-width12 block-wrap__item_m-width1 block-wrap__item_s-width6">
			<div class="footer-main-contacts__social">
				<a class="footer-main-contacts__vk" href="https://vk.com/salon_hairdesign" target="_blank" rel="nofollow"></a>
			</div>
		</div>
	</div>
</div>
