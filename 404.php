<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница не найдена");
$APPLICATION->SetPageProperty("TITLE", "Страница не существует");
$APPLICATION->SetPageProperty("description", 'К сожалению запрашиваемая вами страница не найдена. Для продолжения работы с сайтом Вы можете вернуться на главную страницу или воспользоваться картой сайта');
$APPLICATION->AddChainItem("Страница не найдена");
$hideRecentlyViewedProducts = true;
?>
<p>Неправильно набран адрес, или такой страницы на сайте больше не существует.</p>
<p>Вернитесь <a href="/">на главную</a> или воспользуйтесь картой сайта.</p>
<div class="error-list">
	<div class="error-list__header">Каталог товаров</div>
	<div class="error-list__body">
		<a class="error-list__item" href="/catalog/pariki/">Парики</a>
		<a class="error-list__item" href="/khvosty-i-shinony/">Шиньоны и хвосты</a>
		<a class="error-list__item" href="/catalog/nakladki/">Накладки</a>
		<a class="error-list__item" href="/catalog/golovnye-ubory/">Головные уборы вместо парика</a>
		<a class="error-list__item" href="/stati/ukhod-za-parikami.html">Средства по уходу</a>
	</div>
</div>
<div class="error-list">
	<div class="error-list__header">О магазине</div>
	<div class="error-list__body">
		<a class="error-list__item" href="/o-magazine/">О магазине</a>
		<a class="error-list__item" href="/dostavka-i-oplata/">Доставка и оплата</a>
		<a class="error-list__item" href="/novosti/">Новости и акции</a>
		<a class="error-list__item" href="/vozvrat-i-obmen/">Возврат и обмен товара</a>
		<a class="error-list__item" href="/stati/">Статьи о париках</a>
		<a class="error-list__item" href="/kontakty/">Контакты</a>
</div>
</div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>