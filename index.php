<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Интернет магазин париков в Спб, купить парик в магазине дешево в розницу через сайт");
$APPLICATION->SetPageProperty("description", "Интернет магазин париков, накладок на волосы и головных уборов. Около 200 моделей париков. Работаем с 2002 года. Доставка по России. Конфиденциальность, отдельная примерочная. Большой опыт работы с людьми с потерей волос.");
$APPLICATION->SetTitle("Интернет магазин париков");
?>
<? /* <div class="container home-announcement">
  <div class="content">
    <p style="font-size: 1.25rem; font-weight: bold;">Инвентаризация ассортимента магазина 01.08.2022</p>
    <p>Уважаемые клиенты!</p>
    <p>В связи с полной инвентаризацией ассортимента магазина и салона 01.08.2022 — все заявки будут обработаны с задержкой (02.08.2022).</p>
    <p>Запись к мастерам на этот день закрыта.</p>
    <p>Приносим извинения за предоставленное неудобство.</p>
  </div>
</div> */ ?>
<div class="container home-top">
  <div class="content">
	 <div class="block-wrap  block-wrap_wrap " style="display: flex;">
		<?
		$APPLICATION->IncludeComponent(
			"bitrix:main.include", "",
			Array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "select-wig-form",
			), false, array("HIDE_ICONS"=>"Y")
		);

		$APPLICATION->IncludeComponent(
			"bitrix:main.include", "",
			Array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "banners",
			), false, array("HIDE_ICONS"=>"Y")
		);
		?>
	 </div>
  </div>
</div>

<div class="container"> 
	<div class="content home-discount-slider">
		<p class="home-discount-slider__header">Акции </p>
		<div class="slider home-discount-slider__slider">
			<div class="slider__slides swiper">
				<div class="slider__wrapper swiper-wrapper">

					<?/* <div class="slide swiper-slide">
						<div class="home-discount-slider__item">
							<div class="home-discount-slider__info">
								<p class="home-discount-slider__title">Скидка <mark>15%</mark></p>
								<p class="home-discount-slider__description">На все натуральные парики </p>
								<p class="countdown home-discount-slider__countdown" data-date="2024, 10, 01"></p><a class="home-discount-slider__btn btn" href="/catalog/pariki/naturalnyy/">Каталог</a>
							</div>
							<picture>
								<source srcset="/images/home/discounts/discounts-natural.webp" type="image/webp"/>
								<source srcset="/images/home/discounts/discounts-natural.png"/><img class="home-discount-slider__image" src="/images/home/discounts/discounts-natural.png" loading="lazy" decoding="async" alt="Скидка &lt;mark&gt;10%&lt;/mark&gt;"/>
							</picture>
						</div>
					</div> */?>

					<? /* <div class="slide swiper-slide">
						<div class="home-discount-slider__item">
							<div class="home-discount-slider__info">
								<p class="home-discount-slider__title">Скидка <mark>от 30%</mark></p>
								<p class="home-discount-slider__description"><strong>Ликвидация</strong> на накладки с трессами </p>
								<p class="countdown home-discount-slider__countdown" data-date="2025, 02, 01"></p><a class="home-discount-slider__btn btn" href="/catalog/nakladki/filter/base_cover-is-monofilament-tressy-or-setka-tressy/brand_cover-is-dizayn-volos-collection/apply/">Каталог</a>
							</div>
							<picture>
								<source srcset="/images/home/discounts/discounts-setka.webp" type="image/webp"/>
								<source srcset="/images/home/discounts/discounts-setka.png"/><img class="home-discount-slider__image" src="/images/home/discounts/discounts-setka.png" loading="lazy" decoding="async" alt="Скидка &lt;mark&gt;50%&lt;/mark&gt;"/>
							</picture>
						</div>
					</div>
					<div class="slide swiper-slide">
						<div class="home-discount-slider__item">
							<div class="home-discount-slider__info">
								<p class="home-discount-slider__title">Скидка <mark>10%</mark></p>
								<p class="home-discount-slider__description">На все накладки из натуральных волос</p>
								<p class="countdown home-discount-slider__countdown" data-date="2025, 01, 01"></p><a class="home-discount-slider__btn btn" href="https://vsepariki.com/catalog/nakladki/naturalnyy/">Каталог</a>
							</div>
							<picture>
								<source srcset="/images/home/discounts/discounts-top-natural.webp" type="image/webp"/>
								<source srcset="/images/home/discounts/discounts-top-natural.png"/><img class="home-discount-slider__image" src="/images/home/discounts/discounts-top-natural.png" loading="lazy" decoding="async" alt="Скидка &lt;mark&gt;15%&lt;/mark&gt;"/>
							</picture>
						</div>
					</div>
					<div class="slide swiper-slide">
						<div class="home-discount-slider__item">
							<div class="home-discount-slider__info">
								<p class="home-discount-slider__title">Скидка <mark>10%</mark></p>
								<p class="home-discount-slider__description">На все парики из натуральных волос</p>
								<p class="countdown home-discount-slider__countdown" data-date="2025, 01, 01"></p><a class="home-discount-slider__btn btn" href="https://vsepariki.com/catalog/pariki/naturalnyy/">Каталог</a>
							</div>
							<picture>
								<source srcset="/images/home/discounts/discounts-wig-natural.webp" type="image/webp"/>
								<source srcset="/images/home/discounts/discounts-wig-natural.png"/><img class="home-discount-slider__image" src="/images/home/discounts/discounts-wig-natural.png" loading="lazy" decoding="async" alt="Скидка &lt;mark&gt;15%&lt;/mark&gt;"/>
							</picture>
						</div>
					</div> */?>
					<div class="slide swiper-slide">
						<div class="home-discount-slider__item">
							<div class="home-discount-slider__info">
								<p class="home-discount-slider__title">Модный утюжок <mark>в подарок</mark></p>
								<p class="home-discount-slider__description">Получи подарок при покупке изделия из&nbsp;натуральных волос от&nbsp;10&nbsp;000&nbsp;₽ и&nbsp;создай свой идеальный образ!</p>
								<p class="countdown home-discount-slider__countdown" data-date="2025, 03, 01"></p>
								<a class="home-discount-slider__btn btn" href="https://volosy24.ru/shop/instrumenty-dlya-ukladki-volos/utyuzhok-dlya-vypryamleniya-volos-mini/" target="_blank">Каталог</a>
							</div>
							<picture>
								<source srcset="/images/home/discounts/discounts-iron.webp" type="image/webp"/>
								<source srcset="/images/home/discounts/discounts-iron.png"/><img class="home-discount-slider__image" src="/images/home/discounts/discounts-iron.png" loading="lazy" decoding="async" alt="Модный утюжок &lt;mark&gt;в подарок&lt;/mark&gt;"/>
							</picture>
						</div>
					</div> 
				</div>
				<div class="slider__arrow slider__arrow--prev"></div>
				<div class="slider__arrow slider__arrow--next"></div>
				<div class="slider__pagination"></div>
			</div>
		</div>
	</div>
</div>

<?
$APPLICATION->IncludeComponent("bitrix:main.include", "include", Array(
	"AREA_FILE_SHOW" => "page",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "select-wig-catalog",	// Суффикс имени файла включаемой области
	),
	false
);
?>

<div class="container home-other-products">
    <div class="content">
		<?
		$APPLICATION->IncludeComponent(
			"bitrix:main.include", "",
			Array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "golovnye-ubory",
			), false, array("HIDE_ICONS"=>"Y")
		);

		$APPLICATION->IncludeComponent(
			"bitrix:main.include", "",
			Array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "nakladki",
			), false, array("HIDE_ICONS"=>"Y")
		);

		$APPLICATION->IncludeComponent(
			"bitrix:main.include", "",
			Array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "shinony",
			), false, array("HIDE_ICONS"=>"Y")
		);
		?>
    </div>
</div>

<?
$APPLICATION->IncludeComponent(
	"bitrix:main.include", "",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "static-part",
	)
);
?>

<!-- News -->
<?
$APPLICATION->IncludeComponent(
	"bitrix:main.include", "",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "news",
	)
);
?>
<!-- /News --><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
