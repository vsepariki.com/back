<?
class KonsultatsiyaStoimostIOplataKhotelosByPonyatOtChegoZavisitStoimostParikaIzNaturalnykhVolCest
{
    /** @var AcceptanceTester */
    protected $tester;

    protected $pagesMetadata;

    protected $url;
    protected $pageTitle;
    protected $pageH1;
    protected $metaDescription;
    protected $metaKeywords;

    public function _before(AcceptanceTester $I)
    {
        $this->tester = $I;

        $this->url = "/konsultatsiya/stoimost-i-oplata/khotelos-by-ponyat-ot-chego-zavisit-stoimost-parika-iz-naturalnykh-vol.html";
        $this->pageTitle = "От чего зависит стоимость парика из натуральных волос? / Форум и консультация";
        $this->pageH1 = "От чего зависит стоимость парика из натуральных волос?";
        $this->metaDescription = "здравствуйте! хотелось бы понять от чего зависит стоимость парика из натуральных волос. Чтобы понимать смогу ли я себе это позволить-средняя цена. Благодарю. с уважением. Елена";
        $this->metaKeywords = "Keywords";
    }


    public function runTests()
    {
        $this->tester->amOnPage($this->url);
        $this->responseCodeTest();
        $this->titleTest();
        $this->h1Test();
        $this->metaDescriptionTest();
        $this->metaKeywordsTest();
    }


    protected function responseCodeTest()
    {
        $this->tester->seeResponseCodeIs(200);
    }

    protected function titleTest()
    {
        $grabbedTitle = $this->tester->grabTextFrom("//title");
        $this->tester->assertEquals($this->pageTitle, $grabbedTitle, "Проверка TITLE");
    }

    protected function h1Test()
    {
        $this->tester->see($this->pageH1, "//h1", "Проверка H1");
    }

    private function metaDescriptionTest()
    {
        if($this->metaDescription){
            $grabbedMetaDescription = $this->tester->grabTextFrom('//html/head/meta[@name="description"]/@content');
            $grabbedMetaDescription = html_entity_decode($grabbedMetaDescription);
            $this->tester->assertEquals($this->metaDescription, $grabbedMetaDescription, "Проверка DESCRIPTION");
        }
    }

    private function metaKeywordsTest()
    {
        if($this->metaKeywords){
            $grabbedMetaKeywords = $this->tester->grabTextFrom('//html/head/meta[@name="keywords"]/@content');
            $this->tester->assertEquals($this->metaKeywords, $grabbedMetaKeywords, "Проверка KEYWORDS");
        }
    }

}