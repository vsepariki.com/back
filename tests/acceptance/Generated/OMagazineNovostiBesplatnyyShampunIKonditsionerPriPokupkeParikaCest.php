<?
class OMagazineNovostiBesplatnyyShampunIKonditsionerPriPokupkeParikaCest
{
    /** @var AcceptanceTester */
    protected $tester;

    protected $pagesMetadata;

    protected $url;
    protected $pageTitle;
    protected $pageH1;
    protected $metaDescription;
    protected $metaKeywords;

    public function _before(AcceptanceTester $I)
    {
        $this->tester = $I;

        $this->url = "/o-magazine/novosti/besplatnyy-shampun-i-konditsioner-pri-pokupke-parika.html";
        $this->pageTitle = "Бесплатный шампунь и кондиционер при покупке парика / Новости";
        $this->pageH1 = "Бесплатный шампунь и кондиционер при покупке парика";
        $this->metaDescription = "Дарим профессиональные средства по уходу за париками Estel Otium Aqua при покупке от 15 000 рублей. Акция действует на все коллекции париков";
        $this->metaKeywords = "Бесплатный шампунь и кондиционер при покупке парика";
    }


    public function runTests()
    {
        $this->tester->amOnPage($this->url);
        $this->responseCodeTest();
        $this->titleTest();
        $this->h1Test();
        $this->metaDescriptionTest();
        $this->metaKeywordsTest();
    }


    protected function responseCodeTest()
    {
        $this->tester->seeResponseCodeIs(200);
    }

    protected function titleTest()
    {
        $grabbedTitle = $this->tester->grabTextFrom("//title");
        $this->tester->assertEquals($this->pageTitle, $grabbedTitle, "Проверка TITLE");
    }

    protected function h1Test()
    {
        $this->tester->see($this->pageH1, "//h1", "Проверка H1");
    }

    private function metaDescriptionTest()
    {
        if($this->metaDescription){
            $grabbedMetaDescription = $this->tester->grabTextFrom('//html/head/meta[@name="description"]/@content');
            $grabbedMetaDescription = html_entity_decode($grabbedMetaDescription);
            $this->tester->assertEquals($this->metaDescription, $grabbedMetaDescription, "Проверка DESCRIPTION");
        }
    }

    private function metaKeywordsTest()
    {
        if($this->metaKeywords){
            $grabbedMetaKeywords = $this->tester->grabTextFrom('//html/head/meta[@name="keywords"]/@content');
            $this->tester->assertEquals($this->metaKeywords, $grabbedMetaKeywords, "Проверка KEYWORDS");
        }
    }

}