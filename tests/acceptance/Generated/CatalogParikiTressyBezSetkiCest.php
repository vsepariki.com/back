<?
class CatalogParikiTressyBezSetkiCest
{
    /** @var AcceptanceTester */
    protected $tester;

    protected $pagesMetadata;

    protected $url;
    protected $pageTitle;
    protected $pageH1;
    protected $metaDescription;
    protected $metaKeywords;

    public function _before(AcceptanceTester $I)
    {
        $this->tester = $I;

        $this->url = "/catalog/pariki/tressy-bez-setki/";
        $this->pageTitle = "Парик без сетки, из тресс - купить недорого в Санкт-Петербурге";
        $this->pageH1 = "Парики без сетки";
        $this->metaDescription = "Предлагаем парик без сетки – вентилируемые изделия, хорошо садящиеся по форме головы, легко укладывающиеся, придающие волосам естественный объем. В отдельной примерочной консультанты подберут парик под ваш стиль. У нас вы сможете получить европейское качество по доступной цене.";
        $this->metaKeywords = "Keywords";
    }


    public function runTests()
    {
        $this->tester->amOnPage($this->url);
        $this->responseCodeTest();
        $this->titleTest();
        $this->h1Test();
        $this->metaDescriptionTest();
        $this->metaKeywordsTest();
    }


    protected function responseCodeTest()
    {
        $this->tester->seeResponseCodeIs(200);
    }

    protected function titleTest()
    {
        $grabbedTitle = $this->tester->grabTextFrom("//title");
        $this->tester->assertEquals($this->pageTitle, $grabbedTitle, "Проверка TITLE");
    }

    protected function h1Test()
    {
        $this->tester->see($this->pageH1, "//h1", "Проверка H1");
    }

    private function metaDescriptionTest()
    {
        if($this->metaDescription){
            $grabbedMetaDescription = $this->tester->grabTextFrom('//html/head/meta[@name="description"]/@content');
            $grabbedMetaDescription = html_entity_decode($grabbedMetaDescription);
            $this->tester->assertEquals($this->metaDescription, $grabbedMetaDescription, "Проверка DESCRIPTION");
        }
    }

    private function metaKeywordsTest()
    {
        if($this->metaKeywords){
            $grabbedMetaKeywords = $this->tester->grabTextFrom('//html/head/meta[@name="keywords"]/@content');
            $this->tester->assertEquals($this->metaKeywords, $grabbedMetaKeywords, "Проверка KEYWORDS");
        }
    }

}