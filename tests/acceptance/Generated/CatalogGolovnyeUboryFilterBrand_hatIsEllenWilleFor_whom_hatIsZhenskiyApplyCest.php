<?
class CatalogGolovnyeUboryFilterBrand_hatIsEllenWilleFor_whom_hatIsZhenskiyApplyCest
{
    /** @var AcceptanceTester */
    protected $tester;

    protected $pagesMetadata;

    protected $url;
    protected $pageTitle;
    protected $pageH1;
    protected $metaDescription;
    protected $metaKeywords;

    public function _before(AcceptanceTester $I)
    {
        $this->tester = $I;

        $this->url = "/catalog/golovnye-ubory/filter/brand_hat-is-ellen-wille/for_whom_hat-is-zhenskiy/apply/";
        $this->pageTitle = "Головные уборы Ellen Wille Женский";
        $this->pageH1 = "Головные уборы";
        $this->metaDescription = "";
        $this->metaKeywords = "Keywords";
    }


    public function runTests()
    {
        $this->tester->amOnPage($this->url);
        $this->responseCodeTest();
        $this->titleTest();
        $this->h1Test();
        $this->metaDescriptionTest();
        $this->metaKeywordsTest();
    }


    protected function responseCodeTest()
    {
        $this->tester->seeResponseCodeIs(200);
    }

    protected function titleTest()
    {
        $grabbedTitle = $this->tester->grabTextFrom("//title");
        $this->tester->assertEquals($this->pageTitle, $grabbedTitle, "Проверка TITLE");
    }

    protected function h1Test()
    {
        $this->tester->see($this->pageH1, "//h1", "Проверка H1");
    }

    private function metaDescriptionTest()
    {
        if($this->metaDescription){
            $grabbedMetaDescription = $this->tester->grabTextFrom('//html/head/meta[@name="description"]/@content');
            $grabbedMetaDescription = html_entity_decode($grabbedMetaDescription);
            $this->tester->assertEquals($this->metaDescription, $grabbedMetaDescription, "Проверка DESCRIPTION");
        }
    }

    private function metaKeywordsTest()
    {
        if($this->metaKeywords){
            $grabbedMetaKeywords = $this->tester->grabTextFrom('//html/head/meta[@name="keywords"]/@content');
            $this->tester->assertEquals($this->metaKeywords, $grabbedMetaKeywords, "Проверка KEYWORDS");
        }
    }

}