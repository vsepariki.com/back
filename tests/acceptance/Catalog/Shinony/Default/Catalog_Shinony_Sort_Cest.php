<?
class Catalog_Shinony_Sort_Cest
{
    /** @var AcceptanceTester */
    protected $tester;

    const RESPONSE_CODE = 200;

    const URL = "/catalog/shinony/?sort=prices_asc";
    const NAME = "Шиньоны для волос";
    const TITLE = "Шиньоны для волос - Сортировка по возрастанию цены";
    const META_DESCRIPTION = "Каталог из 24 моделей в наличии. Низкие цены, доставка по России. Купить шиньон для волос европейского качества - Сортировка по возрастанию цены";

    public function runTests()
    {
        $this->tester->amOnPage(static::URL);
        $this->responseCodeTest(static::RESPONSE_CODE);
        if(static::RESPONSE_CODE == 200){
            $this->titleTest(static::TITLE);
            $this->h1Test(static::NAME);
            $this->metaDescriptionTest(static::META_DESCRIPTION);
        }
    }

    protected function responseCodeTest($responseCode)
    {
        $this->tester->seeResponseCodeIs($responseCode, "Проверка кода ответа");
    }

    protected function titleTest($title)
    {
        if($title) {
            $grabbedTitle = $this->tester->grabTextFrom("//title");
            $this->tester->assertEquals(self::j($title), self::j($grabbedTitle), "Проверка TITLE");
        }
    }

    protected function h1Test($h1)
    {
        if($h1){
            $grabbedH1 = $this->tester->grabTextFrom("//h1");
            $this->tester->assertEquals(self::j($h1), self::j($grabbedH1), "Проверка H1");
        }
    }

    private function metaDescriptionTest($metaDescription)
    {
        if($metaDescription){
            $grabbedMetaDescription = $this->tester->grabTextFrom('//html/head/meta[@name="description"]/@content');
            $this->tester->assertEquals(self::j($metaDescription), self::j($grabbedMetaDescription), "Проверка DESCRIPTION");
        }
    }

    // Justify
    protected static function j(string $string): string
    {
        $string = html_entity_decode($string);
        $string = str_replace(' ', ' ', $string);
        $string = str_replace('–', '-', $string);

        return $string;
    }

    public function _before(AcceptanceTester $I){$this->tester = $I;}
}
