<?
class Catalog_Covers_Sotbit_Cest
{
    /** @var AcceptanceTester */
    protected $tester;

    const RESPONSE_CODE = 200;

    const URL = "/catalog/nakladki/zhenskaya/";
    const NAME = "Накладки волос для женщин";
    const TITLE = "Накладки для волос для женщин на голову: купить недорого с доставкой в Санкт-Петербурге";
    const META_DESCRIPTION = "Накладки для волос для женщин максимально продуманы: легкие, позволяют коже дышать, естественно выглядят. Никто не догадается, что вы их носите. В наличии накладки любых оттенков и размеров, отличаются европейским уровнем качества и представлены по низкой цене.";

    public function runTests()
    {
        $this->tester->amOnPage(static::URL);
        $this->responseCodeTest(static::RESPONSE_CODE);
        if(static::RESPONSE_CODE == 200){
            $this->titleTest(static::TITLE);
            $this->h1Test(static::NAME);
            $this->metaDescriptionTest(static::META_DESCRIPTION);
        }
    }

    protected function responseCodeTest($responseCode)
    {
        $this->tester->seeResponseCodeIs($responseCode, "Проверка кода ответа");
    }

    protected function titleTest($title)
    {
        if($title) {
            $grabbedTitle = $this->tester->grabTextFrom("//title");
            $this->tester->assertEquals(self::j($title), self::j($grabbedTitle), "Проверка TITLE");
        }
    }

    protected function h1Test($h1)
    {
        if($h1){
            $grabbedH1 = $this->tester->grabTextFrom("//h1");
            $this->tester->assertEquals(self::j($h1), self::j($grabbedH1), "Проверка H1");
        }
    }

    private function metaDescriptionTest($metaDescription)
    {
        if($metaDescription){
            $grabbedMetaDescription = $this->tester->grabTextFrom('//html/head/meta[@name="description"]/@content');
            $this->tester->assertEquals(self::j($metaDescription), self::j($grabbedMetaDescription), "Проверка DESCRIPTION");
        }
    }

    // Justify
    protected static function j(string $string): string
    {
        $string = html_entity_decode($string);
        $string = str_replace(' ', ' ', $string);
        $string = str_replace('–', '-', $string);

        return $string;
    }

    public function _before(AcceptanceTester $I){$this->tester = $I;}
}
