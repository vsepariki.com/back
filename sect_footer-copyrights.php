<div class="block-wrap footer-main-copyright s-hidden ">
	<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 m-hidden">
    © Магазин париков "Центр Дизайна волос" 2006 - <?= date('Y') ?>
    <br>
    <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">Соглашения на обработку персональных данных</a>
  </div>
	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width8 block-wrap__item_s-width6 footer-main-copyright__developer">
		<? if($numbPage == 'first'): ?>
		<a class="link-primary" href="https://medreclama.ru/" target="_blank">Агентство медицинского маркетинга "Medreclama"</a><br>Разработка интернет-магазина
		<? else: ?>
		<!-- noindex --><a class="link-primary" href="https://medreclama.ru/" target="_blank">Агентство медицинского маркетинга "Medreclama"</a><!--/ noindex --><br>Разработка интернет-магазина
		<? endif; ?>
	</div>
</div>
