<section class="home-description container">
  <div class="content">
	 <h2 class="home-description__header">Магазин париков в Санкт-Петербурге</h2>
	 <div class="home-advantages home-advantages_mobile xl-hidden l-hidden m-hidden">
		<div class="home-advantages__item">
		  <div class="home-advantages__big home-advantages__big_numbers">14</div>
		  <div class="home-advantages__caption">лет помогаем  бороться с облысением</div>
		</div>
		<div class="home-advantages__item">
		  <div class="home-advantages__big home-advantages__big_numbers">200</div>
		  <div class="home-advantages__caption">париков в салоне для примерки</div>
		</div>
		<div class="home-advantages__item">
		  <div class="home-advantages__caption">Професиональные стилисты подберут</div>
		  <div class="home-advantages__big">новый стиль</div>
		</div>
	 </div>
	 <div class="block-wrap home-description__item block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 home-description__image s-hidden">
		  <div class="home-description__image-inner" itemscope itemtype="http://schema.org/ImageObject">
              <picture>
                <source srcset="/images/home/desc-1.webp">
                <img src="/images/home/desc-1.jpg" alt="" itemprop="contentUrl" loading="lazy" decoding="async"/>
              </picture>
		  </div>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 home-description__text">
		  <p>Волосы и изделия из волос (постижерные изделия) всегда были не только показателями красоты, но и отдельным направлением в моде. Парики и накладки из натуральных и искусственных волос до сих пор пользуются большим спросом как для маскировки проблемных волос, так и для смены имиджа.</p>
		  <p>Салон париков Центра дизайна волос предоставляет большой выбор париков, накладок, шиньонов и хвостов, разнообразных оттенков, объема и длины самого высокого качества и по приемлемым ценам. Мы предлагаем различные марки изделий из волос как натуральных, так и из моноволокна.</p>
		  <div class="home-advantages s-hidden">
			 <div class="home-advantages__item">
				<div class="home-advantages__big home-advantages__big_numbers">14</div>
				<div class="home-advantages__caption">лет помогаем  бороться с облысением</div>
			 </div>
			 <div class="home-advantages__item">
				<div class="home-advantages__big home-advantages__big_numbers">200</div>
				<div class="home-advantages__caption">париков в салоне для примерки</div>
			 </div>
		  </div>
		</div>
	 </div>
	 <div class="block-wrap home-description__item block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 home-description__text">
		  <h3 class="home-description__subheader">Продаем топовые парики из Германии</h3>
		  <p>Мы сотрудничаем с лучшими производителями Европы, всегда в наличии изделия из Германии (Ellen Wille), а также других производителей.</p>
		  <p>Почемы мы выбрали только эти бренды:</p>
		  <ul class="list-marked">
			 <li>естественность и натуральность даже искусственных париков</li>
			 <li>естественная густота</li>
			 <li>естественный окрас</li>
			 <li>вентилируемость кожи головы</li>
			 <li>надежная фиксация</li>
			 <li>невидимая линия роста волос</li>
			 <li>большинство париков сделаны вручную</li>
		  </ul>
		  <div class="home-advantages">
			 <? /* <div class="home-advantages__item home-advantages__item_nj-creation"><a href="">NJ-creation Франция</a>
			 </div> */ ?>
			 <div class="home-advantages__item home-advantages__item_ellenwill"> <a href="">Ellen Wille Германия</a>
			 </div>
		  </div>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 home-description__image s-hidden">
		  <div class="home-description__image-inner" itemscope itemtype="http://schema.org/ImageObject">
              <picture>
                  <source srcset="/images/home/desc-2.webp">
                  <img src="/images/home/desc-2.jpg" alt="" itemprop="contentUrl" loading="lazy" decoding="async"/>
              </picture>
		  </div>
		</div>
	 </div>
	 <div class="block-wrap home-description__item block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 home-description__image">
		  <div class="home-description__image-inner" itemscope itemtype="http://schema.org/ImageObject">

              <picture>
                  <source srcset="/images/home/desc-3.webp">
                  <img src="/images/home/desc-3.jpg" alt="" itemprop="contentUrl" loading="lazy" decoding="async"/>
              </picture>

		  </div>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 home-description__text">
		  <h3 class="home-description__subheader">Примеряете парики в персональном кабинете</h3>
		  <p>Помещения нашего магазина оборудованы для комфортной примерки изделий. Вы можете быть уверены в тактичности и профессионализме наших сотрудников. А также опытные мастера готовы не только помочь в выборе наиболее подходящего изделия, но и подстричь или сделать укладку на парике или накладке из натуральных волос.</p>
		  <div class="home-advantages s-hidden">
			 <div class="home-advantages__item">
				<div class="home-advantages__caption">Профессиональные стилисты подберут</div>
				<div class="home-advantages__big">новый стиль</div>
			 </div>
		  </div>
		</div>
	 </div>
	 <div class="block-wrap home-description__item block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 home-description__text">
		  <h3 class="home-description__subheader">Доставляем парики по России</h3>
		  <p>Отправим почтой России до ближайщего к вам отделения.
			 <br>Также возможно доставка на дом. Доставку осуществляют
			 <br>курьеры с 8:00 до 16:00 по рабочим дням.
		  </p>
		  <p>
			 Адрес в Санкт-Петербурге: наб. Обводного канала, 108А<br>Работаем ежедневно с 10 до 21. Приходите на примерку парика.</p>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 home-description__image s-hidden">
		  <div class="home-description__image-inner" itemscope itemtype="http://schema.org/ImageObject" loading="lazy" decoding="async">

              <picture>
                  <source srcset="/images/home/desc-4.webp">
                  <img src="/images/home/desc-4.jpg" alt="" itemprop="contentUrl"/>
              </picture>

		  </div>
		</div>
	 </div>
  </div>
</section>
