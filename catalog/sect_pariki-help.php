<div class="aside-faq s-hidden">

	<div class="aside-faq__title">Помощь в выборе парика</div>

	<div class="aside-faq__item"><a class="link-primary" href="/stati/polnyy-gid-po-vyboru-parika.html">Как подобрать парик</a></div>
	<div class="aside-faq__item"><a class="link-primary" href="/stati/chem-otlichayutsya-osnovy-parika.html">Чем отличаются основы</a></div>
	<div class="aside-faq__item"><a class="link-primary" href="/stati/chem-otlichayutsya-iskustvennye-pariki-ot-naturalnykh.html">Чем отличаются натуральные и искусственные  волосы в парике</a></div>
</div>
