<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Магазин шиньонов в Спб: купить шиньон для волос по низкой цене");
$APPLICATION->SetPageProperty("description", "Магазин хвостов и шиньонов любых размеров и цветов в Спб. Европейское качество по низкое цене. Купить шиньоны из натуральных и искусственных волос.");
$APPLICATION->SetTitle("Шиньоны");?>          <div class="block-wrap  block-wrap_wrap ">
            <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6">
              <p>Продаем шиньоны и хвосты из коллекций французской марки NJ Creation и немецкой Ellen Wille.</p>
              <p>Шиньоны — это женская прическа из накладных волос, подобранных на затылке.</p>
              <p>В наличии шиньоны из натуральных, либо искусственных волос, способных украсить любую женщину. Шиньоны, как правило, используют в качестве дополнения к собственной прическе, чтобы придать ей особый шик. Кроме того, шиньоны применяются для увеличения густоты волос, а при необходимости и длины. </p>
              <p>Благодаря шиньону смена имиджа за считанные минуты доступна каждому.</p>
            </div>
          </div>
          <h2>Шиньоны немецкого производителя Ellen Wille</h2>
          <div class="block-wrap  block-wrap_wrap ">
            <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3">
              <figure class="image-desc" itemscope itemtype="http://schema.org/ImageObject" itemscope itemtype="http://schema.org/ImageObject"><img class="image-desc__img" src="/images/ponytail/hip-look.jpg" alt="Hip Look (хвост на заколке-краб) " title="Hip Look (хвост на заколке-краб) " itemprop="contentUrl"/>
                <figcaption class="image-desc__desc" itemprop="caption" itemprop="caption">Хвост на заколке-краб Annica Hansen Hip Look</figcaption>
              </figure>
            </div>
            <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3">
              <figure class="image-desc" itemscope itemtype="http://schema.org/ImageObject"><img class="image-desc__img" src="/images/ponytail/ready-look.jpg" alt="Annica Hansen Ready look (трессовый шиньон на затылок для доп.объема и длины)" title="Annica Hansen Ready look (трессовый шиньон на затылок для доп.объема и длины)" itemprop="contentUrl"/>
                <figcaption class="image-desc__desc" itemprop="caption">Трессовый шиньон на затылок  Annica Hansen Ready look (для доп.объема и длины) </figcaption>
              </figure>
            </div>
            <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3">
              <figure class="image-desc" itemscope itemtype="http://schema.org/ImageObject"><img class="image-desc__img" src="/images/ponytail/aqua.jpg" alt="Power Pieces Aqua" title="Power Pieces Aqua" itemprop="contentUrl"/>
                <figcaption class="image-desc__desc" itemprop="caption">Хвост Power Pieces Aqua</figcaption>
              </figure>
            </div>
            <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3">
              <figure class="image-desc" itemscope itemtype="http://schema.org/ImageObject"><img class="image-desc__img" src="/images/ponytail/pinot.jpg" alt="Power Pieces Pinot" title="Power Pieces Pinot" itemprop="contentUrl"/>
                <figcaption class="image-desc__desc" itemprop="caption">Трессовый шиньон Power Pieces Pinot для создания высокой прически</figcaption>
              </figure>
            </div>
          </div>
          <h2>Шиньоны французского производителя NJ Creation</h2>
          <p>Самые популярные шиньоны в форме хвоста, сделанного из длинных волос. Хвост это разновидность шиньона. </p>
          <div class="block-wrap  block-wrap_wrap ">
            <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width3">
              <figure class="image-desc" itemscope itemtype="http://schema.org/ImageObject"><img class="image-desc__img" src="/images/ponytail/praline.jpg" alt="Praline кудрявые хвосты" title="Praline кудрявые хвосты" itemprop="contentUrl"/>
                <figcaption class="image-desc__desc" itemprop="caption">Praline кудрявые хвосты</figcaption>
              </figure>
            </div>
            <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width3">
              <figure class="image-desc" itemscope itemtype="http://schema.org/ImageObject"><img class="image-desc__img" src="/images/ponytail/reglisse.jpg" alt="Reglisse длинные хвосты" title="Reglisse длинные хвосты" itemprop="contentUrl"/>
                <figcaption class="image-desc__desc" itemprop="caption">Reglisse длинные хвосты</figcaption>
              </figure>
            </div>
            <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width3">
              <figure class="image-desc" itemscope itemtype="http://schema.org/ImageObject"><img class="image-desc__img" src="/images/ponytail/marrakech.jpg" alt="Marrakech темные хвосты" title="Marrakech темные хвосты" itemprop="contentUrl"/>
                <figcaption class="image-desc__desc" itemprop="caption">Marrakech темные хвосты</figcaption>
              </figure>
            </div>
          </div>
          <div class="text-highlighted">
            <p><strong>В магазине еще больше моделей, цветов, длин шиньонов.</strong></p>
            <p>Цены на изделия зависят от длины волос и фасона изделия. В наличии популярные цвета блонда, темных, седых, рыжих оттенков.</p>
          </div>
          <div class="block-wrap  block-wrap_wrap ">
            <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6">
              <h2>Приглашаем примерить и купить шиньоны в магазин в Санкт-Петербурге</h2>
              <p>Приглашаем всех заинтересованных на бесплатную консультацию по постижерным изделиям. Наш адрес: г Санкт-Петербург, набережная обводного канала 108. Магазин шиньонов и хвостов открыт с 10 утра до 9 вечера, ежедневно.</p>
              <p>Звоните, проконсультируем по наличию цветов и размеров шиньонов:<br><strong>+7 (812) 386-11-62</strong></p><script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=0jFSeud2k6YiSEPpq1L9FIjWP8F6PbZr&amp;height=370&amp;lang=ru_RU&amp;sourceType=constructor"></script> 
            </div>
          </div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
