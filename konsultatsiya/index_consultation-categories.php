<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<aside class="section-consultation-inner-aside">

    <div class="consultation-categories">

        <p class="consultation-categories-header">Категории</p>

        <div class="consultation-categories-items">

            <? $APPLICATION->IncludeComponent("bitrix:menu", "consultation-categories", Array(
                "ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
                    "MAX_LEVEL" => "2",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "top_sub",	// Тип меню для остальных уровней
                    "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                ),
                false
            ); ?>

        </div> <!-- consultation-categories-items -->

    </div> <!-- consultation-categories -->

</aside> <!-- section-consultation-inner-aside -->