<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="consultation-askBlock">
    <div class="consultation-askBlock-left">
		<p>Отвечаем на вопросы про парики, как за ними ухаживать, носить, какие есть альтернативные способы.</p>
		<p>Если Вы не нашли ответ на свой вопрос, обратитесь за консультацией к нашим специалистам.</p>
    </div>
    <div class="consultation-askBlock-right">
        <a href="/konsultatsiya/ask.html" class="btn">Спросить совет</a>
        <p>Советуем с 2007 года</p>
    </div>
</div>
