<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();

global $APPLICATION;

if(CModule::IncludeModule("iblock")) {

    $IBLOCK_ID = 11; // указываем инфоблок с элементами ("Консультация")

    $arOrder = Array("SORT"=>"DESC");
    $arSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL");
    $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y");
    $res = CIBlockSection::GetList($arOrder, $arFilter, true);

    while($sectionObj = $res->GetNext()) // наполняем массив меню пунктами меню
    {
         $aMenuLinksExt[] = Array(
            $sectionObj["NAME"],
            $sectionObj["SECTION_PAGE_URL"],
            Array(),
            Array("ELEMENT_CNT" => $sectionObj["ELEMENT_CNT"]),
            ""
        );
    }

} // endif

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt); // меню сформировано