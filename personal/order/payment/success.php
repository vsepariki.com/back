<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата получена");
$hideRecentlyViewedProducts = true;
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/bootstrap.css");
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/font-awesome.css");

$orderId = (int)$_GET["InvId"];
$order = CSaleOrder::GetByID($orderId);

$cartProducts = getProductsInOrderByOrderId($orderId);
$isAllCartProductsAvailable = isAllCartProductsAvailable($cartProducts);
?>

    <div class="block-wrap block-wrap_wrap">

        <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
            <p>Ваш заказ <b>№<?= $_GET["InvId"] ?></b> успешно оплачен. Заказ передан в обработку.</p>

            <? if($order["DELIVERY_ID"] != 3): // Delivery id == 3 - Самовывоз ?>
                <p>Наши администраторы в ближайшее время <b>свяжутся с вами</b> для подтверждения заказа и уточнения адреса доставки.</p>
            <? else: ?>
                <? if($isAllCartProductsAvailable): ?>
                    <p>
                        Вы можете приехать за заказом в наш магазин по адресу: набережная Обводного канала д. 108.<br>
                        Время работы: ежедневно с 10 до 21.
                    </p>
                <? else: ?>
                    <p>Наши администраторы в ближайшее время <b>свяжутся с вами</b> для подтверждения заказа.</p>
                <? endif ?>
            <? endif ?>

            <p>Вы можете следить за выполнением своего заказа в <a href="/personal/order/">Персональном разделе сайта.</a><br>
            Обратите внимание, что для входа в этот раздел вам необходимо будет ввести логин и пароль пользователя сайта.</p>
        </div>


        <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width6">
            <div class="bx-sidebar-block">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "personal_menu", array(
                    "ROOT_MENU_TYPE" => "personal",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_TYPE" => "A",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(),
                ),
                    false
                );?>
            </div>
        </div>

    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>