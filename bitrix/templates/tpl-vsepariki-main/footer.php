<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>
<?
if (defined('CATEGORY')) {

$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"related",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("ID","NAME","PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_TEXT","DETAIL_PICTURE","SHOW_COUNTER"),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "INFORMATION",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "6",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => CATEGORY,
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("VOTED","vote_count",""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "RAND",
		"SORT_BY2" => "RAND",
		"SORT_ORDER1" => "RAND",
		"SORT_ORDER2" => "RAND",
		"STRICT_SECTION_CHECK" => "N"
	)
);
}?>
		<? if($numbPage == 'all'):

			if(empty($hideRecentlyViewedProducts)){
				blockInclude('footer', 'footer-recently-viewed-products');
			}
			?>

			</div> <!-- /content -->
		</div> <!-- /container -->

		<? endif; ?>

		</main>

		<footer class="footer-main">
			<div class="container">
				<div class="content">

					<div class="block-wrap  block-wrap_wrap">
						<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6 s-hidden footer-main__left">
							<div class="block-wrap footer-main-navigation ">
								<? blockInclude('footer', 'footer-menu-catalog'); ?>
								<? blockInclude('footer', 'footer-menu-service'); ?>
								<? blockInclude('footer', 'footer-menu-help'); ?>
							</div>
						</div>
						<? blockInclude('footer', 'footer-contacts'); ?>
					</div>

					<? blockInclude('footer', 'footer-payments'); ?>

					<? blockInclude('footer', 'footer-copyrights'); ?>

				</div> <!-- content -->
			</div> <!-- container -->

		</footer>

		<? blockInclude('footer', 'footer-counters') ?>
        <? blockInclude('footer', 'footer-styles') ?>
		<? blockInclude('footer', 'footer-scripts') ?>

	</body>



</html>
