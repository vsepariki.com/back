<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$offersColors = array();

$fullSeoName = $arItem["NAME"];
if($titleForSite){
    $fullSeoName .= " ".$titleForSite;
}

$pictureType = "default";
if(!empty($arItem['PREVIEW_PICTURE']['SRC'])){
    $pictureType = 'PREVIEW_PICTURE';
}elseif(!empty($arItem['DETAIL_PICTURE']['SRC'])){
    $pictureType = 'DETAIL_PICTURE';
}
$arSelect = Array("PROPERTY_139","PROPERTY_140");
$arFilter = Array("IBLOCK_ID"=>9,  "ACTIVE"=>"Y", 'ID' => $arItem['ID']);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$id = '';
$custom_price = '';
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $id = $arFields['PROPERTY_139_VALUE'];
    $custom_price = $arFields['PROPERTY_140_VALUE'];
}

$webPHelper = new WebPHelper;


?>
<?php
?>
<div class="catalog-product__offer">

    <a class="catalog-product__image" href="<?= $arItem['DETAIL_PAGE_URL']; ?>">

        <?php if($pictureType == "default"): ?>
            <img
                src="<?= $defaultProductImgSrc ?>"
                alt=""
            >
        <?php else: ?>
            <picture>
                <source
                    <?php if($ifActive): ?>
                        srcset="<?= $webPHelper->getOrCreateWebp($arItem[$pictureType]['SRC']) ?>"
                    <?php else: ?>
                        srcset=""
                        data-srcset="<?= $webPHelper->getOrCreateWebp($arItem[$pictureType]['SRC']) ?>"
                    <?php endif ?>
                >
                <img
                    <?php if($ifActive): ?>
                        src="<?= $arItem[$pictureType]['SRC'] ?>"
                    <?php else: ?>
                        src=""
                        data-src="<?= $arItem[$pictureType]['SRC'] ?>"
                    <?php endif ?>

                    alt="<?= $fullSeoName ?>"
                    title="<?= $fullSeoName ?>"

                    width="<?= $arItem[$pictureType]['WIDTH'] ?>"
                    height="<?= $arItem[$pictureType]['HEIGHT'] ?>"

                    loading="lazy"
                    decoding="async"
                >
            </picture>
            <?php if (!empty($id)){?>
                <div class="catalog-product__discount"><?=$id?></div>

            <?php }?>
        <?php endif ?>

    </a>

    <div class="catalog-product__name">
        <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>"><?= $productName ?></a>
    </div>

    <div class="catalog-product__prices">


        <?php if (!empty($custom_price)){?>
            <div class="catalog-product__old-price">

                <?= number_format($arItem["CATALOG_PRICE_1"], 0, ',', ' ') ?>  руб.

            </div>
            <? /* <? echo "&nbsp";
            ?> */ ?>

            <div class="catalog-product__price">

                <?=$custom_price?>
            </div>


        <?php } else { ?>
            <div class="catalog-product__old-price">
                <?php
                $price = $arItem["CATALOG_PRICE_1"] + ($arItem["CATALOG_PRICE_1"] * 0.2);
                $price = number_format($price, 0, ',', ' ')

                ?>

                <?= $price ?>  руб.
            </div>
            <? /*  echo "&nbsp";
            */ ?>
            <div class="catalog-product__price">

                <?= number_format($arItem["CATALOG_PRICE_1"], 0, ',', ' ') ?>  руб.
            </div>

        <?php }?>
    </div>

</div> <!-- catalog-product__offer -->

<?php if(count($arItem["OFFERS"])): ?>

    <div class="catalog-product__colors">
        <div class="catalog-product-colors">
            <ul class="catalog-product-colors__list">
                <?php foreach($arItem["OFFERS"] as $offer): ?>
                    <li class="catalog-product-colors__item">
                        <img
                            src="#"
                            data-src="<?= $offer['colorFileSrc'] ?>"
                            alt=""
                            title="<?= $offer['colorTitle'] ?>"
                        >
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
<?php endif ?>
