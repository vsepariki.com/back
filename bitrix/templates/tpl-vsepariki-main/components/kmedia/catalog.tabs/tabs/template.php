<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$defaultProductImgSrc = '/upload/medialibrary/baa/baaa03452391641f164ed2e95b71384f.jpg';
?>
<div class="home-catalog__tabs-container">

	<div class="home-catalog__mobile-switch" id="home-catalog__mobile-switch"></div>

	<div class="home-catalog__tabs">
	<?
		foreach(array_keys($arResult["TABS_ITEMS"]) as $key => $tabName):
            $class = "home-catalog__tab";
            if(!$key){
                $class .= " home-catalog__tab_active";
            }
            ?>
            <div
                class="<?= $class ?>"
                data-home-catalog-tab="home-catalog-tab<?= ($key + 1) ?>"
            >
                <?= $tabName ?>
            </div>
        <? endforeach ?>
	</div>

	<div class="home-catalog__tabs-content">
	<?

    $counter = 0;
    foreach($arResult["TABS_ITEMS"] as $tabItem) {
        $counter++;
        $ifActive = ($counter == 1) ? ' home-catalog__tab-content_active' : '';
        ?>
        <div
            class="home-catalog__tab-content<?= $ifActive; ?>"
            data-home-catalog-tab="home-catalog-tab<?= $counter; ?>"
        >
            <div class="block-wrap home-catalog-list block-wrap_wrap ">
            <?
            foreach ($tabItem as $arItem) {

                $titleForSite = "";
                if(empty($arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])){
                    $productName = $arItem["NAME"];
                }else{
                    $titleForSite = $arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
                    $productName = $arItem["NAME"]." | ".$titleForSite;
                }
                ?>
                <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width6 block-wrap__item_s-width3 catalog-list__item">
                    <div class="catalog-product">
                        <? include __DIR__.'/_list-product.php' ?>
                    </div>
                </div>
                <?
            }
            ?>
            </div>
        </div>
        <?
        }
	?>
	</div>

</div>
