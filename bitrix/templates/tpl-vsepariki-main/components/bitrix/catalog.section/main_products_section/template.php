<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;

$defaultProductImgSrc = '/upload/medialibrary/baa/baaa03452391641f164ed2e95b71384f.jpg';

$similarClass = "";
$similarTitle = "";


if (!empty($arResult['ITEMS'])) {
?>
<div class="similar-<?= $similarClass ?>">

	<h2><?= Loc::getMessage('GENERAL_TITLE') ?></h2>

	<div class="block-wrap product-similar block-wrap_wrap ">
		<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
			<?
			$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
			$arFilter = Array("IBLOCK_ID"=>IntVal($arItem["IBLOCK_ID"]), "ID"=>IntVal($arItem['ID']));
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$arItem['DETAIL_PAGE_URL'] =  $arFields['DETAIL_PAGE_URL'];
			}
			
			
			$titleForSite = "";
			if(empty($arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])){
				$productName = $arItem["NAME"];
			}else{
				$titleForSite = $arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
				$productName = $arItem["NAME"]." | ".$titleForSite;
			}
			?>
			
			<div class="block-wrap__item block-wrap__item_xl-width2 block-wrap__item_l-width2 block-wrap__item_m-width3 block-wrap__item_s-width3 product-similar__item">
				<div class="catalog-product">
				
				<?
					if(!empty($arItem["OFFERS"])){
						 include __DIR__.'/_list-product-complex.php';
					}
					else{
						 include __DIR__.'/_list-product-simple.php';
					}
				?>
				</div>
			</div>
		<? endforeach; ?>
	</div>
</div>
<?
}
?>
