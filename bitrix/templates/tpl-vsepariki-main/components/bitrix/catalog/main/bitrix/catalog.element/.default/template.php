<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$deliveryText = getDeliveryTextByProductId($arResult["ID"]);

$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BASIS_PRICE' => $strMainID.'_basis_price',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
	'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
	: $arResult['NAME']
);
$strAlt = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
	: $arResult['NAME']
);
?>

<div
    class="bx_item_detail <? echo $templateData['TEMPLATE_CLASS']; ?>"
    id="<? echo $arItemIDs['ID']; ?>"
    itemscope itemtype="http://schema.org/Product"
>
    <meta itemprop="name" content="<?= $arResult["NAME"] ?>">

<?
if ('Y' == $arParams['DISPLAY_NAME'])
{
?>
<div class="bx_item_title"><h1><span><?
	echo (
		isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != ''
		? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
		: $arResult["NAME"]
	); ?>
</span></h1></div>
<?
}

reset($arResult['MORE_PHOTO']);
$arFirstPhoto = current($arResult['MORE_PHOTO']);

$fullSeoName = $arResult["NAME"];
if(!empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])){
    $fullSeoName .= " ".$arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
}
?>
<div class="block-wrap  block-wrap_wrap ">

	<div
	    class="
	        block-wrap__item
            block-wrap__item_xl-width4
            block-wrap__item_l-width4
            block-wrap__item_m-width4
            block-wrap__item_s-width6
        "
    >

        <div class="bx_item_slider" id="<? echo $arItemIDs['BIG_SLIDER_ID']; ?>">
            <div class="bx_bigimages" id="<? echo $arItemIDs['BIG_IMG_CONT_ID']; ?>">
                <div class="bx_bigimages_imgcontainer">
                    <span class="bx_bigimages_aligner">
                        <img
                            id="<?= $arItemIDs['PICT'] ?>"
                            src="<?= $arFirstPhoto['SRC'] ?>"
                            alt="<?= $fullSeoName ?>"
                            title="<?= $fullSeoName ?>"
                            itemprop="image"
                        >
                          <?php if (!empty($arResult['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'])){?>
                              <div class="product-discount"><?php echo $arResult['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT']?>%</div>

                          <?}?>
                    </span>

                    <?
                    if ('Y' == $arParams['SHOW_DISCOUNT_PERCENT'])
                    {
                        if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS']))
                        {
                            if (0 < $arResult['MIN_PRICE']['DISCOUNT_DIFF'])
                            {
                            ?>
                            <div class="bx_stick_disc right bottom" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>"><? echo -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%</div>
                            <?
                            }
                        }
                        else
                        {
                        ?>
                            <div class="bx_stick_disc right bottom" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>" style="display: none;"></div>
                        <?
                        }
                    }
                    ?>
                    <div class="bx_stick average left top" <?= (empty($arResult['LABEL'])? 'style="display:none;"' : '' ) ?> id="<? echo $arItemIDs['STICKER_ID'] ?>" title="<? echo $arResult['LABEL_VALUE']; ?>"><? echo $arResult['LABEL_VALUE']; ?></div>
                </div>
            </div>
        <?
        if ($arResult['SHOW_SLIDER'])
        {
            if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS']))
            {
                if (5 < $arResult['MORE_PHOTO_COUNT'])
                {
                    $strClass = 'bx_slider_conteiner full';
                    $strOneWidth = (100/$arResult['MORE_PHOTO_COUNT']).'%';
                    $strWidth = (20*$arResult['MORE_PHOTO_COUNT']).'%';
                    $strSlideStyle = '';
                }
                else
                {
                    $strClass = 'bx_slider_conteiner';
                    $strOneWidth = '20%';
                    $strWidth = '100%';
                    $strSlideStyle = 'display: none;';
                }
        ?>
            <div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['SLIDER_CONT_ID']; ?>">
            <div class="bx_slider_scroller_container">
            <div class="bx_slide">
            <ul style="width: <? echo $strWidth; ?>;" id="<? echo $arItemIDs['SLIDER_LIST']; ?>">
        <?
                foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto)
                {
        ?>
            <li data-value="<? echo $arOnePhoto['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>;"><span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span></li>
        <?
                }
                unset($arOnePhoto);
        ?>
            </ul>
            </div>
            <div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
            <div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
            </div>
            </div>
        <?
            }
            else
            {
                foreach ($arResult['OFFERS'] as $key => $arOneOffer)
                {
                    if (!isset($arOneOffer['MORE_PHOTO_COUNT']) || 0 >= $arOneOffer['MORE_PHOTO_COUNT'])
                        continue;
                    $strVisible = ($key == $arResult['OFFERS_SELECTED'] ? '' : 'none');
                    if (5 < $arOneOffer['MORE_PHOTO_COUNT'])
                    {
                        $strClass = 'bx_slider_conteiner full';
                        $strOneWidth = (100/$arOneOffer['MORE_PHOTO_COUNT']).'%';
                        $strWidth = (20*$arOneOffer['MORE_PHOTO_COUNT']).'%';
                        $strSlideStyle = '';
                    }
                    else
                    {
                        $strClass = 'bx_slider_conteiner';
                        $strOneWidth = '20%';
                        $strWidth = '100%';
                        $strSlideStyle = 'display: none;';
                    }
        ?>
            <div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['SLIDER_CONT_OF_ID'].$arOneOffer['ID']; ?>" style="display: <? echo $strVisible; ?>;">
            <div class="bx_slider_scroller_container">
            <div class="bx_slide">
            <ul style="width: <? echo $strWidth; ?>;" id="<? echo $arItemIDs['SLIDER_LIST_OF_ID'].$arOneOffer['ID']; ?>">
        <?
                    foreach ($arOneOffer['MORE_PHOTO'] as &$arOnePhoto)
                    {
        ?>
            <li data-value="<? echo $arOneOffer['ID'].'_'.$arOnePhoto['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>"><span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span></li>
        <?
                    }
                    unset($arOnePhoto);
        ?>
            </ul>
            </div>
            <div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT_OF_ID'].$arOneOffer['ID'] ?>" style="<? echo $strSlideStyle; ?>" data-value="<? echo $arOneOffer['ID']; ?>"></div>
            <div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT_OF_ID'].$arOneOffer['ID'] ?>" style="<? echo $strSlideStyle; ?>" data-value="<? echo $arOneOffer['ID']; ?>"></div>
            </div>
            </div>
        <?
                }
            }
        }
        ?>
        </div>

	<? if(!empty($arResult['DISPLAY_PROPERTIES']["VIDEO"]["VALUE"])) ?>
	<div class="bx_item_video">
		<?= $arResult['DISPLAY_PROPERTIES']["VIDEO"]["~VALUE"]; ?>
	</div>

</div> <!-- block-wrap__item FOTO -->

<div
    class="
        block-wrap__item
        block-wrap__item_xl-width8
        block-wrap__item_l-width8
        block-wrap__item_m-width8
        block-wrap__item_s-width6
    "
>

		<div class="product-articul">
			Артикул: <span><?= $arResult["ID"] ?></span>
		</div>

	  <div class="item_info_section product-top">
			<? include "_prices.php"; ?>
		</div> <!-- product-top -->

	<div id="productAvailabilityWrapper">
		<div class="item_info_section product-availability">
		<?
			if(empty($arResult['OFFERS'])){
				$productQty = $arResult['CATALOG_QUANTITY'];
				if($productQty > 0){ ?>
					<div class="product-availability_yes">
					    В наличии: заказ будет отправлен завтра или можно забрать сегодня в магазине
                    </div>
				<?
				} else {
					?>
					<div class="product-availability_no">
					    <?= $deliveryText ?>
                    </div>
					<?
				}
			}else{
				foreach($arResult['OFFERS'] as $offerItem){
					$productQty = $offerItem['CATALOG_QUANTITY'];
					if($productQty > 0) { ?>
						<div class="product-availability_yes product-offer-availability" id="product-offer-availability-<?= $offerItem["ID"] ?>">
							В наличии: заказ будет отправлен завтра или можно забрать сегодня в магазине
						</div>
						<?
					}else{ ?>
						<div class="product-availability_no product-offer-availability" id="product-offer-availability-<?= $offerItem["ID"] ?>">
                            Под заказ<? /* < ?= $deliveryText ?> */ ?>
						</div>
						<?
					}
				}
			}
		?>
		</div>
	</div>

<?
unset($minPrice);

if ('' != $arResult['PREVIEW_TEXT'])
{
if (
	'S' == $arParams['DISPLAY_PREVIEW_TEXT_MODE']
	|| ('E' == $arParams['DISPLAY_PREVIEW_TEXT_MODE'] && '' == $arResult['DETAIL_TEXT'])
)
{
?>
<div class="item_info_section">
<?
	echo ('html' == $arResult['PREVIEW_TEXT_TYPE'] ? $arResult['PREVIEW_TEXT'] : '<p>'.$arResult['PREVIEW_TEXT'].'</p>');
?>
</div>
<?
}
}

$APPLICATION->IncludeComponent("bitrix:catalog.brandblock", "", array(
"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
"IBLOCK_ID" => $arParams['IBLOCK_ID'],
"ELEMENT_ID" => $arResult['ID'],
"ELEMENT_CODE" => "",
"PROP_CODE" => "COLOR",
"CACHE_TYPE" => $arParams['CACHE_TYPE'],
"CACHE_TIME" => $arParams['CACHE_TIME'],
"CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
"WIDTH" => "",
"HEIGHT" => "",
),
$component,
array("HIDE_ICONS" => "Y")
);

if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) && !empty($arResult['OFFERS_PROP']))
{
$arSkuProps = array();
?>



<div
    class="item_info_section item_info_section_colours"
    id="<? echo $arItemIDs['PROP_DIV']; ?>"
>
<?
foreach ($arResult['SKU_PROPS'] as &$arProp)
{
	if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]))
		continue;
	$arSkuProps[] = array(
		'ID' => $arProp['ID'],
		'SHOW_MODE' => $arProp['SHOW_MODE'],
		'VALUES_COUNT' => $arProp['VALUES_COUNT']
	);
	if ('TEXT' == $arProp['SHOW_MODE'])
	{
		if (5 < $arProp['VALUES_COUNT'])
		{
			$strClass = 'bx_item_detail_size full';
			$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
			$strWidth = (20*$arProp['VALUES_COUNT']).'%';
			$strSlideStyle = '';
		}
		else
		{
			$strClass = 'bx_item_detail_size';
			$strOneWidth = '20%';
			$strWidth = '100%';
			$strSlideStyle = 'display: none;';
		}
?>
<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
	<span class="bx_item_section_name_gray"><? echo htmlspecialcharsEx($arProp['NAME']); ?></span>
	<div class="bx_size_scroller_container"><div class="bx_size">
		<ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;margin-left:0%;">
<?
		foreach ($arProp['VALUES'] as $arOneValue)
		{
			$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
?>
<li data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID']; ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" style="width: <? echo $strOneWidth; ?>; display: none;">
<i title="<? echo $arOneValue['NAME']; ?>"></i><span class="cnt" title="<? echo $arOneValue['NAME']; ?>"><? echo $arOneValue['NAME']; ?></span></li>
<?
		}
?>
		</ul>
		</div>
		<div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
		<div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
	</div>
</div>
<?
	}
	elseif ('PICT' == $arProp['SHOW_MODE'])
	{
		if (5 < $arProp['VALUES_COUNT'])
		{
			$strClass = 'bx_item_detail_scu full';
			$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
			$strWidth = (20*$arProp['VALUES_COUNT']).'%';
			$strSlideStyle = '';
		}
		else
		{
			$strClass = 'bx_item_detail_scu';
			$strOneWidth = '20%';
			$strWidth = '100%';
			$strSlideStyle = 'display: none;';
		}
?>
<div class="<?= $strClass; ?>" id="<?= $arItemIDs['PROP'].$arProp['ID']; ?>_cont">

		<span id="offerColorName" class="bx_item_section_name_gray">
			<?= htmlspecialcharsEx($arProp['NAME']) ?>
		</span>

	<?
	/*
	if($arResult['SECTION']['ID'] == WIGS_SECTION_ID): ?>
		<a class="link-interactive block-wrap__item product-palette pull-right" href="#product-color-modal">
			<span>Как выбрать цвет</span>
		</a>
	<?
		$APPLICATION->IncludeFile(
			'/catalog/product_pariki-help.html',
			Array(),
			Array("MODE"=>"html")
		);
	endif;
	*/
	?>

	<?
	$propCode = false;
	if(
        $arResult["IBLOCK_SECTION_ID"] == WIGS_SECTION_ID
        || $arResult["IBLOCK_SECTION_ID"] == TEST_SECTION_ID
    ){
		$propCode = "SKU_SORT_COLOR";
	}
	if($arResult["IBLOCK_SECTION_ID"] == HATS_SECTION_ID){
		$propCode = "SKU_SORT_COLOR_HAT";
	}
	if($arResult["IBLOCK_SECTION_ID"] == COVERS_SECTION_ID){
		$propCode = "SKU_SORT_COLOR_COVERS";
	}
	if($arResult["IBLOCK_SECTION_ID"] == SHINONY_SECTION_ID){
		$propCode = "SKU_SORT_COLOR_SHINONY";
	}


	// Если товар Парик, Головной убор или Накладка + есть отсортированные по количеству предложения
	if($propCode && !empty($arResult["sortedOffers"])){

		// $arPropSorted - Отсортированная по доступному количеству копия слайдов + пометка о доступности
		$arPropSorted = [];

		foreach($arResult["sortedOffers"] as $sortedOffer){
			foreach($arProp['VALUES'] as $k => $arOneValue){
				if($sortedOffer[$propCode] == $arOneValue["SORT"]){
					$arPropSorted[$k] = $arOneValue;
					if($sortedOffer['CATALOG_QUANTITY'] <= 0){
						$arPropSorted[$k]["availability"] = "not_available";
					}else{
						$arPropSorted[$k]["availability"] = "available";
					}
				}
			}
		}

		if($arPropSorted){
			$arProp['VALUES'] = $arPropSorted;
		}

	} // endif
	?>

	<div class="bx_scu_scroller_container">
		<div class="bx_scu">
			<ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;margin-left:0%;">
			<?
            $count = 0;
            foreach ($arProp['VALUES'] as  $arOneValue)
			{
				$availability = $arOneValue["availability"];
				$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
				?>
				<li
					data-treevalue="<?= $arProp['ID'].'_'.$arOneValue['ID'] ?>"
					data-onevalue="<?= $arOneValue['ID']; ?>"
					style="width: <?= $strOneWidth; ?>; padding-top: <?= $strOneWidth; ?>; display: none;"
				>

					<i title="<?= $arOneValue['NAME']; ?>"></i>
					<span class="cnt <?= $availability ?>">
						<span
							class="cnt_item"
							style="background-image:url('<?= $arOneValue['PICT']['SRC']; ?>');"
							title="<?= $arOneValue['NAME']; ?>"
                            data-id="<?=$arResult['SKU_PROPS']['VALUES']['OFFER_ID'][$count]?>"
						>
						</span>
					</span>

					<? if($availability == "available"): ?>
						<div class="availability-sign">в наличии</div>

					<? endif ?>

				</li>
				<?
                $count++;
			}

			?>
			</ul>
		</div>
		<div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
		<div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
	</div>

	<div id="select_color_first">пожалуйста, выберите цвет</div>

</div>

<? if(!isProductAvailableInRussia($arResult)): ?>
<p class="product-notice product-notice-unavailable">
    Данная модель временно не&nbsp;доступна для доставки в&nbsp;Россию. Обратитесь&nbsp;к <a href="whatsapp://send?phone=79215710124&amp;text=" target="_blank">нашему менеджеру</a>,
    чтобы подобрать аналогичную модель другого бренда.
</p>
<? endif ?>

		<?
	}
}
unset($arProp);
?>
</div>
<?
}
?>
<div class="item_info_section">
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
$canBuy = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
}
else
{
$canBuy = $arResult['CAN_BUY'];
}
$buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
$addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
$notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);

if($arResult['CATALOG_SUBSCRIBE'] == 'Y')
$showSubscribeBtn = true;
else
$showSubscribeBtn = false;
$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));

if ($arParams['USE_PRODUCT_QUANTITY'] == 'Y')
{
if ($arParams['SHOW_BASIS_PRICE'] == 'Y')
{
	$basisPriceInfo = array(
		'#PRICE#' => $arResult['MIN_BASIS_PRICE']['PRINT_DISCOUNT_VALUE'],
		'#MEASURE#' => (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : '')
	);
?>
	<p id="<? echo $arItemIDs['BASIS_PRICE']; ?>" class="item_section_name_gray"><? echo GetMessage('CT_BCE_CATALOG_MESS_BASIS_PRICE', $basisPriceInfo); ?></p>
<?
}
?>
<?
if (($arResult['PRODUCT']['QUANTITY'] == 0) && (count($arResult['OFFERS']) == 0)) {?>
<style>
.item_buttons_counter_block {display:none !important}
.item_section_name_gray {display:none !important}
.item_section_name_gray {display:none !important}
</style>
<?}?>

<span class="item_section_name_gray" id="quantityBlockLabel"><? echo GetMessage('CATALOG_QUANTITY'); ?></span>
<div class="item_buttons vam">

	<span class="item_buttons_counter_block" id="quantityBlock">
		<a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>">-</a>
		<input id="<? echo $arItemIDs['QUANTITY']; ?>" type="text" class="tac transparent_input" value="<? echo (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])
				? 1
				: $arResult['CATALOG_MEASURE_RATIO']
			); ?>">
		<a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_UP']; ?>">+</a>
		<span class="bx_cnt_desc" id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"><? echo (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : ''); ?></span>
	</span>

	<span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>">
<?
if ($showBuyBtn)
{
?>
		<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
<?
}
if ($showAddBtn)
{
?>
		<a href="javascript:void(0);" class="btn btn-link btn-big product-buy__button" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><? echo $addToBasketBtnMessage; ?></a>

<?
}
?>
	</span>

	<?if($showSubscribeBtn)
	{
		$APPLICATION->includeComponent('bitrix:catalog.product.subscribe','',
			array(
				'PRODUCT_ID' => $arResult['ID'],
				'BUTTON_ID' => $arItemIDs['SUBSCRIBE_LINK'],
				'BUTTON_CLASS' => 'bx_big bx_bt_button',
				'DEFAULT_DISPLAY' => !$canBuy,
			),
			$component, array('HIDE_ICONS' => 'Y')
		);
	}?>

	<br>

<? if ($arParams['DISPLAY_COMPARE'])
{
	?>
	<span class="item_buttons_counter_block">
		<a href="javascript:void(0);" class="bx_big bx_bt_button_type_2 bx_cart" id="<? echo $arItemIDs['COMPARE_LINK']; ?>"><? echo $compareBtnMessage; ?></a>
	</span>
<?} ?>

</div>


<? if ('Y' == $arParams['SHOW_MAX_QUANTITY'])
{
	if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
	{
?>
<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>" style="display: none;"><? echo GetMessage('OSTATOK'); ?>: <span></span></p>
<?
	}
	else
	{
		if ('Y' == $arResult['CATALOG_QUANTITY_TRACE'] && 'N' == $arResult['CATALOG_CAN_BUY_ZERO'])
		{
?>
<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>"><? echo GetMessage('OSTATOK'); ?>: <span><? echo $arResult['CATALOG_QUANTITY']; ?></span></p>
<?
		}
	}
}
}
else
{
?>
<div class="item_buttons vam">
	<span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>">
<?
if ($showBuyBtn)
{
?>
		<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
<?
}
if ($showAddBtn)
{
?>
	<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><? echo $addToBasketBtnMessage; ?></a>
<?
}
?>
	</span>
	<?if($showSubscribeBtn)
	{
		$APPLICATION->IncludeComponent('bitrix:catalog.product.subscribe','',
			array(
				'PRODUCT_ID' => $arResult['ID'],
				'BUTTON_ID' => $arItemIDs['SUBSCRIBE_LINK'],
				'BUTTON_CLASS' => 'bx_big bx_bt_button',
				'DEFAULT_DISPLAY' => !$canBuy,
			),
			$component, array('HIDE_ICONS' => 'Y')
		);
	}?>
	<br>
	<span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable<?=($showSubscribeBtn ? ' bx_notavailable_subscribe' : ''); ?>"><? echo $notAvailableMessage; ?></span>
<?if($arParams['DISPLAY_COMPARE'])
{
	?>
		<span class="item_buttons_counter_block">
	<? if ($arParams['DISPLAY_COMPARE'])
	{
		?><a href="javascript:void(0);" class="bx_big bx_bt_button_type_2 bx_cart" id="<? echo $arItemIDs['COMPARE_LINK']; ?>"><? echo $compareBtnMessage; ?></a><?
	} ?>
		</span>
<?}?>
</div>
<?
}
unset($showAddBtn, $showBuyBtn);
?>
</div> <!-- item_info_section -->

<? include("_properties.php"); ?>
<? include("_features.php"); ?>

<div class="item_info_section">
<?
if ('' != $arResult['DETAIL_TEXT'])
{
?>
<div class="product-description" itemprop="description">
	<?/*<!--div class="bx_item_section_name_gray" style="border-bottom: 1px solid #f2f2f2;"><? echo GetMessage('FULL_DESCRIPTION'); ?></div>*/?>
	<h3><? echo GetMessage('FULL_DESCRIPTION'); ?></h3>
<?
if ('html' == $arResult['DETAIL_TEXT_TYPE'])
{
	echo $arResult['DETAIL_TEXT'];
}
else
{
	?><p><? echo $arResult['DETAIL_TEXT']; ?></p><?
}
?>
</div>
<?
}
?>
</div>




<div class="clb"></div>
	</div>

<div class="bx_md">
<div class="item_info_section">
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	if ($arResult['OFFER_GROUP'])
	{
		foreach ($arResult['OFFER_GROUP_VALUES'] as $offerID)
		{
?>
	<span id="<? echo $arItemIDs['OFFER_GROUP'].$offerID; ?>" style="display: none;">
<?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
	".default",
	array(
		"IBLOCK_ID" => $arResult["OFFERS_IBLOCK"],
		"ELEMENT_ID" => $offerID,
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"]
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?><?
?>
	</span>
<?
		}
	}
}
else
{
	if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP'])
	{
?><?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
	".default",
	array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_ID" => $arResult["ID"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"]
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?><?
	}
}

if ($arResult['CATALOG'] && $arParams['USE_GIFTS_DETAIL'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled("sale"))
{
	$APPLICATION->IncludeComponent("bitrix:sale.gift.product", ".default", array(
			'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
			'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'SUBSCRIBE_URL_TEMPLATE' => $arResult['~SUBSCRIBE_URL_TEMPLATE'],
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],

			"SHOW_DISCOUNT_PERCENT" => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
			"SHOW_OLD_PRICE" => $arParams['GIFTS_SHOW_OLD_PRICE'],
			"PAGE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
			"LINE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
			"HIDE_BLOCK_TITLE" => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
			"BLOCK_TITLE" => $arParams['GIFTS_DETAIL_BLOCK_TITLE'],
			"TEXT_LABEL_GIFT" => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
			"SHOW_NAME" => $arParams['GIFTS_SHOW_NAME'],
			"SHOW_IMAGE" => $arParams['GIFTS_SHOW_IMAGE'],
			"MESS_BTN_BUY" => $arParams['GIFTS_MESS_BTN_BUY'],

			"SHOW_PRODUCTS_{$arParams['IBLOCK_ID']}" => "Y",
			"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
			"PRODUCT_SUBSCRIPTION" => $arParams["PRODUCT_SUBSCRIPTION"],
			"MESS_BTN_DETAIL" => $arParams["MESS_BTN_DETAIL"],
			"MESS_BTN_SUBSCRIBE" => $arParams["MESS_BTN_SUBSCRIBE"],
			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ADD_PROPERTIES_TO_BASKET" => $arParams["ADD_PROPERTIES_TO_BASKET"],
			"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
			"PARTIAL_PRODUCT_PROPERTIES" => $arParams["PARTIAL_PRODUCT_PROPERTIES"],
			"USE_PRODUCT_QUANTITY" => 'N',
			"OFFER_TREE_PROPS_{$arResult['OFFERS_IBLOCK']}" => $arParams['OFFER_TREE_PROPS'],
			"CART_PROPERTIES_{$arResult['OFFERS_IBLOCK']}" => $arParams['OFFERS_CART_PROPERTIES'],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"POTENTIAL_PRODUCT_TO_BUY" => array(
				'ID' => isset($arResult['ID']) ? $arResult['ID'] : null,
				'MODULE' => isset($arResult['MODULE']) ? $arResult['MODULE'] : 'catalog',
				'PRODUCT_PROVIDER_CLASS' => isset($arResult['PRODUCT_PROVIDER_CLASS']) ? $arResult['PRODUCT_PROVIDER_CLASS'] : 'CCatalogProductProvider',
				'QUANTITY' => isset($arResult['QUANTITY']) ? $arResult['QUANTITY'] : null,
				'IBLOCK_ID' => isset($arResult['IBLOCK_ID']) ? $arResult['IBLOCK_ID'] : null,

				'PRIMARY_OFFER_ID' => isset($arResult['OFFERS'][0]['ID']) ? $arResult['OFFERS'][0]['ID'] : null,
				'SECTION' => array(
					'ID' => isset($arResult['SECTION']['ID']) ? $arResult['SECTION']['ID'] : null,
					'IBLOCK_ID' => isset($arResult['SECTION']['IBLOCK_ID']) ? $arResult['SECTION']['IBLOCK_ID'] : null,
					'LEFT_MARGIN' => isset($arResult['SECTION']['LEFT_MARGIN']) ? $arResult['SECTION']['LEFT_MARGIN'] : null,
					'RIGHT_MARGIN' => isset($arResult['SECTION']['RIGHT_MARGIN']) ? $arResult['SECTION']['RIGHT_MARGIN'] : null,
				),
			)
		), $component, array("HIDE_ICONS" => "Y"));
}
if ($arResult['CATALOG'] && $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled("sale"))
{
	$APPLICATION->IncludeComponent(
			"bitrix:sale.gift.main.products",
			".default",
			array(
				"PAGE_ELEMENT_COUNT" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
				"BLOCK_TITLE" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],

				"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
				"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],

				"AJAX_MODE" => $arParams["AJAX_MODE"],
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],

				"ELEMENT_SORT_FIELD" => 'ID',
				"ELEMENT_SORT_ORDER" => 'DESC',
				//"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
				//"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
				"FILTER_NAME" => 'searchFilter',
				"SECTION_URL" => $arParams["SECTION_URL"],
				"DETAIL_URL" => $arParams["DETAIL_URL"],
				"BASKET_URL" => $arParams["BASKET_URL"],
				"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],

				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],

				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"SET_TITLE" => $arParams["SET_TITLE"],
				"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
				"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
				"CURRENCY_ID" => $arParams["CURRENCY_ID"],
				"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
				"TEMPLATE_THEME" => (isset($arParams["TEMPLATE_THEME"]) ? $arParams["TEMPLATE_THEME"] : ""),

				"ADD_PICT_PROP" => (isset($arParams["ADD_PICT_PROP"]) ? $arParams["ADD_PICT_PROP"] : ""),

				"LABEL_PROP" => (isset($arParams["LABEL_PROP"]) ? $arParams["LABEL_PROP"] : ""),
				"OFFER_ADD_PICT_PROP" => (isset($arParams["OFFER_ADD_PICT_PROP"]) ? $arParams["OFFER_ADD_PICT_PROP"] : ""),
				"OFFER_TREE_PROPS" => (isset($arParams["OFFER_TREE_PROPS"]) ? $arParams["OFFER_TREE_PROPS"] : ""),
				"SHOW_DISCOUNT_PERCENT" => (isset($arParams["SHOW_DISCOUNT_PERCENT"]) ? $arParams["SHOW_DISCOUNT_PERCENT"] : ""),
				"SHOW_OLD_PRICE" => (isset($arParams["SHOW_OLD_PRICE"]) ? $arParams["SHOW_OLD_PRICE"] : ""),
				"MESS_BTN_BUY" => (isset($arParams["MESS_BTN_BUY"]) ? $arParams["MESS_BTN_BUY"] : ""),
				"MESS_BTN_ADD_TO_BASKET" => (isset($arParams["MESS_BTN_ADD_TO_BASKET"]) ? $arParams["MESS_BTN_ADD_TO_BASKET"] : ""),
				"MESS_BTN_DETAIL" => (isset($arParams["MESS_BTN_DETAIL"]) ? $arParams["MESS_BTN_DETAIL"] : ""),
				"MESS_NOT_AVAILABLE" => (isset($arParams["MESS_NOT_AVAILABLE"]) ? $arParams["MESS_NOT_AVAILABLE"] : ""),
				'ADD_TO_BASKET_ACTION' => (isset($arParams["ADD_TO_BASKET_ACTION"]) ? $arParams["ADD_TO_BASKET_ACTION"] : ""),
				'SHOW_CLOSE_POPUP' => (isset($arParams["SHOW_CLOSE_POPUP"]) ? $arParams["SHOW_CLOSE_POPUP"] : ""),
				'DISPLAY_COMPARE' => (isset($arParams['DISPLAY_COMPARE']) ? $arParams['DISPLAY_COMPARE'] : ''),
				'COMPARE_PATH' => (isset($arParams['COMPARE_PATH']) ? $arParams['COMPARE_PATH'] : ''),
			)
			+ array(
				'OFFER_ID' => empty($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID']) ? $arResult['ID'] : $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'],
				'SECTION_ID' => $arResult['SECTION']['ID'],
				'ELEMENT_ID' => $arResult['ID'],
			),
			$component,
			array("HIDE_ICONS" => "Y")
	);
}
?>
</div>
		</div> <!-- bx_md -->

	<div class="bx_lb">
		<div class="tac ovh"></div>
		<? include("_comments.php"); ?>
	</div> <!-- bx_lb -->

</div>

<?
include("tabs/_tabs.php");

/* Похожие парики */
if($arResult['SECTION']['ID'] == WIGS_SECTION_ID){
	include("_similar_wigs.php");
}

/* Похожие Головные уборы */
if($arResult['SECTION']['ID'] == HATS_SECTION_ID){
	include("_similar_hats.php");
}

/* Похожие накладки */
if($arResult['SECTION']['ID'] == COVERS_SECTION_ID){
	include("_similar_covers.php");
}

/* Похожие шиньоны */
if($arResult['SECTION']['ID'] == SHINONY_SECTION_ID){
	include("_similar_shinony.php");
}

include("_recommended.php");
?>

    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" style="display: none;">
        <? $minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']); ?>
        <meta itemprop="price" content="<?= $minPrice['PRINT_DISCOUNT_VALUE'] ?>">
        <meta itemprop="priceCurrency" content="RUB">
    </div>

	<div class="clb"></div>
</div> <!-- bx_item_detail -->
<?
include("_offers.php");
include("_script.php");
/*
global $USER;
if (!$USER->IsAdmin()){
    include("_e-commerce.php");
}*/

?>

<div class="lazyLoadOfferPhoto" style="height: 0; overflow: hidden">
<? foreach($arResult['OFFERS'] as $OFFER): ?>
	<? $morePhohtosCount = count($OFFER["MORE_PHOTO"]); ?>
	<? $lastPhotoIndex = $morePhohtosCount - 1; ?>
	<? $lastPhoto = $OFFER["MORE_PHOTO"][$lastPhotoIndex] ?>
	<? $lastPhotoSrc = $lastPhoto["SRC"] ?>
	<img src="<?= $lastPhotoSrc ?>">
<? endforeach ?>
</div>
