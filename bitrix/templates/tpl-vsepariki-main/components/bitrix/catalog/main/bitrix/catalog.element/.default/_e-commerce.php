<script>
    document.addEventListener("DOMContentLoaded", function() {

        dataLayer.push({
            "ecommerce": {
                "detail": {
                    "products": [
                        {
                            "id": <?= $strObName ?>.product.id.toString(),
                        "name" : <?= $strObName ?>.product.name,
                        "price": parseInt(<?= $strObName ?>.currentBasisPrice.VALUE)
                    }
                ]
            }
        }
    });

});

BX.addCustomEvent("OnBasketChange", function() {
    var offers = <?= $strObName ?>.offers;
    var offerNumID = offers[<?= $strObName ?>.offerNum].ID;

    var treePropId = "PROP_" + <?= $strObName ?>.treeProps[0].ID;

    for(var i = 0; i < offers.length; i++){
        if(offers[i].ID == offerNumID){
            var propId = offers[i].TREE[treePropId];
            var offerName = document.querySelector("li[data-onevalue='" + propId + "'] i").title;
            var offerPrice = parseInt(offers[i].PRICE.VALUE);
        }
    }

    dataLayer.push({
        "ecommerce": {
            "add": {
                "products": [
                    {
                        "id": <?= $strObName ?>.product.id.toString(),
                        "name": <?= $strObName ?>.product.name,
                        "price": offerPrice,
                        "quantity": parseInt(<?= $strObName ?>.basketParams.quantity),
                        "variant": offerName
                    }
                ]
            }
        }
    });


});
</script>