<?
	if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
	{
		// Все свойства
		$props = $arResult['DISPLAY_PROPERTIES'];

		// Бренд
		$propBrand = !empty($props["BRAND"]["DISPLAY_VALUE"]) ? $props["BRAND"]["DISPLAY_VALUE"] : false;

		// Материал волос
		$propMaterial = !empty($props["MATERIAL"]["DISPLAY_VALUE"]) ? $props["MATERIAL"]["DISPLAY_VALUE"] : false;

		// Вид материала волос
		$propTypeMaterial = !empty($props["HAIR_MATERIAL_TYPE"]["DISPLAY_VALUE"]) ? $props["HAIR_MATERIAL_TYPE"]["DISPLAY_VALUE"] : false;

		// Вид основы парика
		$propWigBaseType = !empty($props["WIG_BASE_TYPE"]["DISPLAY_VALUE"]) ? $props["WIG_BASE_TYPE"]["DISPLAY_VALUE"] : false;

		// Степень естественности
		$propNaturality    = !empty($props["NATURALITY"]["DISPLAY_VALUE"]) ? $props["NATURALITY"]["DISPLAY_VALUE"] : false;


		// -> Справочники
		// Материал волос - $propBrand[$propMaterial]
		$refBrandMaterial["Ellen Wille"] = [
			"Натуральный" => "<strong>Remy hair</strong> - это волосы высокого качества, крепкие и не склонные к запутыванию",
			"Моноволокно (не отличить от натурального)" => "<strong>Искусственные волосы</strong> - готовы к носке, всегда хорошо уложены, неотличимы от натуральных волос",
			"Смешанный" => "<strong>Сочетание натуральных и термостойких искусственных волос</strong> - дольше сохраняется насыщенный цвет, укладка будет сохраняться дольше"
		];

		// Вид материала волос - $propBrand[$propTypeMaterial]
		$refBrandTypeMaterial["NJ Creation"] = [
			"Remy hair" => "<strong>Remy hair</strong> - это волосы высокого качества, крепкие и не склонные к запутыванию",
			"European hair" => "<strong>European hair</strong> - долговечны, выдерживают температуру до 210 градусов",
			"MOOVIE CURL" => "<strong>MOOVIE CURL</strong> - сочетание натуральных и искусственных волос - дольше сохраняется насыщенный цвет, укладка будет сохраняться дольше, даже во влажную погоду",
			"EASY BRUSH" => "<strong>EASY BRUSH</strong> - выдерживает температуру до 180 градусов",
			"EUROFIBER CONCEPT" => "<strong>EUROFIBER CONCEPT</strong> - долговечны, выдерживают температуру до 210 градусов",
		];

		// Вид основы парика
		$refWigBaseType = [
			"Deluxe" => "<strong>Монофиламент</strong> - эффект натурального роста волос, можно укладывать волосы в любом направлении.<br><strong>Ручная работа</strong> - легкий и комфортный",
			"Comfort" => "<strong>100% ручная работа</strong> - максимально натуральный вид, можно укладывать волосы в любом направлении",
			"Mono" => "<strong>Монофиламент</strong> - в области монофиламента вы можете делать пробор или укладывать волосы в любом направлении",
			"Teil-mono including mono-parting" => "<strong>В области монофиламента вы можете делать пробор.</strong><br><strong>Трессы</strong> - удобно в ношении, хорошо пропускает воздух",
			"Teil-mono including mono-crown" => "<strong>В области монофиламента вы можете делать пробор</strong><br><strong>Трессы</strong> - удобно в ношении, хорошо пропускает воздух",
			"Tresse" => "<strong>Трессы</strong> - очень удобная в ношении, хорошо пропускает воздух, дает хорошую посадку по форме головы",
			"Concept A" => "<strong>Трессы</strong> - очень удобная в ношении, хорошо пропускает воздух, дает хорошую посадку по форме головы<br>В области монофиламента вы можете делать пробор",
			"Concept B" => "<strong>100% ручная работа</strong> - максимально натуральный вид, можно укладывать волосы в любом направлении",
			"Concept C" => "<strong>Монофиламент</strong> - эффект натурального роста волос, можно укладывать волосы в любом направлении<br><strong>Трессы</strong> - удобно в ношении, хорошо пропускает воздух",
			"Concept D" => "<strong>Монофиламент</strong> - эффект натурального роста волос, можно укладывать волосы в любом направлении<br><strong>Ручная работа</strong> - легкий и комфортный",
		];

		// Степень естественности
		$refNaturality = [
			"С естественной фронтальной линией" => "<strong>Невидимая фронтальная линия</strong> - линия роста волос выглядит натурально и практически невидима"
		];

		// Бренд
		$refBrand = [
			"NJ Creation" => "<strong>Максимальная эластичность</strong> - уменьшает давление шапки<br><strong>Антискольжение</strong> - микроперфорированная мембрана регулирует температуру (комфортно зимой и не жарко летом), сокращает потоотделение, надежно фиксирует парик<br><strong>Modulcap</strong> - позволяет за несколько секунд уменьшить размер шапки"
		];
		// <- Справочники

		$features = [];
		if(!empty($refBrandMaterial[$propBrand][$propMaterial])){
			$features[] = $refBrandMaterial[$propBrand][$propMaterial];
		}

		if(!empty($refBrandTypeMaterial[$propBrand][$propTypeMaterial])){
			$features[] = $refBrandTypeMaterial[$propBrand][$propTypeMaterial];
		}

		if(!empty($refWigBaseType[$propWigBaseType])){
			$features[] = $refWigBaseType[$propWigBaseType];
		}

		if(!empty($propNaturality)){
			if(!is_array($propNaturality)){$propNaturality = [$propNaturality];}
			foreach ($propNaturality as $propNaturalityItem){
				if(!empty($refNaturality[$propNaturalityItem])){
					$features[] = $refNaturality[$propNaturalityItem];
				}
			}
		}

		if(!empty($refBrand[$propBrand])){
			$features[] = $refBrand[$propBrand];
		}


		if(!empty($features)): ?>
		<div class="item_info_section product-features">
			<h3>Особенности</h3>
			<? foreach ($features as $feature): ?>
				<div class="product-features__item"><?= $feature; ?></div>
			<? endforeach; ?>
		</div> <!-- item_info_section --><?
		endif;
	?>

	<?
	}
?>
