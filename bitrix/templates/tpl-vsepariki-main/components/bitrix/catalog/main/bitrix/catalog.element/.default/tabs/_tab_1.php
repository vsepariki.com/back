<? if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']): ?>
	<div class="product-tabs__tab product-tabs__tab_active xl-hidden l-hidden m-hidden" data-product-tab="product-tab1">Характеристики</div>

	<div class="product-tabs__tab-content product-tabs__tab-content_active" data-product-tab="product-tab1">
		<div class="item_info_section product-properties">

			<? if($arResult["SECTION"]["ID"] == WIGS_SECTION_ID): ?>

				<? include("_tab_inc_properties.php"); ?>

				<!--noindex-->
					<? include("_tab_inc_hair_material.php"); ?>
					<? include("_tab_inc_construkciya_osnovy.php"); ?>
				<!--/noindex-->

			<? endif; ?>

			<?
			if(
				$arResult["SECTION"]["ID"] == HATS_SECTION_ID OR
				$arResult["SECTION"]["ID"] == COVERS_SECTION_ID OR
				$arResult["SECTION"]["ID"] == SHINONY_SECTION_ID
			):
			?>
				<? include("_tab_inc_properties.php"); ?>
			<? endif; ?>

		</div> <!-- item_info_section -->
	</div>
<? endif ?>
