<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$fullSeoName = $arItem["NAME"];
if($titleForSite){
    $fullSeoName .= " ".$titleForSite;
}
?>
<a class="catalog-product__image" href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
	<? if(!empty($arItem['PREVIEW_PICTURE']['SRC'])): ?>

		<img
		    src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>"
		    alt="<?= $fullSeoName ?>"
		    title="<?= $fullSeoName ?>"
        >
        <?php if (!empty($arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'])){?>
            <div class="catalog-product__discount"><?php echo $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT']?>%</div>

        <?}?>
	<? elseif(!empty($arItem['DETAIL_PICTURE']['SRC'])): ?>

		<img
		    src="<?= $arItem['DETAIL_PICTURE']['SRC']; ?>"
		    alt="<?= $fullSeoName ?>"
		    title="<?= $fullSeoName ?>"
        >
        <?php if (!empty($arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'])){?>
            <div class="catalog-product__discount"><?php echo $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT']?>%</div>

        <?}?>
	<? else: ?>
		<img src="<?= $defaultProductImgSrc ?>" alt="">
	<? endif; ?>
</a>

<div class="catalog-product__name">
	<a href="<?= $arItem['DETAIL_PAGE_URL']; ?>"><?= $productName ?></a>
</div>

<div class="catalog-product__prices">
    <div class="catalog-product__price">

        <?= $arItem["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE"] ?>
    </div>
    <?php if($arItem["PRICES"]["BASE"]["DISCOUNT_DIFF"]): ?>
        <div class="catalog-product__old-price">
            <?= $arItem["PRICES"]["BASE"]["PRINT_VALUE"] ?>
        </div>
    <?php endif; ?>
    <div class="catalog-product__old-price">
        <?php
        if (empty($arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'])){
            $price = $arItem["PRICES"]["BASE"]["VALUE"];
            $fullPrice = $price + ($price * 0.2); // + 20% к базовой стоимости
            $fullPrice = number_format($fullPrice, 0, ',', ' ');
            echo $fullPrice?>  руб
        <?php } ?>
    </div>

</div>

<div class="catalog-product-colors">
    <div class="catalog-product__colors">
        <div class="catalog-product-buy"><a class="product-buy-one-click modal-link-buy-one-click catalog-product-buy__one-click" href="#buy-one-click">Купить в 1 клик</a>
            <a class="btn catalog-product-buy__button modal-link-add-to-cart" href="#add-to-cart" data-url="<?=$arItem['ADD_URL']?>&ajax_basket=Y&quantity=1" data-curr-price="<?php echo $fullPrice ?>" data-id="<?=$arItem['ID']?>">В корзину</a>

            <a class="btn catalog-product-buy__button catalog-product-buy__button--pre-order modal-link-availability hidden" href="#availability">Предзаказ</a>
        </div>
    </div>

</div>
