<div class="product-tabs__tab xl-hidden l-hidden m-hidden" data-product-tab="product-tab3">Размер</div>
<div class="product-tabs__tab-content" data-product-tab="product-tab3">
	<div class="item_info_section product-properties">
		<p>Зная длину окружности головы, можно подобрать размер парика, воспользовавшись таблицей:</p>
		<table class="table">
			<tr>
				<th>Размер парика</th><th>Длина окружности головы</th>
			</tr>
			<tr>
				<td>Большой</td><td>58-60 см.</td>
			</tr>
			<tr>
				<td>Стандартный</td><td>55-57 см.</td>
			</tr>
			<tr>
				<td>Маленький</td><td>52-54 см.</td>
			</tr>
		</table>
		<p itemscope itemtype="http://schema.org/ImageObject"><img src="/images/wigs_construkciya_osnovy/image8.png" alt="" itemprop="contentUrl"></p>
		<p><strong>Возьмите сантиметровую ленту, измерьте длину окружности головы.</strong></p>
		<ol class="list-numbered">
			<li>Начинайте измерения с области лба (над бровями), двигаясь в  одну сторону вдоль линии роста волос.</li>
			<li>Когда дойдете до уха, протяните ленту за ухо.</li>
			<li>Двигаясь по кругу, оберните голову и вернитесь к исходной точке на лбу.</li>
		</ol>
		<p>
			Важно: <br>
			Парики среднего размера подходят 95% клиентов.
		</p>
		<p>
			Большинство моделей париков с внутренней стороны шапки имеют специальные резиночки на липучках, которые позволяют менять размер парика для комфортного и надежного ношения на 2 см в меньшую или большую сторону.
		</p>

		<div class="product-tabs__subheader">
			Модели каких размеров представлены в нашем магазине?
		</div>
		<p>Большинство моделей париков в нашем каталоге имеет стандартный размер. Но есть отдельные коллекции.</p>
		<p>- Марка "Ellen Wille"</p>
			<ul class="list-marked">
				<li>Коллекция “Hairpower small & large” - есть маленькие и большие размеры, помимо стандартных</li>
				<li>Коллекция “Hair MANia” - только большие размеры</li>
				<li>Коллекция “Wigs for Kids” - только маленькие размеры</li>
		</ul>
		<p>- Марка "NJ Creation"</p>
		<ul class="list-marked">
			<li>большинство моделей имеют маленький размер</li>
		</ul>


	</div>
</div>
