<div class="product-tabs__title">Детальная информация</div>
<?
$refArrPropsInTabs = [];

if($arResult["SECTION"]["ID"] == WIGS_SECTION_ID){
	$refArrPropsInTabs = [
		"STRUCTURE" => "Структура волос",
		"FEATURES_COLOR" => "Особенности цвета",
		"LENGTH_STRING" => "Длина",
		"LENGTH" => "Длина",
		"WEIGHT" => "Вес",
		"FIT" => "Подходит к форме лица",
		"STYLE" => "Стиль",
		"HAIRCUT" => "Стрижка",
		"TYPE" => "Тип",
	];
}

if($arResult["SECTION"]["ID"] == HATS_SECTION_ID){
	$refArrPropsInTabs = [
		"FOR_WHOM_HAT" => "Для кого",
		"ACCESSORIES" => "Аксессуары",
		"USING_HAT" => "Применение",
	];
}

if($arResult["SECTION"]["ID"] == COVERS_SECTION_ID){
	$refArrPropsInTabs = [
		"ATTACH_COVER" => "Способ крепления",
		"STRUCTURE_COVER" => "Структура волос",
		"PURPOSE_COVER" => "Назначение",
		"FEATURE_COVER" => "Особенности",
		"LENGTH_STRING_COVER" => "Длина волос",
		"SIZE_STRING_COVER" => "Размер",
		"WEIGHT_STRING_COVER" => "Вес",
		"TYPE_COVER" => "Тип",
	];
}

if($arResult["SECTION"]["ID"] == SHINONY_SECTION_ID){
	$refArrPropsInTabs = [
        "TYPE_SHINONY" => "Вид",
        "STYLE_SHINONY" => "Стиль",
        "POSITION_SHINONY" => "Место",
        "STRUCTURE_SHINONY" => "Структура",
        "WEIGHT_SHINONY" => "Вес",
	];
}

if (!empty($arResult['DISPLAY_PROPERTIES']))
{
	$sortedDisplayProperties = [];
	foreach ($refArrPropsInTabs as $refArrPropInTabsKey => $refArrPropInTabsVal){
		if(!empty($arResult['DISPLAY_PROPERTIES'][$refArrPropInTabsKey]['DISPLAY_VALUE'])){
			$sortedDisplayProperties[] = $arResult['DISPLAY_PROPERTIES'][$refArrPropInTabsKey];
		}
	}

	foreach ($sortedDisplayProperties as $arOneProp)
	{
		$displayNameVal = "";

		if(is_array($arOneProp['DISPLAY_VALUE'])){
			$displayVal = implode(' / ', $arOneProp['DISPLAY_VALUE']);
		}else{
			$displayVal = $arOneProp['DISPLAY_VALUE'];
		}
		$displayName = $refArrPropsInTabs[$arOneProp["CODE"]];

		if($arOneProp["CODE"] == "LENGTH_STRING"){
			$displayNameVal = "<div class=\"product-properties__name\">".$displayName.":</div><div class=\"product-properties__value\">".$displayVal."</div>";
		}elseif($arOneProp["CODE"] == "LENGTH"){
			if(empty($arResult['DISPLAY_PROPERTIES']["LENGTH_STRING"])){
				$displayNameVal = "<div class=\"product-properties__name\">".$displayName.":</div><div class=\"product-properties__value\">".$displayVal."</div>";
			}
		}elseif(strpos($arOneProp["CODE"], "WEIGHT") !== false){
			$displayNameVal = "<div class=\"product-properties__name\">".$displayName.": </div><div class=\"product-properties__value\">".$displayVal." г.</div>";
		}else{
			$displayNameVal = "<div class=\"product-properties__name\">".$displayName.":</div><div class=\"product-properties__value\">".$displayVal."</div>";
		}

		if($displayNameVal): ?>
		<div class="product-properties__item"><?= $displayNameVal; ?></div>
		<?
		endif;
	}
}

if ($arResult['SHOW_OFFERS_PROPS']){
	?>
	<dl id="<? echo $arItemIDs['DISPLAY_PROP_DIV'] ?>" style="display: none;"></dl><?
}
?>
