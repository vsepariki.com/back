<?
namespace CatalogSectionSeoHandler\PageTypes;

class Main implements PageTypesInterface
{
    public function getWigsTitle(): string
    {
        return "Купить хороший парик в Спб можно недорого, сколько стоит качественный парик: цена, каталог и большой выбор";
    }

    public function getWigsDescription(): string
    {
        return "Каталог из 200 моделей в наличии. Низкие цены, доставка по России. Купить хороший парики европейского качества недорого. Не отличить от своих волос, надежная фиксация. Подберем стиль в отдельной примерочной (конфиденциально).";
    }

    public function getHatsTitle(): string
    {
        return "Головные уборы после химиотерапии для женщин: купить, магазин головных уборов при облысении";
    }

    public function getHatsDescription(): string
    {
        return "Широкий выбор головных уборов после химиотерапии и при облысении. Около 100 моделей в наличии для женщин. Легкие и изящные, европейское качество, на лето, весну, осень и зиму.";
    }

    public function getCoversTitle(): string
    {
        return "Купить накладки для волос в Спб, цены на накладки на голову";
    }

    public function getCoversDescription(): string
    {
        return "Каталог из 50 моделей в наличии. Низкие цены, доставка по России. Купить накладки для волос европейского качества. Не отличить от своих волос, надежная фиксация. Подберем стиль в отдельной примерочной (конфиденциально).";
    }

    public function getShinonyTitle(): string
    {
        return "Купить накладной шиньон для волос в интернет магазине в спб с фото по низкой цене";
    }

    public function getShinonyDescription(): string
    {
        return "Интернет магазин шиньонов любых видов, длин и цветов в Спб. Немецкое качество по низкое цене. 24 модели из синтетического волокна Modacrylic имитируют натуральные волосы. Легкие и готовые к носке.";
    }

    public function getSotbitTitle($sectionName, $sotbitProps, $sotbitSeoMetaTitle): string
    {
        return $sotbitSeoMetaTitle ? $sotbitSeoMetaTitle : trim($sectionName." ".$sotbitProps);
    }

    public function getDefaultTitle($sectionName, $filterParts, $sotbitSeoMetaTitle): string
    {

        return !empty($sotbitSeoMetaTitle) ? $sotbitSeoMetaTitle : trim($sectionName)." ".$filterParts;
    }

}