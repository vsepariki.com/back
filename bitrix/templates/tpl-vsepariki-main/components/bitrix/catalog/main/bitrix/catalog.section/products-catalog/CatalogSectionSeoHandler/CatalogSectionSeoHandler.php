<?
use CatalogSectionSeoHandler\PageTypes\PageTypesInterface;
use CatalogSectionSeoHandler\ProductTypes\ProductTypesInterface;

use CatalogSectionSeoHandler\PageTypes\PagerSort;
use CatalogSectionSeoHandler\PageTypes\Pager;
use CatalogSectionSeoHandler\PageTypes\Sort;
use CatalogSectionSeoHandler\PageTypes\Main;

use CatalogSectionSeoHandler\ProductTypes\Wigs;
use CatalogSectionSeoHandler\ProductTypes\Hats;
use CatalogSectionSeoHandler\ProductTypes\Covers;
use CatalogSectionSeoHandler\ProductTypes\Shinony;

class CatalogSectionSeoHandler
{
    private $app;

    public function __construct()
    {
        global $APPLICATION;
        $this->app = $APPLICATION;
    }

    public function getPageTypeModel(): PageTypesInterface
    {
        if(!empty($_GET["PAGEN_1"]) && !empty($_GET["sort"])){
            return new PagerSort($_GET["PAGEN_1"], $_GET["sort"]);
        }

        if(!empty($_GET["sort"])){
            return new Sort($_GET["sort"]);
        }

        if(!empty($_GET["PAGEN_1"])){
            return new Pager($_GET["PAGEN_1"]);
        }

        return new Main();
    }

    public function getProductTypeModel(int $sectionId): ProductTypesInterface
    {
        if(isSectionWigs($sectionId)) {return new Wigs;}
        if(isSectionHats($sectionId)) {return new Hats;}
        if(isSectionCovers($sectionId)) {return new Covers;}

        return new Shinony;
    }

    public function getTitle(PageTypesInterface $pageType, ProductTypesInterface $productType): string
    {
        $classname = (new \ReflectionClass($productType))->getShortName();
        $methodName = "get".$classname."Title";

        return $pageType->$methodName();
    }

    public function getDescription(PageTypesInterface $pageType, ProductTypesInterface $productType): string
    {
        $classname = (new \ReflectionClass($productType))->getShortName();
        $methodName = "get".$classname."Description";

        return $pageType->$methodName();
    }

    public function setTitle(string $title): void
    {
        if(!empty($title)){
            $this->app->SetPageProperty("title", $title);
        }
    }

    public function setDescription($description): void
    {
        if(!empty($description)){
            $this->app->SetPageProperty("description", $description);
        }else{
            $this->app->SetPageProperty("description", "");
        }
    }

    public function setCanonical(): void
    {
        if(!empty($_GET["PAGEN_1"])){
            $curPage = $this->app->GetCurPage();
            $this->app->SetPageProperty("canonical", 'https://'.$_SERVER["HTTP_HOST"].$curPage);
        }
    }

    public function getFilterParts($globalFilter)
    {
        $paramValues = [];

        foreach ($globalFilter as $propIdRaw => $propValIds){
            $propId = str_replace("=PROPERTY_", "", $propIdRaw);
            foreach ($propValIds as $propValId){
                $paramValues[] = $this->getProductPropertyValue($propId, $propValId);
            }
        }

        return implode(" ", $paramValues);
    }

    private function getProductPropertyValue($propertyId, $propertyValueId)
    {
        $res = CIBlockProperty::GetPropertyEnum(
            $propertyId,
            [],
            ["IBLOCK_ID" => IBLOCK_PRODUCTS_ID, "ID" => $propertyValueId]
        );

        if(!$propertyValueArr = $res->GetNext()){
            return false;
        }

        return $propertyValueArr["VALUE"];
    }

	public function getDescriptionForCustomSetURLs($arResult, $sotbitSeoMetaH1): string
	{
		$count = $arResult["ITEMS_COUNT"];

		if($count){
			$models = pluralDeclension($count, "модели", "моделей", "моделей");
			$countModels = "Каталог из ".$count." ".$models." в наличии.";
		}else{
			$countModels = "В каталоге не найдено ни одной модели.";
		}

		$h1 = !empty($sotbitSeoMetaH1) ? $sotbitSeoMetaH1 : $arResult['SECTION_NAME'];
		$h1 = mb_strtolower($h1);

		return $countModels." "."Низкие цены, доставка по России. Подберите ".$h1." европейского качества. Надежная фиксация. Подберем стиль в отдельной примерочной (конфиденциально), Звоните +7 (812) 413-99-06";

	}

	public function hasDescriptionForCustomSetURLs(): bool
	{
		$customSetURLs = [
			'/catalog/golovnye-ubory/dlya-sna/',
			'/catalog/pariki/nezametnyy/naturalnyy/',
			'/catalog/pariki/100-ruchnaya-rabota/naturalnyy/',
			'/catalog/pariki/muzhskoy/bryunet/',
			'/catalog/shinony/volnistye/',
			'/catalog/pariki/zhenskiy/naturalnyy/',
			'/catalog/pariki/bryunet/s-chelkoy/',
			'/catalog/pariki/iskusstvennyy/korotkiy/',
			'/catalog/pariki/monofilament-ruchnaya-rabota-na-setke/naturalnyy/',
			'/catalog/pariki/zhenskiy/korotkiy/',
			'/catalog/golovnye-ubory/kepka/',
			'/catalog/nakladki/na-fiksiruyushchey-lente/',
			'/catalog/golovnye-ubory/s-sharfom-platkom/',
			'/catalog/golovnye-ubory/povsednevnye/',
			'/catalog/golovnye-ubory/bambuk/',
			'/catalog/golovnye-ubory/shapochka/letniy/',
			'/catalog/pariki/ryzhiy/kudryavyy/',
			'/catalog/pariki/filter/style-is-na-svadbu/apply/',
			'/catalog/pariki/korotkiy/bryunet/',
			'/catalog/pariki/filter/size-is-bolshoy/apply/',
			'/catalog/golovnye-ubory/zhenskiy/shapochka/',
			'/catalog/nakladki/filter/brand_cover-is-dizayn-volos-collection/apply/',
			'/catalog/golovnye-ubory/osenniy/',
			'/catalog/golovnye-ubory/poliester/',
			'/catalog/shinony/vecherniy/',
			'/catalog/pariki/iskusstvennyy/dlinnyy/',
			'/catalog/pariki/filter/collection_wig-is-prime_power_2016/apply/',
			'/catalog/pariki/muzhskoy/naturalnyy/',
			'/catalog/pariki/muzhskoy/iskusstvennyy/',
			'/catalog/pariki/filter/size-is-standartnyy/apply/',
			'/catalog/golovnye-ubory/sharf/',
			'/catalog/golovnye-ubory/s-kozyrkom/',
			'/catalog/pariki/kare/bryunet/',
			'/catalog/pariki/naturalnyy/dlinnyy/',
			'/catalog/shinony/povsednevnyy/',
			'/catalog/golovnye-ubory/dlya-sozdaniya-stilya/',
			'/catalog/pariki/bryunet/kudryavyy/',
			'/catalog/pariki/muzhskoy/korotkiy/',
			'/catalog/golovnye-ubory/dlya-sporta/',
			'/catalog/pariki/kare/s-chelkoy/',
			'/catalog/golovnye-ubory/zhenskiy/letniy/',
			'/catalog/pariki/blond/korotkiy/',
			'/catalog/golovnye-ubory/s-cvetkom/',
			'/catalog/pariki/volnistyy/',
			'/catalog/pariki/muzhskoy/dlinnyy/',
			'/catalog/pariki/naturalnyy/korotkiy/',
			'/catalog/pariki/filter/style-is-delovoy/apply/',
			'/catalog/pariki/blond/iskusstvennyy/',
			'/catalog/pariki/sredniy/naturalnyy/',
			'/catalog/golovnye-ubory/povyazka-obodok/',
			'/catalog/shinony/delovoy/',
			'/catalog/nakladki/smeshannyy/',
			'/catalog/pariki/dlinnyy/ryzhiy/',
			'/catalog/golovnye-ubory/s-bantom/',
			'/catalog/pariki/filter/style-is-vecherniy/apply/',
			'/catalog/pariki/filter/price_category-is-sredniy/apply/',
			'/catalog/pariki/dorogoy/naturalnyy/',
			'/catalog/pariki/filter/base-is-monofilament-tressy/apply/',
			'/catalog/pariki/filter/style-is-klassicheskiy/apply/',
			'/catalog/shinony/pryamye/',
			'/catalog/shinony/nakladka-dlya-dliny/',
			'/catalog/golovnye-ubory/dlya-doma/',
			'/catalog/pariki/blond/kudryavyy/',
			'/catalog/pariki/detskiy/blond/',
			'/catalog/golovnye-ubory/elastan/',
			'/catalog/pariki/kare/blond/',
			'/catalog/golovnye-ubory/zhenskiy/dlya-sozdaniya-stilya/',
			'/catalog/pariki/blond/dlinnyy/',
			'/catalog/pariki/filter/size-is-malenkiy/apply/',
			'/catalog/golovnye-ubory/vesenniy/shlyapka/',
			'/catalog/pariki/naturalnyy/kare/',
			'/catalog/shinony/sredniy/',
			'/catalog/pariki/bryunet/dlinnyy/',
			'/catalog/pariki/oblegchennyy/naturalnyy/',
			'/catalog/pariki/naturalnyy/ellen-wille/',
			'/catalog/pariki/sredniy/iskusstvennyy/',
			'/catalog/pariki/zhenskiy/iskusstvennyy/',
			'/catalog/pariki/zhenskiy/dlinnyy/',
		];

		return in_array(getCurPage(), $customSetURLs);
	}

}
