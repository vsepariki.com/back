<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$defaultProductImgSrc = '/upload/medialibrary/baa/baaa03452391641f164ed2e95b71384f.jpg';
$curUri  = $APPLICATION->GetCurUri();
$productNameSeoRefs = include __DIR__.'/product-name-seo-refs.php';

if(
	$curUri == "/catalog/pariki/" OR
	$curUri == "/catalog/golovnye-ubory/" OR
	$curUri == "/catalog/nakladki/" OR
	$curUri == "/catalog/shinony/"
):
	if(!empty($arResult["UF_SECTION_ANONS"])): ?>
		<div class="bx_catalog_anons">
			<?= $arResult["~UF_SECTION_ANONS"]; ?>
		</div>
	<?
	endif;
endif;

global $sotbitSeoMetaTopDesc;
if(
	!empty($sotbitSeoMetaTopDesc) &&
	empty($_GET["PAGEN_1"]) &&
	empty($_GET["sort"])
){ ?>
	<div class="bx_catalog_anons">
		<?= $sotbitSeoMetaTopDesc; ?>
	</div> <?
}

$curPage = $APPLICATION->GetCurPage();
$activeSort = ' catalog-sort__item_active';
$activeSort_2 = $activeSort_3 = $activeSort_4 = $activeSort_5 = '';

if(!empty($_GET['sort'])){
	$sort = $_GET['sort'];
	switch ($sort){
		case 'prices_asc' : $activeSort_2 = $activeSort; break;
		case 'prices_desc': $activeSort_3 = $activeSort; break;
		case 'novelty'    : $activeSort_4 = $activeSort; break;
		case 'discount'   : $activeSort_5 = $activeSort; break;
	}
}else{
	$activeSort_1 = $activeSort;
}
?>
<div class="catalog-sort s-hidden">
	<div class="catalog-sort__title">Сортировать:</div>
	<div class="catalog-sort__items">
		<a class="catalog-sort__item<?= $activeSort_1; ?>" href="<?= $curPage ?>">по популярности</a>
		<a class="catalog-sort__item<?= $activeSort_2; ?>" href="<?= $curPage ?>?sort=prices_asc">по возрастанию цены</a>
		<a class="catalog-sort__item<?= $activeSort_3; ?>" href="<?= $curPage ?>?sort=prices_desc">по убыванию цены</a>
		<a class="catalog-sort__item<?= $activeSort_4; ?>" href="<?= $curPage ?>?sort=novelty">по новинкам</a>
		<!--a class="catalog-sort__item<?//= $activeSort_5; ?>" href="<?//= $curPage ?>?sort=discount">по скидкам</a-->
	</div>
</div>

<div class="block-wrap home-other-products-list block-wrap_wrap ">

	<? if(!$arResult["ITEMS"]): ?>
        <div class="block-wrap__item block-wrap__item_xl-width12">
            <? if(isCurPageFilterResult()): ?>
                <p>
                    По выбранным вами характеристикам не найдено ни одной модели
                    <?= isSectionWigs($arResult["ORIGINAL_PARAMETERS"]["SECTION_ID"]) ? "парика" : "товара" ?>. <br>
                    Попробуйте изменить параметры или снять отметку с незначительных параметров
                </p>
			<? else: ?>
                <p>В категории пока нет товаров</p>
			<? endif ?>
        </div>
        <style>.catalog-sort{display: none;}</style>
    <? endif ?>

	<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
		<?
		    $titleForSite = "";
			if(empty($arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])){
				$productName = $arItem["NAME"];
			}else{
				$titleForSite = $arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
				$productName = $arItem["NAME"]." | ".$titleForSite;
			}

            if(!empty($productNameSeoRefs[$curPage])){
				$productName .= ' - '.$productNameSeoRefs[$curPage];
            }
		?>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width3 catalog-list__item">
			<div class="catalog-product">
                <?
                if(!empty($arItem["OFFERS"])){
                    include __DIR__.'/_list-product-complex.php';
                }
                else{
                    include __DIR__.'/_list-product-simple.php';
                }
                ?>
            </div>


		</div>
	<? endforeach; ?>
    <div class="modal" id="availability">
        <a class="modal__close" href="" rel="modal:close">×</a>
        <div class="modal_body">
            <div class="modal__header">Сообщить о поступлении</div>
            <form method="post" class="modal-availability modal_form_subscribe subscribe validated-form submitJS" name="modal-availability">
                <input type="hidden" name="product_art" class="product_art" value="781" wfd-id="id23">
                <input type="hidden" name="product" value="781" wfd-id="id24">
                <div class="form-field form-field-phone">
                    <label for="name">Имя</label>
                    <input id="name" type="text" name="name" class="validated required" wfd-id="id25">
                    <div class="validation-msg validation-msg-required">Обязательное поле</div>
                </div>
                <div class="form-field form-field-phone">
                    <label for="phone">Телефон</label>
                    <input id="phone" type="tel" name="phone" class="validated required" placeholder="+7 (___) ___-__-__" wfd-id="id26">
                    <div class="validation-msg validation-msg-required">Обязательное поле</div>
                </div>
                <input type="submit" value="Заказать" class="btn" wfd-id="id27">
                <div class="form-field modal-availability__agreement">
                    <input type="checkbox" id="modal-availability__agreement" name="modal-availability__agreement" class="validated required" checked="" required="" wfd-id="id28">
                    <label for="modal-availability__agreement">Нажимая на кнопку, я даю свое <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">согласие на обработку своих персональных данных</a>.</label>
                </div>
            </form>
        </div>
    </div>
    <div class="modal quickBuy" id="buy-one-click">
        <a class="modal__close" href="" rel="modal:close">×</a>
        <div class="modal_body">
            <div class="modal__header">Заказать НАЗВАНИЕ ПАРИКА</div>
            <p><strong>Укажите номер телефона.</strong></p>
            <p>Перезвоним проконсультировать по&nbsp;товару и&nbsp;помочь в&nbsp;подборе, уточнить место доставки, выбрать удобный способ оплаты.</p>
            <form method="post" class="quick-buy validated-form submitJS" name="quick-buy">
                <div class="quick-buy__row">
                    <div class="form-field form-field-phone">
                        <input type="tel" name="phone" class="validated required" placeholder="+7 (___) ___-__-__" wfd-id="id18">
                        <div class="validation-msg validation-msg-required">Напишите телефон мы перезвоним и подтвердим заказ</div>
                    </div>
                    <input type="submit" value="Заказать" class="btn" wfd-id="id19">
                </div>
                <div class="form-field quick-buy__agreement">
                    <input type="checkbox" id="quick-buy__agreement" name="agreement" class="validated required" checked="" required="" wfd-id="id20">
                    <label for="quick-buy__agreement">Нажимая на кнопку, я даю свое <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">согласие на обработку своих персональных данных</a>.</label>
                </div>
                <input type="hidden" name="productId" value="2127" wfd-id="id21">
                <input type="hidden" name="sectionName" value="Парики" wfd-id="id22">
            </form>
        </div>
    </div>
    <div class="modal modal-cart" id="add-to-cart"><a class="modal__close" href="" rel="modal:close">×</a>
        <div class="modal__body">
            <div class="modal__header">Товар добавлен в корзину</div>
            <div class="modal-cart__inner"><img class="modal-cart__image" src="images/catalog/product-2.png">
                <div class="modal-cart__name">Dream Look — сексуаьный, длинный парик с открытым лбом</div><a class="btn" href="/personal/cart/">Перейти в корзину</a>
            </div>
        </div>
    </div>
</div>

<?
if ($arParams["DISPLAY_BOTTOM_PAGER"])
{
	 echo $arResult["NAV_STRING"];
}

$interestingProductsCount = 12 - count($arResult["ITEMS"]);
if($interestingProductsCount){
	include(__DIR__."/interesting-products.php");
}

if(
	$curUri == "/catalog/pariki/" OR
	$curUri == "/catalog/golovnye-ubory/" OR
	$curUri == "/catalog/nakladki/"OR
    $curUri == "/catalog/shinony/"
):
	if(
		$arParams['HIDE_SECTION_DESCRIPTION'] !== 'Y' &&
		!empty($arResult["DESCRIPTION"])
	)
	: ?>
	<div class="catalog-description">
		<?= $arResult["DESCRIPTION"]; ?>
	</div>
	<?
	endif;
endif;

global $sotbitSeoMetaBottomDesc;
if(
	!empty($sotbitSeoMetaBottomDesc) &&
	empty($_GET["PAGEN_1"]) &&
	empty($_GET["sort"])
){ ?>
	<div class="catalog-description">
		<?= $sotbitSeoMetaBottomDesc; ?>
	</div> <?
}



