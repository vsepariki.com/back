<?
namespace CatalogSectionSeoHandler\PageTypes;

class Pager implements PageTypesInterface
{
    public $pageNumberString;

    public function __construct($pageParam)
    {
        $this->pageNumberString = "Страница ".(int)$pageParam;
    }

    public function getWigsTitle(): string
    {
        return "Парики / ".$this->pageNumberString;
    }

    public function getWigsDescription(): string
    {
        return "Каталог из 200 моделей в наличии. Низкие цены, доставка по России. Купить хороший парики европейского качества недорого. Не отличить от своих волос. ".$this->pageNumberString;
    }

    public function getHatsTitle(): string
    {
        return "Головные уборы / ".$this->pageNumberString;
    }

    public function getHatsDescription(): string
    {
        return "Широкий выбор головных уборов после химиотерапии и при облысении. Около 100 моделей в наличии для женщин. ".$this->pageNumberString;
    }

    public function getCoversTitle(): string
    {
        return "Накладки для волос / ".$this->pageNumberString;
    }

    public function getCoversDescription(): string
    {
        return "Каталог из 50 моделей в наличии. Низкие цены, доставка по России. Купить накладку для волос европейского качества. Не отличить от своих волос. ".$this->pageNumberString;
    }

    public function getShinonyTitle(): string
    {
        return "Шиньоны для волос / ".$this->pageNumberString;
    }

    public function getShinonyDescription(): string
    {
        return "Каталог из 24 моделей в наличии. Низкие цены, доставка по России. Купить шиньон для волос европейского качества. ".$this->pageNumberString;
    }

    public function getSotbitTitle($sectionName, $sotbitProps): string
    {
        return $sectionName." ".$sotbitProps." / ".$this->pageNumberString;
    }

    public function getDefaultTitle($sectionName, $filterParts): string
    {
        return trim($sectionName)." ".$filterParts." / ".$this->pageNumberString;
    }

}