<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$offersColors = array();

$propCode = false;
if($arItem["IBLOCK_SECTION_ID"] == WIGS_SECTION_ID){
	$propCode = "COLOR";
}
if($arItem["IBLOCK_SECTION_ID"] == HATS_SECTION_ID){
	$propCode = "COLOR_HAT";
}
if($arItem["IBLOCK_SECTION_ID"] == COVERS_SECTION_ID){
	$propCode = "COLOR_COVERS";
}
if($arItem["IBLOCK_SECTION_ID"] == SHINONY_SECTION_ID){
    $propCode = "COLOR_SHINONY";
}

$fullSeoName = $arItem["NAME"];
if($titleForSite){
    $fullSeoName .= " ".$titleForSite;
}

$pictureType = "default";
if(!empty($arItem['PRODUCT_PREVIEW']['SRC'])){
    $pictureType = 'PRODUCT_PREVIEW';
}elseif(!empty($arItem['PRODUCT_PREVIEW_SECOND']['SRC'])){
    $pictureType = 'PRODUCT_PREVIEW_SECOND';
}
?>


    <a class="catalog-product__image" href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
        <?php if($pictureType == "default"): ?>
            <img
                src="<?= $defaultProductImgSrc ?>"
                alt=""
            >
            <?php if(!empty($arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'])){?>
                <div class="catalog-product__discount"><?php echo $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT']?>%</div>

            <?php }?>

        <?php else: ?>
            <img
                src="<?= $arItem[$pictureType]['SRC'] ?>"
                alt="<?= $fullSeoName ?>"
                title="<?= $fullSeoName ?>"
            >
        <?php if(!empty($arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'])){?>
                <div class="catalog-product__discount"><?php echo $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT']?>%</div>

            <?php }?>

        <?php endif ?>
    </a>

    <div class="catalog-product__name">
        <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>"><?= $productName ?></a>
    </div>

    <div class="catalog-product__prices">
        <div class="catalog-product__price">

            <?= $arItem["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE"] ?>
        </div>
        <?php if($arItem["PRICES"]["BASE"]["DISCOUNT_DIFF"]): ?>
            <div class="catalog-product__old-price">
                <?= $arItem["PRICES"]["BASE"]["PRINT_VALUE"] ?>
            </div>
        <?php endif; ?>
        <div class="catalog-product__old-price">
            <?php
            if (empty($arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'])){
                $price = $arItem["PRICES"]["BASE"]["VALUE"];
                $fullPrice = $price + ($price * 0.2); // + 20% к базовой стоимости
                $fullPrice = number_format($fullPrice, 0, ',', ' ');
                echo $fullPrice?>  руб
            <?php } ?>
        </div>

    </div>

<?php
foreach($arItem["OFFERS"] as $k => $offer) {
	$offerColorCode = $offer["PROPERTIES"][$propCode]["VALUE"];
	$offerColorMap = $arResult['SKU_PROPS'][IBLOCK_PRODUCTS_ID][$propCode]["XML_MAP"][$offerColorCode];
	$offerColorArr = $arResult['SKU_PROPS'][IBLOCK_PRODUCTS_ID][$propCode]["VALUES"][$offerColorMap];

    $offerColorTitle = $offerColorArr["NAME"];
	$offerColorSrc = $offerColorArr["PICT"]["SRC"];
	$offersColors[$k] = array('title' => $offerColorTitle, 'src' => $offerColorSrc, 'id' => $offerColorMap);

}
?>
<div class="catalog-product__colors">
	<div class="catalog-product-colors">
        <div class="catalog-product__loader"></div>
		<ul class="catalog-product-colors__list" id="colorList">

		</ul>

	</div>
    <div class="catalog-product-buy"><a class="product-buy-one-click modal-link-buy-one-click catalog-product-buy__one-click" href="#buy-one-click">Купить в 1 клик</a>
        <a class="btn catalog-product-buy__button modal-link-add-to-cart" href="#add-to-cart" data-url="<?=$arItem['ADD_URL']?>&ajax_basket=Y&quantity=1" data-curr-price="<?php echo $fullPrice ?>" data-id="<?=$arItem['ID']?>">В корзину</a>

        <a class="btn catalog-product-buy__button catalog-product-buy__button--pre-order modal-link-availability hidden" href="#availability">Предзаказ</a>
    </div>
</div>
