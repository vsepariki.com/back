<?
setEmptyFilterSotbitMetaData();

global $sotbitSeoMetaH1;
$curUri  = $APPLICATION->GetCurUri();

$h1 = false;

if($sotbitSeoMetaH1){
	$h1 = $sotbitSeoMetaH1;
}
else{
	$h1 = $arResult["NAME"];
}

if($curUri == "/catalog/pariki/"){
	$h1 = "Купить парик";
}

if($curUri == "/catalog/nakladki/"){
	$h1 = "Купить накладку для волос";
}

if($curUri == "/catalog/shinony/"){
    $h1 = "Шиньоны";
}

if($h1){
	$arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] = $h1;
}