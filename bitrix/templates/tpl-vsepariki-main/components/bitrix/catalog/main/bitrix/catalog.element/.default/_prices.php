<div class="product-price block-wrap block-wrap_wrap block-wrap_nopadding">

	<?
	$minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
	$boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);
	?>

	<div class="item-product__fullprice-wrapper block-wrap__item block-wrap__item_xl-width3 block-wrap__item_m-width6">
		<p>Обычная цена</p>

		<?

        if (empty($arResult['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'])){
            $fullPrice = $minPrice["VALUE"] + ($minPrice["VALUE"] * 0.2); // + 20% к базовой стоимости
            $fullPrice = number_format($fullPrice, 0, ',', ' ');
        }else{
            $fullPrice = $minPrice["VALUE"];
            $fullPrice = number_format($fullPrice, 0, ',', ' ');


        }

		?>

		<span class="item-product__fullprice"><?= $fullPrice ?> руб.</span>
	</div>

	<div class="item-product__discountprice-wrapper block-wrap__item block-wrap__item_xl-width3 block-wrap__item_m-width6">
		<p>Акционная цена</p>
		<div class="item_current_price" id="<? echo $arItemIDs['PRICE']; ?>">
			<? echo $minPrice['PRINT_DISCOUNT_VALUE']; ?>
		</div>
		<? include "_quick_buy.php"; ?>
	</div>

	<div class="item-product__discount-period block-wrap__item block-wrap__item_xl-width6 block-wrap__item_m-width12">
		<p>До окончания акции:</p>
		<p id="countdown"></p>
	</div>

</div>
