<?
$propArrCovers = [];

// Тип
if(!empty($arResult["PROPERTIES"]["TYPE_COVER"]["VALUE_ENUM_ID"])){
    $propArrCovers["TYPE_COVER"] = $arResult["PROPERTIES"]["TYPE_COVER"]["VALUE_ENUM_ID"];
}

// Категория длины волос
if(!empty($arResult["PROPERTIES"]["LENGTH_COVER"]["VALUE_ENUM_ID"])){
    $propArrCovers["LENGTH_COVER"] = $arResult["PROPERTIES"]["LENGTH_COVER"]["VALUE_ENUM_ID"];
}

if($propArrCovers)
{

    global $similarCoversFilter;
    $similarCoversFilter = [
        [
            "LOGIC" => "AND",
            "PROPERTY" => $propArrCovers
        ],
        "!ID" => $arResult["ID"]
    ];

    $APPLICATION->IncludeComponent(
        "bitrix:catalog.section",
        "similar-products",
        array(
            "MAIN_PRODUCT_PROPS" => $propArrCovers,
            "ACTION_VARIABLE" => "action",
            "ADD_PICT_PROP" => "-",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "ADD_TO_BASKET_ACTION" => "ADD",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "BACKGROUND_IMAGE" => "-",
            "BASKET_URL" => "/personal/basket.php",
            "BROWSER_TITLE" => "-",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "CONVERT_CURRENCY" => "N",
            "DETAIL_URL" => "",
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "ELEMENT_SORT_FIELD" => "show_counter",
            "ELEMENT_SORT_FIELD2" => "active_from",
            "ELEMENT_SORT_ORDER" => "desc",
            "ELEMENT_SORT_ORDER2" => "desc",
            "FILTER_NAME" => "similarCoversFilter",
            "HIDE_NOT_AVAILABLE" => "N",
            "IBLOCK_ID" => "9",
            "IBLOCK_TYPE" => "CATALOG",
            "INCLUDE_SUBSECTIONS" => "Y",
            "LABEL_PROP" => "-",
            "LINE_ELEMENT_COUNT" => "4",
            "MESSAGE_404" => "",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "META_DESCRIPTION" => "-",
            "META_KEYWORDS" => "-",
            "OFFERS_CART_PROPERTIES" => array(
                0 => "COLOR_COVERS",
            ),
            "OFFERS_FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "OFFERS_LIMIT" => "",
            "OFFERS_PROPERTY_CODE" => array(
                0 => "COLOR_COVERS",
                1 => "",
            ),
            "OFFERS_SORT_FIELD" => "show_counter",
            "OFFERS_SORT_FIELD2" => "id",
            "OFFERS_SORT_ORDER" => "desc",
            "OFFERS_SORT_ORDER2" => "desc",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => ".default",
            "PAGER_TITLE" => "Товары",
            "PAGE_ELEMENT_COUNT" => "6",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRICE_CODE" => array(
                0 => "BASE",
            ),
            "PRICE_VAT_INCLUDE" => "Y",
            "PRODUCT_DISPLAY_MODE" => "Y",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPERTIES" => array(
            ),
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "",
            "PRODUCT_SUBSCRIPTION" => "N",
            "PROPERTY_CODE" => array(
                0 => "",
                1 => "",
            ),
            "SECTION_CODE" => "",
            "SECTION_CODE_PATH" => "",
            "SECTION_ID" => "18",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "SECTION_URL" => "",
            "SECTION_USER_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SEF_MODE" => "Y",
            "SEF_RULE" => "",
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SHOW_ALL_WO_SECTION" => "N",
            "SHOW_CLOSE_POPUP" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_OLD_PRICE" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "TEMPLATE_THEME" => "blue",
            "USE_MAIN_ELEMENT_SECTION" => "N",
            "USE_PRICE_COUNT" => "N",
            "USE_PRODUCT_QUANTITY" => "N",
            "HIDE_SECTION_DESCRIPTION" => "Y",
            "COMPONENT_TEMPLATE" => "similar-products"
        )
        );
    }