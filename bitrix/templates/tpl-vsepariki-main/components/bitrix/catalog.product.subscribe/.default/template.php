<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc;

CJSCore::init(array('popup', 'ajax'));

$this->setFrameMode(true);

$landingId = null;
if (is_callable(["LandingPubComponent", "getMainInstance"]))
{
	$instance = \LandingPubComponent::getMainInstance();
	$landingId = $instance["SITE_ID"];
}

$strMainId = $this->getEditAreaId($arResult['PRODUCT_ID']);
$jsObject = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainId);
$paramsForJs = array(
	'buttonId' => $arResult['BUTTON_ID'],
	'jsObject' => $jsObject,
	'alreadySubscribed' => $arResult['ALREADY_SUBSCRIBED'],
	'listIdAlreadySubscribed' => (!empty($_SESSION['SUBSCRIBE_PRODUCT']['LIST_PRODUCT_ID']) ?
		$_SESSION['SUBSCRIBE_PRODUCT']['LIST_PRODUCT_ID'] : []),
	'productId' => $arResult['PRODUCT_ID'],
	'buttonClass' => htmlspecialcharsbx($arResult['BUTTON_CLASS']),
	'urlListSubscriptions' => '/',
	'landingId' => ($landingId ? $landingId : 0)
);

$showSubscribe = true;

/* Compatibility with the sale subscribe option */
$saleNotifyOption = Bitrix\Main\Config\Option::get('sale', 'subscribe_prod');
if($saleNotifyOption <> '')
	$saleNotifyOption = unserialize($saleNotifyOption, ['allowed_classes' => false]);
$saleNotifyOption = is_array($saleNotifyOption) ? $saleNotifyOption : array();
foreach($saleNotifyOption as $siteId => $data)
{
	if($siteId == SITE_ID && $data['use'] != 'Y')
		$showSubscribe = false;
}
$templateData = $paramsForJs;
$templateData['showSubscribe'] = $showSubscribe;

$subscribeBtnName = !empty($arParams['MESS_BTN_SUBSCRIBE']) ? $arParams['MESS_BTN_SUBSCRIBE'] : Loc::getMessage('CPST_SUBSCRIBE_BUTTON_NAME');

if($showSubscribe):?>

               <div class="modal" id="availability">
                    <a class="modal__close" href="" rel="modal:close">×</a>
                    <div class="modal_body">
                      <div class="modal__header">Сообщить о поступлении</div>
                      <form method="post" class="modal-availability modal_form_subscribe subscribe validated-form submitJS" name="modal-availability">
						<input type="hidden" name="product_art" class="product_art"  value="">
						<input type="hidden" name="product" value="<?=$arResult['PRODUCT_ID'];?>">
                        <div class="form-field form-field-phone">
                          <label for="name">Имя</label><input id="name" type="text" name="name"
                            class="validated required">
                          <div class="validation-msg validation-msg-required">Обязательное поле</div>
                        </div>
                        <div class="form-field form-field-phone">
                          <label for="phone">Телефон</label><input id="phone" type="tel" name="phone"
                            class="validated required" placeholder="+7 (___) ___-__-__">
                          <div class="validation-msg validation-msg-required">Обязательное поле</div>
                        </div>
                        <input type="submit" value="Заказать" class="btn">
                        <div class="form-field modal-availability__agreement">
                          <input type="checkbox" id="modal-availability__agreement" name="modal-availability__agreement"
                            class="validated required" checked="" required="">
                          <label for="modal-availability__agreement">Нажимая на кнопку, я даю свое <a
                              href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">согласие на обработку своих
                              персональных данных</a>.</label>
                        </div>
                      </form>
                    </div>
                  </div>
			<a id="<?=htmlspecialcharsbx($arResult['BUTTON_ID'])?>"
						class="btn btn-big modal-link-availability"
						data-item="<?=htmlspecialcharsbx($arResult['PRODUCT_ID'])?>"
						style="<?=($arResult['DEFAULT_DISPLAY']?'':'display: none;')?>" href="#availability">
					<span>
						Предзаказ
					</span>
				</a>
	<input type="hidden" id="<?=htmlspecialcharsbx($arResult['BUTTON_ID'])?>_hidden">

	<script type="text/javascript">
		BX.message({
			CPST_SUBSCRIBE_POPUP_TITLE: '<?=GetMessageJS('CPST_SUBSCRIBE_POPUP_TITLE');?>',
			CPST_SUBSCRIBE_BUTTON_NAME: '<?=$subscribeBtnName?>',
			CPST_SUBSCRIBE_BUTTON_CLOSE: '<?=GetMessageJS('CPST_SUBSCRIBE_BUTTON_CLOSE');?>',
			CPST_SUBSCRIBE_MANY_CONTACT_NOTIFY: '<?=GetMessageJS('CPST_SUBSCRIBE_MANY_CONTACT_NOTIFY');?>',
			CPST_SUBSCRIBE_LABLE_CONTACT_INPUT: '<?=GetMessageJS('CPST_SUBSCRIBE_LABLE_CONTACT_INPUT');?>',
			CPST_SUBSCRIBE_VALIDATE_UNKNOW_ERROR: '<?=GetMessageJS('CPST_SUBSCRIBE_VALIDATE_UNKNOW_ERROR');?>',
			CPST_SUBSCRIBE_VALIDATE_ERROR_EMPTY_FIELD: '<?=GetMessageJS('CPST_SUBSCRIBE_VALIDATE_ERROR_EMPTY_FIELD');?>',
			CPST_SUBSCRIBE_VALIDATE_ERROR: '<?=GetMessageJS('CPST_SUBSCRIBE_VALIDATE_ERROR');?>',
			CPST_SUBSCRIBE_CAPTCHA_TITLE: '<?=GetMessageJS('CPST_SUBSCRIBE_CAPTCHA_TITLE');?>',
			CPST_STATUS_SUCCESS: '<?=GetMessageJS('CPST_STATUS_SUCCESS');?>',
			CPST_STATUS_ERROR: '<?=GetMessageJS('CPST_STATUS_ERROR');?>',
			CPST_ENTER_WORD_PICTURE: '<?=GetMessageJS('CPST_ENTER_WORD_PICTURE');?>',
			CPST_TITLE_ALREADY_SUBSCRIBED: '<?=GetMessageJS('CPST_TITLE_ALREADY_SUBSCRIBED');?>',
			CPST_POPUP_SUBSCRIBED_TITLE: '<?=GetMessageJS('CPST_POPUP_SUBSCRIBED_TITLE');?>',
			CPST_POPUP_SUBSCRIBED_TEXT: '<?=GetMessageJS('CPST_POPUP_SUBSCRIBED_TEXT');?>'
		});

		// Показать модальное окно "Сообщить о поступлении"
		/*$('#<?=htmlspecialcharsbx($arResult['BUTTON_ID'])?>').click(function() {
			$(this).modal({
				showClose: false
			});
			return false;
		});*/
		//var <?=$jsObject?> = new JCCatalogProductSubscribe(<?=CUtil::phpToJSObject($paramsForJs, false, true)?>);
	</script>
<?endif;