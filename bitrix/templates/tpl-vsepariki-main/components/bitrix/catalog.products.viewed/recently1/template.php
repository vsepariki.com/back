<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$defaultProductImgSrc = '/upload/medialibrary/baa/baaa03452391641f164ed2e95b71384f.jpg';

if (!empty($arResult['ITEMS']))
{
?>
<div class="recently-viewed">
	<h2>Просмотренные раннее товары</h2>
	<div class="block-wrap product-viewed block-wrap_wrap ">
		<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
		<?
		    $titleForSite = "";
			if(empty($arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])){
				$productName = $arItem["NAME"];
			}else{
				$titleForSite = $arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
				$productName = $arItem["NAME"]." | ".$titleForSite;
			}
		?>
			<div class="block-wrap__item block-wrap__item_xl-width2 block-wrap__item_l-width2 block-wrap__item_m-width2 block-wrap__item_s-width3 product-viewed__item">
				<div class="catalog-product">
				<?
					if(!empty($arItem["OFFERS"])){
						 include __DIR__.'/_list-product-complex.php';
					}
					else{
						 include __DIR__.'/_list-product-simple.php';
					}
				?>
				</div>
			</div>
		<? endforeach; ?>
	</div>
</div>
<?
} // if (!empty($arResult['ITEMS']))