<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<? $APPLICATION->ShowHead(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">
		<title><? $APPLICATION->ShowTitle(); ?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
		<?
			$numbPage = ($APPLICATION->GetCurPage() == '/') ? 'first' : 'all';
			$APPLICATION->ShowPanel();
		?>
        <link href="/local/front/template/styles/core.css?v=2" rel="stylesheet">

        <? /*
		<script type="text/javascript">
		var __cs = __cs || [];
		__cs.push(["setCsAccount", "KggM6rPznstIFS43VCgGYviXrUs_vE0e"]);
		__cs.push(["setCsHost", "//server.comagic.ru/comagic"]);
		</script>
		<script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>
	*/ ?>
	</head>

	<body class="page<?= ' '.$numbPage; ?>">

		<?
		global $USER;
		if (!$USER->IsAdmin()){
			blockInclude('header', 'header-scripts');
		}
		?>

		<div class="container appointment">
			<div class="content">
				<p>Предварительная запись в салон обязательна, так как консультация и примерка занимает около 1 часа</p>
			</div>
		</div>
		<? blockInclude('header', 'header-menu-sites'); ?>


		<header class="header-main">

			<div class="container header-main__top">
				<div class="content">

				 <div class="block-wrap block-wrap_align-center s-hidden ">

					<a class="block-wrap__item header-main__logo" href="/"></a>

					<div class="block-wrap__item header-main__nav">
						<? blockInclude('header', 'header-menu-services'); ?>
						<div class="block-wrap block-wrap_justify-space-between header-info ">
							<div class="block-wrap__item header-info__title">
								<? blockInclude('header', 'header-info_title', false); ?>
							</div>
							<? blockInclude('header', 'header-info_phone', false); ?>
						</div>
					</div>

					<? blockInclude('header', 'header-basket'); ?>

				</div>

				<? blockInclude('header', 'header-logo-mobile'); ?>

			</div> <!-- content -->
		</div> <!-- header-main__top -->

		<div class="container header-main__bottom">
			<div class="content">
				<? blockInclude('header', 'header-menu-products'); ?>
				<? blockInclude('header', 'header-menu-mobile'); ?>
		  </div> <!-- content -->
		</div> <!-- header-main__bottom -->

		<? /* <div class="container">
			<div class="content">
				<div class="header__banner" style="text-align:center; border:1px solid #7b66a1; padding-top:1rem; padding-bottom:1rem; margin-top:1rem;">
					<p style="margin-bottom:0;"><strong>8&nbsp;марта&nbsp;&mdash; выходной. Поздравляем с&nbsp;праздником!</strong></p>
				</div>
			</div>
		</div> 

		<div class="container banner-schedule"><a class="banner-schedule__title content modal-link" href="#new-year">График работы в новогодние праздники</a>
			<div class="modal" id="new-year"><a class="modal__close" href="" rel="modal:close">×</a>
				<div class="modal__header"></div>
				<p>
					30&nbsp;декабря&nbsp;&mdash; 10:00&nbsp;&mdash; 19:00 <br> с&nbsp;31&nbsp;декабря по&nbsp;3&nbsp;января&nbsp;&mdash; выходной 
					<br> с&nbsp;4&nbsp;января по&nbsp;6&nbsp;января&nbsp;&mdash; 10:00&nbsp;&mdash; 19:00
					<br> 7&nbsp;января&nbsp;&mdash; выходной
					<br> 8&nbsp;января&nbsp;&mdash; 10:00&nbsp;&mdash; 19:00
					<br> 10&nbsp;января&nbsp;&mdash; 10:00&nbsp;&mdash; 15:00
				</p>
			</div>
		</div> */?>
	</header>

	<main>

	<? if($numbPage == 'all'): ?>
		<div class="container<?= $APPLICATION->AddBufferContent('getPageWrapperClass'); ?>">
			<div class="content">

			<? blockInclude('header', 'header-breadcrumbs'); ?>

			<?
				if(
					!preg_match('|^/catalog/(.*)/$|', $curPage) AND
					!preg_match('|^/konsultatsiya/([^/]+)/([^/]+).html$|', $curPage)
				): //не выводим h1 из шаблона на стр. Раздела каталога
				?>
				<h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>
			<? endif; ?>

	<? endif; ?>
