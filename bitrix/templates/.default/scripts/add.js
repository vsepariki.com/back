jQuery(document).ready(function(){
    let href = '';
    let clickedByClick = false;
    let clickedInput = false;
    let offerId = '';
    let modalName = document.querySelector('.modal-cart__name')
    let rightPart = '';
    let leftPart = ''
    const processedProducts = new Set(); // Хранит уже обработанные элементы
    let productNameElement = ''
    let colorList = document.querySelector('.catalog-product-colors__list')
    const productLoadData = (productParent) => {

        // Проверяем, что родительский элемент найден и он еще не был обработан
        if (!productParent || processedProducts.has(productParent)) return;

        processedProducts.add(productParent); // Добавляем элемент в Set, чтобы не обрабатывать его снова

        productNameElement = productParent.querySelector('.catalog-product__name a');

        // Проверяем, что элемент найден
        if (productNameElement) {


            // Получаем ссылку на продукт
            const href = productNameElement.getAttribute('href');

            fetch(href)
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.text();
                })
                .then(html => {
                    const parser = new DOMParser();
                    const doc = parser.parseFromString(html, 'text/html');
                    const offersColorsList = productParent.querySelector('.catalog-product-colors__list');

                    // Очищаем список перед добавлением новых элементов
                    offersColorsList.innerHTML = '';

                    // Массив для хранения новых элементов
                    let offersColors = [];

                    // Находим первый доступный data-id
                    let firstAvailableDataId = null;
                    const listItems = doc.querySelectorAll('li[data-onevalue]');
                    for (const listItem of listItems) {
                        if (listItem.querySelector('.availability-sign') !== null) {
                            const cntItems = listItem.querySelectorAll('.cnt_item');
                            firstAvailableDataId = cntItems[0].getAttribute('data-id');
                            break; // Выходим из цикла, как только нашли первый
                        }
                    }

                    // Используем найденный firstAvailableDataId для кнопки
                    if (firstAvailableDataId) {
                        const button = productParent.querySelector('.catalog-product-buy__button.modal-link-add-to-cart');
                        if (button) {
                            const url = button.getAttribute('data-url');
                            const newUrl = url.replace(/id=\d+/, `id=${firstAvailableDataId}`);
                            button.setAttribute('data-url', newUrl);
                        }
                    }

                    let checkAvailable = false;

                    listItems.forEach(listItem => {
                        const cntItems = listItem.querySelectorAll('.cnt_item');

                        cntItems.forEach(item => {
                            const style = item.getAttribute('style');
                            const urlMatch = style.match(/url\(['"]?([^'")]+)['"]?\)/);

                            if (urlMatch) {
                                const imageUrl = urlMatch[1];
                                const title = item.getAttribute('title');
                                const dataIdName = item.getAttribute('data-id');

                                // Создаем новый элемент label
                                const label = document.createElement('label');
                                if (listItem.querySelector('.availability-sign') !== null) {
                                    label.className = "catalog-product-colors__item catalog-product-colors__item--available";
                                    checkAvailable = true;
                                } else {
                                    label.className = "catalog-product-colors__item catalog-product-colors__item--unavailable";
                                }

                                // Создаем img элемент
                                const img = document.createElement('img');
                                img.src = imageUrl;
                                img.setAttribute('data-src', imageUrl);
                                img.alt = "";
                                img.title = title;

                                // Создаем input элемент
                                const input = document.createElement('input');
                                input.className = "catalog-product-colors__input";
                                input.type = "radio";
                                input.name = "color";
                                input.setAttribute('data-id', dataIdName);
                                input.value = title;
                                input.checked = true;

                                // Добавляем img и input в label
                                label.appendChild(img);
                                label.appendChild(input);
                                offersColors.push(label);

                                let loaders = document.querySelectorAll('.catalog-product__loader')

                                if (document.querySelector('.catalog-product-colors__item') !== null) {
                                    loaders.forEach((item) => {
                                        item.classList.add('hidden')
                                    })
                                }
                            }
                        });
                    });

                    // Обновляем наличие
                    if (checkAvailable && !productParent.querySelector('.catalog-product-colors__availability')) {
                        const availabilityDiv = document.createElement('div');
                        availabilityDiv.className = 'catalog-product-colors__availability';
                        availabilityDiv.textContent = 'В наличии';
                        productParent.querySelector('.catalog-product-colors').appendChild(availabilityDiv);
                    } else {
                        productParent.querySelector('.modal-link-availability').classList.remove('hidden');
                        productParent.querySelector('.modal-link-add-to-cart').classList.add('hidden');
                        productParent.querySelector('.modal-link-buy-one-click').classList.add('hidden');
                    }

                    // Добавляем все созданные элементы в offersColorsList
                    offersColors.forEach(color => {
                        offersColorsList.appendChild(color);
                    });
                    productParent.querySelectorAll('.catalog-product-colors__item--unavailable input').forEach((input) => {
                        input.setAttribute('disabled', 'disabled');
                    });
                })
                .catch(error => {
                    console.error('There was a problem with the fetch operation:', error);
                });

        }
    }

    const observer = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                const productParent = entry.target;
                productLoadData(productParent);
                observer.unobserve(productParent);
            }
        });
    }, {
        rootMargin: '100px 0px',
        threshold: 0.01
    });
    const productCards = document.querySelectorAll('.catalog-product');
    productCards.forEach((product) => observer.observe(product));


    // Находим все кнопки с классом 'test'
    let buttons = document.querySelectorAll('.catalog-product-buy__button');

// Обрабатываем клик по каждой кнопке
    buttons.forEach(function(button) {
        button.addEventListener('click', function(event) {
            const parent = event.target.closest('.catalog-product')
            const picture = parent.querySelector('.catalog-product__image img').getAttribute('src')
            const productName = parent.querySelector('.catalog-product__name a').textContent
            const modalPicture = document.querySelector('.modal-cart__image')

            modalPicture.setAttribute('src',picture)
            modalName.textContent = productName

            let dataId = event.target.getAttribute('data-url');

            // Предотвращаем переход по ссылке, если кнопка находится в <a>
            event.preventDefault();

            // Получаем необходимую информацию
            var siteId = 's1'; // Замените на нужный Вам siteId
            var templateName = '.default';   // Ваш templateName

            // Данные для передачи
            var data = {
                siteId: siteId,
                templateName: templateName,
                sessid: BX.bitrix_sessid(),
                quantity: 1 // Как пример, число можно изменить по вашим требованиям
            };

            // Выполняем AJAX запрос
            fetch(dataId, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data) // Отправляем данные в формате JSON
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok ' + response.statusText);
                    }
                    return response.json(); // Ожидаем JSON ответ
                })
                .then(data => {
                    if (data.STATUS === 'OK'){
                        BX.onCustomEvent('OnBasketChange'); // Обновляем корзину

                    }
                })
                .catch(error => {
                    console.error('Ошибка:', error);
                });
        });
    });
    document.addEventListener('click',(e)=>{

        if (e.target.classList.contains('catalog-product-colors__input')){
            let offerId = e.target.getAttribute('data-id')
            clickedInput = true
            productNameElement = e.target.closest('.catalog-product')
            productNameElement = productNameElement.querySelector('.catalog-product__name a')
            let desc = productNameElement.textContent
            leftPart = desc.split('|')[0].trim();
            rightPart = desc.split('|')[1].trim()

            const modalHeaderElement = document.querySelector('.quickBuy .modal__header');
            const formQuickForm = document.querySelector('.quick-buy')
            const fieldsQuickBy = formQuickForm.querySelector('input[name="productId"]')
            console.log(e.target.value)
            // Проверяем, что элемент найден


            if(offerId !== null){
                let parentElement = e.target.closest('.catalog-product')
                let cardActionStrAttr = parentElement.querySelector('.modal-link-add-to-cart').getAttribute('data-url')
                let cardActionStr = parentElement.querySelector('.modal-link-add-to-cart')
                cardActionStrAttr = cardActionStrAttr.replace(/id=\d+/, `id=${offerId}`)
                cardActionStr.setAttribute('data-url', cardActionStrAttr);

            }
            if (clickedInput ===  true){
                modalName.textContent = leftPart + '(' + e.target.value + ')' + ' | ' + rightPart
                modalHeaderElement.textContent = 'Заказать' + ' ' + leftPart + '(' + e.target.value + ')'

            }
        }

    })
    document.addEventListener('mouseover',(e)=>{
        if (e.target.classList.contains('modal-link-availability')){
            console.log('kkk')
            let  productParent = e.target.closest('.catalog-product')
            let dataId = productParent.querySelector('.modal-link-add-to-cart').getAttribute('data-id')
            let formAvailability = document.querySelector('.modal-availability ')
            let fields = formAvailability.querySelector('input[name="product"]')
            fields.value = dataId

        }
        if (e.target.classList.contains('catalog-product-buy__one-click')){
            clickedByClick = true

            let  productParent = e.target.closest('.catalog-product')
            productNameElement = productParent.querySelector('.catalog-product__name a')
            if (clickedInput == false){
                offerId = productParent.querySelector('.modal-link-add-to-cart').getAttribute('data-id')

            }else {
                offerId = productParent.querySelector('.catalog-product-colors__input').getAttribute('data-id')

            }


            let desc = productNameElement.textContent
            leftPart = desc.split('|')[0].trim();
            rightPart = desc.split('|')[1].trim()

            const modalHeaderElement = document.querySelector('.quickBuy .modal__header');
            const formQuickForm = document.querySelector('.quick-buy')
            const fieldsQuickBy = formQuickForm.querySelector('input[name="productId"]')
            // Проверяем, что элемент найден
            // Вставляем левую часть текста вместо названия парика
            fieldsQuickBy.value = offerId
            if (modalHeaderElement &&  clickedInput == false) {
                // Вставляем левую часть текста вместо названия парика
                modalHeaderElement.textContent = 'Заказать ' + ' ' +  leftPart;
                fieldsQuickBy.value = offerId

            }


        }
    })

    let isRequestSent = false;

    document.addEventListener('mousemove', (e) => {
        if (!isRequestSent) {
            isRequestSent = true; // Задаем флаг, чтобы запрос был выполнен только один раз

            fetch('https://vsepariki.com/ajax/ajax.php', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Cache-Control': 'no-cache, no-store, must-revalidate',
                    'Pragma': 'no-cache',
                    'Expires': '0'
                },
                body: JSON.stringify({
                    status: 'ok'
                })
            })
                .then(response => {
                    if (response.ok) {
                        console.log('Запрос успешно выполнен');
                    } else {
                        console.error('Ошибка при выполнении запроса:', response.status);
                    }
                })
                .catch(error => {
                    console.error('Произошла ошибка:', error);
                });
        }
    });



    $.maskfn.definitions['~']='[+-]';
    $('body').delegate('input[type="tel"], input.phone', 'focus', function(){
        $(this).maskfn('+7 (999) 999-99-99');
    });

    $('.modal-link').click(function() {
        $(this).modal({
            showClose: false
        });
        return false;
    });

// Показать модальное окно "Сообщить о поступлении"
    $('.modal-link-availability').click(function() {
        let product_id = $(this).data('item');
        $('.product_art').val(product_id);
        $(this).modal({
            showClose: false
        });
        return false;
    });

    /* Показать окно "Купить в 1 клик" только если выбрано ТП - иначе показать ошибку */
    $('.modal-link-buy-one-click').click(function() {

        var sliderHTML = $(".bx_scu").html();
        if(sliderHTML && sliderHTML.indexOf("bx_active") == -1){
            $("#select_color_first").show();
            return false;
        }

        $(this).modal({
            showClose: false
        });
        return false;
    });

    $('#buy-one-click').on($.modal.OPEN, function(event, modal) {
        $("input[type='tel']", $(modal.elm)).focus();
    });

    $('form.subscribe').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            handler: 'addFormDataSubscribeProduct',
            message: {
                title: 'Подписка оформлена',
                text: '<p>Мы оповестим Вас при поступлении товара.</p>',
                close: 'Закрыть',
            }
        });
    });

    $('form.quick-buy').on('submitJS', function(event){
        ajaxFormSubmit({
            event:   event,
            handler: 'addFormDataQuickBuy/index',
            message: {
                title: 'Заказ оформлен',
                text:  '<p>Перезвоним в течении часа рабочего времени.<br>Ежедневно с 10 до 21</p>',
                close: 'Закрыть',
            }
        });
    });

    $('form.consultation-ask-form').on('submitJS', function(event){
        ajaxFormSubmit({
            event:    event,
            handler:  'addFormDataConsultation',
            redirect: '/konsultatsiya/answer_ask-question.html',
            //successAction: 'yaCounter39305555.reachGoal("ask_question_submit")'
        });
    });

    $('.home-filter-form').on('submit', function(e){
        var choices = $(this).serialize();
        choices = choices.replace(/&/g, '/');
        choices = choices.replace(/=/g, '-is-');
        window.location.href = '/catalog/pariki/filter/' + choices + '/apply/';
        e.preventDefault();
    });

    var bxInclArea = $('header [id*="bx_incl_area"], footer [id*="bx_incl_area"]');
    bxInclArea.each(function(i, e){
        var child = $(e).find(':first');
        var childClass = child.attr('class');
        if(childClass){
            child.attr('class', 'block-wrap__item block-wrap__item_xl-width12');
            $(e).attr('class', childClass).css('display', 'flex');
        }
    });

    /* -> Привязка целей метрики */
    function reachYaGoal(selector, event, goalId){
        $(selector).on(event, function () {
            //yaCounter39305555.reachGoal(goalId);
        });
    }

    reachYaGoal('.product-buy__button', 'click', 'add_to_basket_click'); // нажатие кнопки "Добавить в корзину" на странице товара
    reachYaGoal('.product-buy-one-click', 'click', 'buy_1_click'); // нажатие кнопки "Купить в 1 клик" на странице товара
    reachYaGoal('.bx_ordercart_order_pay_center a', 'click', 'order_click'); // нажатие кнопки "Оформить заказ" на странице корзины
    reachYaGoal('#bx-soa-orderSave a', 'click', 'final_order_click'); // нажатие кнопки "Оформить заказ" на странице оформления заказа

    $('.bx-soa-more-btn a').on('click', function () {
        var selectedSection = $('.bx-soa-section.bx-selected');
        if(selectedSection.length){
            if(selectedSection.attr('id') == 'bx-soa-region'){
                //yaCounter39305555.reachGoal('to_chose_shipping_region_click'); // переход к выбору региона доставки
            }
            if(selectedSection.attr('id') == 'bx-soa-delivery'){
                //yaCounter39305555.reachGoal('to_chose_shipping_method_click'); // переход к выбору способа доставки
            }
            if(selectedSection.attr('id') == 'bx-soa-paysystem'){
                //yaCounter39305555.reachGoal('to_chose_payment_method_click'); // переход к выбору способа оплаты
            }
            if(selectedSection.attr('id') == 'bx-soa-properties'){
                //yaCounter39305555.reachGoal('to_enter_customer_info_click'); // переход к вводу информации о покупателе
            }
        }
    });
    /* <- Привязка целей метрики */

// Подгрузка иконок цветов ТП по наведению на блок товара (в списках)
// $(".catalog-product").on("mouseenter", loadTpImgOnHover);
// $(".catalog-product").on("focus", loadTpImgOnHover);

// function loadTpImgOnHover(e)
// {
//     if(e.target.className == "catalog-product"){
//         var parent = $(e.target);
//     }else{
//         var parent = $(e.target).parents(".catalog-product");
//     }

//     var colorsImg = $(".catalog-product-colors__item img", parent);

//     colorsImg.each(function(){
//         var dataSrc = this.getAttribute("data-src");
//         if(dataSrc){
//             $(this).attr("src", dataSrc);
//         }
//     });
// }

    var colorsImg = $(".catalog-product-colors__item img");

    colorsImg.each(function(){
        var dataSrc = this.getAttribute("data-src");
        if(dataSrc){
            $(this).attr("src", dataSrc);
        }
    });

    var homeCatalogTab = $(".home-catalog__tab");
    homeCatalogTab.on("click", function (e) {
        var targetTabDataValue = this.getAttribute("data-home-catalog-tab");
        var targetTab = $(".home-catalog__tabs-content [data-home-catalog-tab='" + targetTabDataValue + "']");
        $(".catalog-product__offer img", targetTab).each(function(){
            var dataSrc = this.getAttribute("data-src");
            if(dataSrc){
                $(this).attr("src", dataSrc);
            }
        });
        $(".catalog-product__offer source", targetTab).each(function(){
            var dataSrc = this.getAttribute("data-srcset");
            if(dataSrc){
                $(this).attr("srcset", dataSrc);
            }
        });
    });

    const mrcSH = () => {
        const mrcSHTitles = Array.from(document.querySelectorAll('[data-mrc-sh-title]'));
        const mrcSHBodies = Array.from(document.querySelectorAll('[data-mrc-sh-body]'));
        mrcSHTitles.forEach((title) => {
            const itemObj = { name: title, bodies: [] };
            const itemName = title.dataset.mrcShTitle;
            const bodiesList = [];
            mrcSHBodies.forEach((body) => {
                const itemBody = body;
                const bodyName = itemBody.dataset.mrcShBody;
                if (itemName === bodyName) {
                    bodiesList.push(itemBody);
                    itemBody.style.display = 'none';
                }
            });
            if (bodiesList) {
                itemObj.bodies = bodiesList;
                itemObj.name.addEventListener('click', (e) => {
                    e.preventDefault();
                    bodiesList.forEach((item) => {
                        const currentItem = item;
                        currentItem.style.display = currentItem.style.display === 'none' ? '' : 'none';
                    });
                });
            }
        });
    };

    mrcSH();



}); //jQuery(document).ready
