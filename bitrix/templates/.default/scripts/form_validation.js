/***************** Functions *********************/
// set class "validated-form" to form to validate this form fields
// set class "validated" to field - to start validation of this field
// set class "validated-type-or" to field - to start validation of group fields with this class
// set class "required"  to field - set validation rule: field is required
// set class "min-chars_n"  to field - set validation rule: field should have at least n characters
// set class "max-chars_n"  to field - set validation rule: field should have no more then n characters

function formFieldsCheck(fields){
    var result = true;
    jQuery.each(fields, function(index, field){
        if(!formFieldCheck($(field))){
            result = false;
        }
    });
    return result;
}

function formFieldsCheckTypeOr(fields, form){
    var result = false;
    jQuery.each(fields, function(index, field){
        if(formFieldCheck($(field))){
            result = true;
            clearFieldsError(form);
            return false;
        }
    });
    return result;
}

function formFieldCheck(field)
{
    var result = true;

    if(field.hasClass('required')){
        result = formFieldCheckForRequired(field);
        if(!result){
            setFieldError(field, 'required');
            return false;
        }else{
            clearFieldError(field, 'required');
        }
    }

    if(field.hasClass('max-chars')){
        var maxChars = jQuery(field).attr('data-max-chars');
        if(!maxChars){maxChars = 18;}
        result = formFieldCheckForMaxChars(field, maxChars);
        if(!result){
            setFieldError(field, 'max-chars');
            return false;
        }else{
            clearFieldError(field, 'max-chars');
        }
    }

    if(field.hasClass('min-chars')){
        var minChars = jQuery(field).attr('data-min-chars');
        if(!minChars){minChars = 5;}
        result = formFieldCheckForMinChars(field, minChars);
        if(!result){
            setFieldError(field, 'min-chars');
            return false;
        }else{
            clearFieldError(field, 'min-chars');
        }
    }

    return true;
}

function formFieldCheckForRequired(field)
{
    var result = true;
    field = jQuery(field);

    var type = field.prop("type");

    if(type == 'checkbox'){
        if(!field.prop('checked')){
            result = false;
        }
    }else{
        if(!field.val()){
            result = false;
        }
    }

    return result;
}

function formFieldCheckForMaxChars(field, maxChars)
{
    var result = true;
    field    = jQuery(field);
    var fieldVal = field.val();

    if(fieldVal.length > maxChars){
        result = false;
    }

    return result;
}

function formFieldCheckForMinChars(field, minChars)
{
    var result = true;
    field    = jQuery(field);
    var fieldVal = field.val();

    if(fieldVal.length < minChars){
        result = false;
    }

    return result;
}

function setFieldError(field, errorType){
    var fieldParent = jQuery(field).parent();
    fieldParent.addClass('validation-error validation-error-' + errorType);

    if(field.hasClass('error-pulse')){
        fieldParent.addClass('error-show-pulse');
    }

}

function clearFieldError(field, errorType){
    var fieldParent = jQuery(field).parent();
    fieldParent.removeClass('validation-error validation-error-' + errorType);
}

function clearFieldsError(form){
    var removedClasses = 'validation-error';
    removedClasses += ' validation-error-required';
    removedClasses += ' validation-error-max-chars';
    removedClasses += ' validation-error-min-chars';
    jQuery('.validation-error', form).removeClass(removedClasses);
}
/***************** /Functions *********************/

jQuery(document).ready(function($)
{
    $('body').delegate('form.validated-form', 'submit', function(event)
    {
        var thisForm = $(this);

        var fieldsValidated       = $('.validated', thisForm);
        var fieldsValidatedTypeOr = $('.validated-type-or', thisForm);

        clearFieldsError(thisForm);

        if(fieldsValidated.length){
            if(!formFieldsCheck(fieldsValidated)){

                $('.validation-error').on('webkitAnimationEnd mozAnimationEnd msAnimationEnd oAnimationEnd animationEnd', function() {
                    $('.error-show-pulse').removeClass('error-show-pulse');
                });

                event.preventDefault();
                event.stopPropagation();
                return false;
            }
        }

        if(fieldsValidatedTypeOr.length){
            if(!formFieldsCheckTypeOr(fieldsValidatedTypeOr, thisForm)){
                event.preventDefault();
                event.stopPropagation();
                return false;
            }
        }

        if(thisForm.hasClass('submitJS')){
            event.preventDefault();
            thisForm.trigger('submitJS');
        }

    });

    $('body').delegate('.validated', 'change', function(){
        var thisForm = $(this).parents('form');
        clearFieldsError(thisForm);
        formFieldCheck($(this));
    });
    $('body').delegate('.validated', 'keydown', function(){
        var thisForm = $(this).parents('form');
        clearFieldsError(thisForm);
    });
/*
    $('.validated-type-or').on('change', function(){
        formFieldsCheckTypeOr($(this));
    });
*/

});


