<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();?>

<link href="/local/front/template/styles/rest.css?v=6" rel="stylesheet">
<link href="/bitrix/templates/.default/styles/jquery.modal.min.css" rel="stylesheet">

<?php
if (strpos($APPLICATION->GetCurPage(false), '/catalog/') === 0) { ?>
    <link href="/bitrix/templates/.default/styles/jquery.modal.min.css" rel="stylesheet">
<?php } ?>


<?php
if(isHomePage()) { ?>
    <link href="/local/front/template/styles/home.css?v=4" rel="stylesheet">

<?php }else{?>
    <?php if ($APPLICATION->GetCurPage() === '/o-magazine/kontakty/') { ?>
        <link href="https://unpkg.com/video.js/dist/video-js.min.css" rel="stylesheet">
    <?php } ?>
    <link href="/local/front/template/styles/inner.css?v=9" rel="stylesheet">
<?php }
$stylesFileNamesArr[] = 'products_list';
$stylesFileNamesArr[] = 'jquery.countdown';
$stylesFileNamesArr[] = 'add';


foreach($stylesFileNamesArr as $stylesFileName){ ?>
    <link href="/bitrix/templates/.default/styles/<?=$stylesFileName?>.css?1" rel="stylesheet">
<?php } ?>;
<link href="/local/front/template/styles/fotorama.css" rel="stylesheet">
<link rel="stylesheet" href="/local/front/template/styles/swiper-bundle.min.css"/>
