<ul class="block-wrap__item footer-main-navigation__list">
	<li class="footer-main-navigation__item footer-main-navigation__item_main"><a class="link-primary" href="/catalog/pariki/">Каталог товаров</a></li>
	<li class="footer-main-navigation__item"><a href="/catalog/pariki/zhenskiy/">Женские парики</a></li>
	<li class="footer-main-navigation__item"><a href="/catalog/pariki/muzhskoy/">Мужские парики</a></li>
	<li class="footer-main-navigation__item"><a href="/catalog/pariki/detskiy/">Детские парики</a></li>
	<li class="footer-main-navigation__item"><a href="/catalog/shinony/">Шиньоны</a></li>
	<li class="footer-main-navigation__item"><a href="/catalog/nakladki/">Накладки</a></li>
	<li class="footer-main-navigation__item"><a href="/catalog/golovnye-ubory/">Головные уборы</a></li>
</ul>
