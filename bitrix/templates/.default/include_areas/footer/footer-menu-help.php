<ul class="block-wrap__item footer-main-navigation__list">
	<li class="footer-main-navigation__item footer-main-navigation__item_main"><a class="link-primary" href="/stati/">Помощь в подборе парика</a></li>
	<li class="footer-main-navigation__item"><a href="/stati/polnyy-gid-po-vyboru-parika.html">Полный гид по выбору парика</a></li>
	<li class="footer-main-navigation__item"><a href="/stati/kak-vybrat-razmer.html">Как выбрать размер</a></li>
	<li class="footer-main-navigation__item"><a href="/stati/kak-vybrat-stil-parika.html">Как выбрать стиль парика</a></li>
	<li class="footer-main-navigation__item"><a href="/stati/ukhod-za-parikami.html">Уход за париками</a></li>
</ul>
