<div class="block-wrap s-hidden block-wrap_wrap ">
	<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width10 block-wrap__item_s-width6 footer-main-payment">
		<div class="footer-main-payment__item footer-main-payment__item_visa"></div>
		<div class="footer-main-payment__item footer-main-payment__item_master"></div>
		<div class="footer-main-payment__item footer-main-payment__item_yandex"></div>
		<div class="footer-main-payment__item footer-main-payment__item_webmoney"></div>
		<div class="footer-main-payment__item footer-main-payment__item_euroset"></div>
		<div class="footer-main-payment__item footer-main-payment__item_mts"></div>
		<div class="footer-main-payment__item footer-main-payment__item_megafon"></div>
	</div>
</div>