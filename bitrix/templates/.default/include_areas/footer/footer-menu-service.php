<ul class="block-wrap__item footer-main-navigation__list">
	<li class="footer-main-navigation__item footer-main-navigation__item_main"><a class="link-primary" href="/o-magazine/">О магазине париков</a></li>
	<li class="footer-main-navigation__item"><a href="/o-magazine/dostavka-i-oplata/">Доставка и оплата</a></li>
	<li class="footer-main-navigation__item"><a href="/o-magazine/novosti/">Новости и акции</a></li>
	<li class="footer-main-navigation__item"><a href="/o-magazine/vozvrat-i-obmen/">Возврат и обмен</a></li>
	<li class="footer-main-navigation__item"><a href="/o-magazine/kontakty/">Контакты</a></li>
	<li class="footer-main-navigation__item"><a href="/o-magazine/izgotovlenie-postizhernykh-izdeliy/">Изготовление постижерных изделий</a></li>
</ul>
