<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();

$scriptsFileNamesArr   = array();
$scriptsFileNamesArr[] = 'jquery.maskedinput';
$scriptsFileNamesArr[] = 'jquery.countdown';
$scriptsFileNamesArr[] = 'form_validation';
$scriptsFileNamesArr[] = 'ajax_form';
$scriptsFileNamesArr[] = 'add';
?>
<script src="/local/front/template/scripts/jquery-3.2.1.min.js"></script>
<script src="/local/front/template/scripts/fotorama.js"></script>
<script src="/local/front/template/scripts/jquery.modal.min.js"></script>
<script src="/local/front/template/scripts/swiper-bundle.min.js"></script>

<?php foreach($scriptsFileNamesArr as $scriptsFileName){ ?>
    <script src="/bitrix/templates/.default/scripts/<?=$scriptsFileName?>.js?v=62"></script>
    <?php } ?>

<?php if ($APPLICATION->GetCurPage() === '/o-magazine/kontakty/') { ?>
    <script src="https://unpkg.com/video.js/dist/video.min.js"></script>
    <script src="https://unpkg.com/videojs-vr/dist/videojs-vr.min.js"></script>
<?php } ?>

<script src="/local/front/template/scripts/script.js?v=4"></script>
