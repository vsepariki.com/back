<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require __DIR__."/ModelQuickBuy.php";

try{
    $modelQuickBuy = new ModelQuickBuy($_POST);

    $newElementId = $modelQuickBuy->createQuickOrder();

    $productStructure = $modelQuickBuy->getProductById();
    $productTextBlock = $modelQuickBuy->getProductTextBlock($productStructure);

    $modelQuickBuy->sendEmail($newElementId, $productTextBlock);

    $result = "success";
    $message = $modelQuickBuy->getMessage();
}catch (Exception $e){
    $result = "error";
    $message = $e->getMessage();
}finally{
    die(json_encode([
        "result" => $result,
        "message" => $message
    ]));
}

