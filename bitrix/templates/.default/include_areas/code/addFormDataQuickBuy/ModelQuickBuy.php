<?
class ModelQuickBuy
{
    const SITE_CODE = "s1";
    const EVENT_NAME = "NEW_QUICK_BUY_ORDER";
    const SUBJECT_INIT = "Новый заказ товара";
    const STATUS_COME = 17; // статус заказа "Поступил"
    const IBLOCK_TYPE = "ORDERS";

    private $request;
    private $messages = [];

    public function __construct($request)
    {
        if(empty($request)) {
            die("No data in request");
        }

        $this->request = $request;

        CModule::IncludeModule("iblock");
    }

    public function createQuickOrder()
    {
        $dateForCode = ConvertTimeStamp(time(), "FULL");
        $dateForUser = date('Y/m/d H:i:s');

        $subjectForCode = self::SUBJECT_INIT."_".$dateForCode;
        $subjectForUser = self::SUBJECT_INIT." от ".$dateForUser;

        if(empty($this->request["phone"])){
            throw new Exception("Phone is empty");
        }

        //code generation
        $arParamsCode = array(
            "max_len" => 255,
            "change_case" => "L",
            "replace_space" => '-',
            "replace_other" => '-',
            "delete_repeat_replace" => true
        );

        $element = new CIBlockElement;

        $arElementFields = [
            "IBLOCK_ID" => IBLOCK_QUICK_BUY_ID,
            "NAME" => $subjectForUser,
            "CODE" => CUtil::translit($subjectForCode, "ru", $arParamsCode),
            "ACTIVE" => "Y",
            "DATE_ACTIVE_FROM" => $dateForCode,
            "PROPERTY_VALUES" => [
                "PHONE" => $this->request["phone"],
                "PRODUCT" => $this->request["productId"],
                "STATUS" => self::STATUS_COME,
            ]
        ];

        if(!$newElementId = $element->Add($arElementFields)){
            throw new Exception($element->LAST_ERROR);
        }

        $this->messages[] = "New Quick Order created, Id = ".$newElementId;

        return $newElementId;
    }

    public function getProductById()
    {
        $arOrder = [];
        $arFilter = [
            "ID" => $this->request["productId"],
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        ];
        $arGroupBy = false;
        $arLimit  = [];
        $arSelect = [
            "ID",
            "IBLOCK_ID",
            "NAME",
            "CATALOG_GROUP_1"
        ];

        $res = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arLimit, $arSelect);

        if(!$ar_element = $res->GetNextElement()){
            throw new Exception("Не найден товар");
        }

		$fields = $ar_element->GetFields();
		$properties = $ar_element->GetProperties();

		$product["NAME"] = $fields["NAME"];

		if($fields["CATALOG_PRICE_1"]){
            $product["PRICE"] = $fields["CATALOG_PRICE_1"];
		}

        $product["COLOR"] = $this->getColorName($properties);

		return $product;
    }

    private function getColorName($properties)
    {
        $productType = false;
		if(!empty($properties["COLOR"]["VALUE"])){
            $productType = "COLOR";
		}elseif(!empty($properties["COLOR_HAT"]["VALUE"])){
            $productType = "COLOR_HAT";
		}elseif(!empty($properties["COLOR_COVERS"]["VALUE"])){
            $productType = "COLOR_COVERS";
		}elseif(!empty($properties["COLOR_SHINONY"]["VALUE"])){
            $productType = "COLOR_SHINONY";
		}

		if(!$productType){
		    return false;
		}

        $colorXmlId = $properties[$productType]["VALUE"];
        $tableName = $properties[$productType]["USER_TYPE_SETTINGS"]["TABLE_NAME"];

        global $DB;
        $query = "SELECT `UF_NAME` FROM ".$tableName." WHERE `UF_XML_ID` = '".$colorXmlId."'";
        $resource = $DB->Query($query);
        $ar_result = $resource->Fetch();

        if(!$ar_result){
            return false;
        }

        $colorName = $ar_result["UF_NAME"];

        return $colorName;
    }

    public function getProductTextBlock($productStructure)
    {
        $lines = [];

        $lines[] = "Название товара: <b>".$productStructure["NAME"]."</b>";

		if(!empty($this->request["sectionName"])){
			$lines[] = "Тип товара: <b>".$this->request["sectionName"]."</b>";
		}

		if($productStructure["COLOR"]){
			$lines[] = "Цвет: <b>".$productStructure["COLOR"]."</b>";
		}

		if(!empty($productStructure["PRICE"])){
			$price = CCurrencyLang::CurrencyFormat($productStructure["PRICE"], "RUB");
			$lines[] = "Цена: <b>".$price."</b>";
		}

		$text = implode("<br>", $lines);

		$this->messages[] = "Product Text: ".$text;

		return $text;
    }

    public function sendEmail($newElementId, $productTextBlock)
    {
        $backendLink = "http://".SITE_SERVER_NAME."/bitrix/admin/iblock_element_edit.php";
        $backendLink .= "?IBLOCK_ID=".IBLOCK_QUICK_BUY_ID."&type=".self::IBLOCK_TYPE."&ID=".$newElementId;

        $arEventFields = array(
            "PHONE" => $this->request["phone"],
            "PRODUCT" => $productTextBlock,
            "DATE" => date('Y/m/d H:i:s'),
            "ID" => $newElementId,
            "BE_LINK" => $backendLink
        );

        if($GLOBALS["prod"]){
            CEvent::SendImmediate(self::EVENT_NAME, self::SITE_CODE, $arEventFields);
            $this->messages[] = "E-mail send";
        }else{
            $this->messages[] = "Local development, no E-mail send";
        }

    }

    public function getMessage()
    {
        $message = "";

        if($this->messages){
            $message = implode(" - ", $this->messages);
        }

        return $message;
    }

}
