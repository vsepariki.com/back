<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!empty($_POST))
{
	$iblockId = 11; // Инфоблок "Консультация"

	//init data
	$question = htmlspecialcharsbx($_POST['question']);
	$fio      = htmlspecialcharsbx($_POST['fio']);
	$email    = htmlspecialcharsbx($_POST['email']);
	$phone    = htmlspecialcharsbx($_POST['phone']);
	$city     = htmlspecialcharsbx($_POST['city']);

	$questionToName = false;

	$dateForUser  = date('Y/m/d H:i:s');
	$dateForCode  = date('Y-m-d-H-i-s');

	CModule::IncludeModule('iblock');
	if($question){
		$topicInit    = 'Новый вопрос от ';
		$topicForUser = $topicInit.' '.$dateForUser;
		$topicForCode = $topicInit.'_'.$dateForCode;
	}else{
		die(json_encode(array('result' => 'error', 'message' => 'Question is empty')));
	}

	//code generation
	$arParamsCode = array(
		"max_len" => 255,
		"change_case" => "L",
		"replace_space" => '-',
		"replace_other" => '-',
		"delete_repeat_replace" => true
	);

	$code = CUtil::translit($topicForCode, "ru", $arParamsCode);

	$element = new CIBlockElement;

	$elementProps = array();
	$elementProps['FIO']   = $fio;
	$elementProps['PHONE'] = $phone;
	$elementProps['EMAIL'] = $email;
	$elementProps['CITY']  = $city;

	$arElementFields = Array(
		"IBLOCK_ID"        => $iblockId,
		"PROPERTY_VALUES"  => $elementProps,
		"NAME"             => $topicForUser,
		"PREVIEW_TEXT"     => $question,
		"CODE"			    => $code,
		"ACTIVE"           => "N",
		"DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL")
	);

	if ($NEW_INFOBLOCK_ITEM_ID = $element->Add($arElementFields))
	{
		$backend_link  = 'http://'.SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php';
		$backend_link .= '?IBLOCK_ID='.$iblockId.'&type=consultation&ID='.$NEW_INFOBLOCK_ITEM_ID;

		$arEventFields = array(
			"NAME"             => $fio,
			"DATE"             => $dateForUser,
			"ID"               => $NEW_INFOBLOCK_ITEM_ID,
			"PHONE"            => $phone,
			"EMAIL"            => $email,
			"CITY"             => $city,
			"COMMENTS"         => $question,
			"BE_LINK"          => $backend_link
		);

		$returnMsg = "New Iblock Item successfully added";
		if($GLOBALS['prod']) {
			$resSend = CEvent::Send("NEW_QUESTION", "s1", $arEventFields);
			$returnMsg .= "; Message send: " . $resSend;
		}

		$data = array("result" => "success", "message" => $returnMsg);

	}
	else{
		$data = array("result" => "error", "message" => $element->LAST_ERROR);
	}

	echo json_encode($data);

}
else
{
	LocalRedirect('/');
}

?>
