<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!empty($_POST))
{
    function generateRandomString($length = 10) {
        return bin2hex(random_bytes($length / 2));
    }

    $randomString = generateRandomString(16); // Генерация строки длиной 16 символов
    $iblockId = 13; // Инфоблок "Консультация"

    //init data
    $fio      = htmlspecialcharsbx($_POST['name']);
    $phone    = htmlspecialcharsbx($_POST['phone']);
    $product     = htmlspecialcharsbx($_POST['product']);
    $product_art     = htmlspecialcharsbx($_POST['product_art']);
    $topicForUser = $fio.' - '.$phone;
    $questionToName = false;
    $prop_add = '';

    $dateForUser  = date('Y/m/d H:i:s');
    $dateForCode  = date('Y-m-d-H-i-s');

    CModule::IncludeModule('iblock');


    //code generation
    $arParamsCode = array(
        "max_len" => 255,
        "change_case" => "L",
        "replace_space" => '-',
        "replace_other" => '-',
        "delete_repeat_replace" => true
    );


    $element = new CIBlockElement;

    $elementProps = array();
    $elementProps['FIO']   = $fio;
    $elementProps['PHONE'] = $phone;
    $elementProps['PRODUCT'] = $product;
    $elementProps['PRODUCT_ART']  = $product_art;

    $arElementFields = Array(
        "IBLOCK_ID"        => $iblockId,
        "PROPERTY_VALUES"  => $elementProps,
        "NAME"             => $topicForUser,
        "CODE"			    => $randomString,
        "ACTIVE"           => "N",
        "DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL")
    );
    $prop_common = CIBlockElement::GetByID($elementProps['PRODUCT']);

    if ($NEW_INFOBLOCK_ITEM_ID = $element->Add($arElementFields))
    {
        $prop_name = '';
        $backend_link  = 'http://'.SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php';
        $backend_link .= '?IBLOCK_ID='.$iblockId.'&type=preorder&ID='.$NEW_INFOBLOCK_ITEM_ID;
        if ($ob = $prop_common->GetNext()) {

            if ($elementProps['PRODUCT'])
            {
                $prop_add = CIBlockElement::GetByID($elementProps['PRODUCT']);
            }
            if ($prop = $prop_add->GetNext()) {
                $prop_name = $prop['NAME'];
            }



            $arEventFields = array(
                "NAME"             => $fio,
                "DATE"             => $dateForUser,
                "ID"               => $NEW_INFOBLOCK_ITEM_ID,
                "PHONE"            => $phone,
                "PRODUCT"             => $product,
                "PRODUCT_ART"             => $product_art,
                "HREF_TEXT"             => '<a href="http://'.SITE_SERVER_NAME.'/'.$ob['DETAIL_PAGE_URL'].'">'.$ob['NAME'].' '.$prop_name.'</a>',
                "BE_LINK"          => $backend_link
            );

            $returnMsg = "New Iblock Item successfully added";
            if($GLOBALS['prod']) {
                $resSend = CEvent::Send("NEW_SUBS", "s1", $arEventFields);
                $returnMsg .= "; Message send: " . $resSend;
            }

            $data = array("result" => "success", "message" => $returnMsg);
        }

    }
    else{
        $data = array("result" => "error", "message" => $element->LAST_ERROR);
    }

    echo json_encode($data);

}
else
{
    LocalRedirect('/');
}

?>
