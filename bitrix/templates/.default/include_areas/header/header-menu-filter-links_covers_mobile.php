<ul class="nav-submenu">
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Тип</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/nakladki/zhenskaya/">Женская</a></li>
  	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Категория длины</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/nakladki/dlinnaya/">Длинная</a></li>
      <li><a href="/catalog/nakladki/korotkaya/">Короткая</a></li>
      <li><a href="/catalog/nakladki/srednyaya/">Средняя</a></li>
  	</ul>
  </li>

  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Материал волос</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/nakladki/naturalnyy/">Натуральный</a></li>
      <li><a href="/catalog/nakladki/iskusstvennyy/">Искусственный</a></li>
      <li><a href="/catalog/nakladki/filter/material_nature_cover-is-smeshannyy/apply/">Смешанный</a></li>
  	</ul>
  </li>

  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Назначение</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/nakladki/na_makushku/">На макушку</a></li>
      <li><a href="/catalog/nakladki/na-probor/">На пробор</a></li>
      <li><a href="/catalog/nakladki/filter/purpose_cover-is-na-temennuyu-zonu/apply/">На теменную зону</a></li>
      <li><a href="/catalog/nakladki/poluparik/">Полупарик</a></li>
      <li><a href="/catalog/nakladki/s-chelkoy/">На переднюю часть</a></li>
  	</ul>
  </li>

  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Особенности</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/nakladki/dlya-obema/">Для объема</a></li>
      <li><a href="/catalog/nakladki/monofilament-ruchnaya-rabota-na-setke/">Имитирующая собственные волосы</a></li>
      <li><a href="/catalog/nakladki/monofilament-ruchnaya-rabota-na-setke/naturalnyy/">Моно</a></li>
  	</ul>
  </li>

  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Способ крепления</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/nakladki/na-zakolkah/">На заколках</a></li>
      <li><a href="/catalog/nakladki/filter/attach_cover-is-na-fiksiruyushchey-lente/apply/">На фиксирующей ленте</a></li>
  	</ul>
  </li>

</ul>