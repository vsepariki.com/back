<ul class="nav-submenu nav-submenu_6cols">
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Для кого</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/golovnye-ubory/zhenskiy/">Женский</a></li>
      <li><a href="/catalog/golovnye-ubory/detskiy/">Детский</a></li>
  	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Вид</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/golovnye-ubory/bandana/">Бандана</a></li>
      <li><a href="/catalog/golovnye-ubory/beret/">Берет</a></li>
      <li><a href="/catalog/golovnye-ubory/kepka/">Кепка</a></li>
      <li><a href="/catalog/golovnye-ubory/kosynka/">Косынка</a></li>
      <li><a href="/catalog/golovnye-ubory/povyazka-obodok/">Ободок</a></li>
      <li><a href="/catalog/golovnye-ubory/tyurban-chalma/">Тюрбан</a></li>
      <li><a href="/catalog/golovnye-ubory/shapochka/">Шапочка</a></li>
      <li><a href="/catalog/golovnye-ubory/shlyapka/">Шляпка</a></li>
      <li><a href="/catalog/golovnye-ubory/sharf/">Шарф</a></li>
  	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Применение</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/golovnye-ubory/posle-himioterapii/">После химиотерапии</a></li>
      <li><a href="/catalog/golovnye-ubory/dlya-doma/">Для дома</a></li>
      <li><a href="/catalog/golovnye-ubory/dlya-kundalini/">Для кундалини</a></li>
      <li><a href="/catalog/golovnye-ubory/dlya-plyazha/">Для пляжа</a></li>
      <li><a href="/catalog/golovnye-ubory/dlya-sna/">Для сна</a></li>
      <li><a href="/catalog/golovnye-ubory/dlya-sozdaniya-stilya/">Для создания стиля</a></li>
      <li><a href="/catalog/golovnye-ubory/dlya-sporta/">Для спорта</a></li>
      <li><a href="/catalog/golovnye-ubory/povsednevnye/">Повседневные</a></li>
      <li><a href="/catalog/golovnye-ubory/musulmanskaya/">Мусульманская</a></li>
  	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Сезон</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/golovnye-ubory/vesenniy/">Весна</a></li>
      <li><a href="/catalog/golovnye-ubory/letniy/">Лето</a></li>
      <li><a href="/catalog/golovnye-ubory/osenniy/">Осень</a></li>
  	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Материал</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/golovnye-ubory/bambuk/">Бамбук</a></li>
      <li><a href="/catalog/golovnye-ubory/velyur/">Велюр</a></li>
      <li><a href="/catalog/golovnye-ubory/viskoza/">Вискоза</a></li>
      <li><a href="/catalog/golovnye-ubory/poliester/">Полиэстер</a></li>
      <li><a href="/catalog/golovnye-ubory/trikotazh/">Трикотаж</a></li>
      <li><a href="/catalog/golovnye-ubory/hlopok/">Хлопок</a></li>
		<? /* <li><a href="/catalog/golovnye-ubory/shelk/">Шелк</a></li> */ ?>
      <li><a href="/catalog/golovnye-ubory/elastan/">Эластан</a></li>
  	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Аксессуары</div>
  	<ul class="nav-submenu__sublist">
      <li><a href="/catalog/golovnye-ubory/s-bantom/">С бантом</a></li>
      <li><a href="/catalog/golovnye-ubory/s-kozyrkom/">С козырьком</a></li>
      <li><a href="/catalog/golovnye-ubory/s-cvetkom/">С цветком</a></li>
      <li><a href="/catalog/golovnye-ubory/s-sharfom-platkom/">С шарфом/платком</a></li>
  	</ul>
  </li>
</ul>