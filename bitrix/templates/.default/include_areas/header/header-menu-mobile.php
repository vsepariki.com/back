<div class="block-wrap xl-hidden l-hidden m-hidden header-mobile block-wrap_nopadding ">

	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width2 header-mobile__menu mobile-menu">
		<input class="mobile-menu__check" id="mobile-menu__check" type="checkbox">
		<label class="mobile-menu__label" for="mobile-menu__check">Меню</label>
		<div class="mobile-menu__body">

			<?
			$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu-products-mobile", Array(
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_TYPE" => "A",	// Тип кеширования
				"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
				"IBLOCK_ID" => "9",	// Инфоблок
				"IBLOCK_TYPE" => "CATALOG",	// Тип инфоблока
				"SECTION_CODE" => "",	// Код раздела
				"SECTION_FIELDS" => array(),	// Поля разделов
				"SECTION_ID" => "",	// ID раздела
				"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
				"SECTION_USER_FIELDS" => array(),	// Свойства разделов
				"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
				"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
				"VIEW_MODE" => "LINE",	// Вид списка подразделов
				), false
			);
			?>

			<nav class="navigation-mobile">
				<ul class="navigation-mobile__list">
					<li class="navigation-mobile__item"><a href="/o-magazine/">О магазине</a></li>
					<li class="navigation-mobile__item"><a href="/o-magazine/dostavka-i-oplata/">Доставка и оплата</a></li>
					<li class="navigation-mobile__item"><a href="/o-magazine/vozvrat-i-obmen/">Возврат и обмен</a></li>
					<li class="navigation-mobile__item"><a href="/o-magazine/novosti/">Новости и акции</a></li>
					<li class="navigation-mobile__item"><a href="/stati/">Все о париках</a></li>
					<li class="navigation-mobile__item"><a href="/konsultatsiya/">Консультация</a></li>
					<li class="navigation-mobile__item"><a href="/o-magazine/kontakty/">Контакты</a></li>
				</ul>
			</nav>

			<div class="navigation-mobile navigation-mobile-other">
				<div class="navigation-mobile-other__title">Другие направления</div>
				<nav class="navigation-other-activities">
					<ul class="navigation-other-activities__list">
						<li class="navigation-other-activities__item"><a href="http://www.hairdesign.ru/"><span>Центр<br>замещения волос</span></a></li>
						<li class="navigation-other-activities__item"><a href="http://salonvolos.spb.ru/"><span>Салон красоты</span></a></li>
						<li class="navigation-other-activities__item navigation-other-activities__item_active">
							<div class="a-repl"><span>Парики и<br>головные уборы</span></div>
						</li>
						<li class="navigation-other-activities__item"><a href="http://triholog.org/"><span>Лечебная косметика<br>для волос</span></a></li>
						<li class="navigation-other-activities__item"><a href="http://zagustitelvolos.com/"><span>Загустители волос<br>Toppik</span></a></li>
					</ul>
				</nav>
			</div>

		</div> <!-- mobile-menu__body -->
	</div> <!-- block-wrap mobile-menu -->

	<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width3 header-mobile__phone">+7 (812) 413-99-06</div>
            <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width5 header-mobile__search">
              		<?
					$APPLICATION->IncludeComponent("bitrix:search.form", "search", Array(
						"PAGE" => "/search/",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
						"USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
						), false
					);
					?>
            </div>
            <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width1 header-mobile__search-switch">
            </div>

</div>
