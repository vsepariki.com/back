<div class="block-wrap s-hidden block-wrap_nopadding">
	<?
	$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu-products-desktop", Array(
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "N",	// Тип кеширования
		"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
		"IBLOCK_ID" => "9",	// Инфоблок
		"IBLOCK_TYPE" => "CATALOG",	// Тип инфоблока
		"SECTION_CODE" => "",	// Код раздела
		"SECTION_FIELDS" => array(),	// Поля разделов
		"SECTION_ID" => "",	// ID раздела
		"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(),	// Свойства разделов
		"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
		"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
		"VIEW_MODE" => "LINE",	// Вид списка подразделов
		), false
	);
	?>
<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width6 m-hidden s-hidden">

		<?
		$APPLICATION->IncludeComponent("bitrix:search.form", "search", Array(
			"PAGE" => "/search/",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
			"USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
			), false
		);
		?>
</div>
</div>