<ul class="nav-submenu">
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Для кого</div>
	<ul class="nav-submenu__sublist">
	  <li class="nav-submenu__item"><a href="/catalog/pariki/zhenskiy/">Женский</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/muzhskoy/">Мужской</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/detskiy/">Детский</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/raquel-welch-urban-style-2013/korotkiy/">Для пожилых</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Материал волос</div>
	<ul class="nav-submenu__sublist">
	  <li class="nav-submenu__item"><a href="/catalog/pariki/naturalnyy/">Натуральный</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/iskusstvennyy/">Искусственный</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/smeshannyy/">Смешанный</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/easy-brush/">Термоволокно</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Длина волос</div>
	<ul class="nav-submenu__sublist">
	  <li class="nav-submenu__item"><a href="/catalog/pariki/dlinnyy/">Длинные</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/korotkiy/">Короткие</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/sredniy/">Средние</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Структура волос</div>
	<ul class="nav-submenu__sublist">
	  <li class="nav-submenu__item"><a href="/catalog/pariki/pryamoy/">Прямые</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/volnistyy/">Волнистые</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/kudryavyy/">Кудрявые</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Конструкция шапки</div>
	<ul class="nav-submenu__sublist">
	  <li class="nav-submenu__item"><a href="/catalog/pariki/monofilament-ruchnaya-rabota-na-setke/">На сетке</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/tressy-bez-setki/">Без сетки (трессы)</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/100-ruchnaya-rabota/">Ручная работа</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Стрижка</div>
	<ul class="nav-submenu__sublist">
	  <li class="nav-submenu__item"><a href="/catalog/pariki/s-proborom/">С пробором</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/kare/">Каре</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/bob/">Боб</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/kaskad/">Каскад</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/s-chelkoy/">С челкой</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/bez-chelki/">Без челки</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Стиль</div>
	<ul class="nav-submenu__sublist">
	  <li class="nav-submenu__item"><a href="/catalog/pariki/filter/style-is-vecherniy/apply/">Вечерний</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/filter/style-is-delovoy/apply/">Деловой</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/filter/style-is-klassicheskiy/apply/">Классический</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/modnyy/">Модный</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/filter/style-is-na-svadbu/apply/">На свадьбу</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/povsednevnyy/">Повседневный</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/elegantnyy/">Элегантный</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Особенности</div>
	<ul class="nav-submenu__sublist">
	  <li class="nav-submenu__item"><a href="/catalog/pariki/nezametnyy/">С имитацией кожи головы</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/oblegchennyy/">Облегченный</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/frontalnaya-liniya/">Lace front</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/na-rezinke/">На резинке</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/na-zakolkah/">На заколках</a></li>
		<? /* <li class="nav-submenu__item"><a href="/catalog/pariki/umnyy/">Умный</a></li> */ ?>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/european-hair/">Славянские</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/concept-b/">Lace wigs</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Цвет</div>
	<ul class="nav-submenu__sublist">
	  <li class="nav-submenu__item"><a href="/catalog/pariki/blond/">Блонд</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/svetlyy/">Светлый</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/bryunet/">Темный</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/shaten/">Шатен</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/ryzhiy/">Рыжий</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/sedoy/">Седой</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/effekt-melirovaniya/">Эффект мелирования</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/pariki/otrosshie-korni/">Омбре</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Бренд</div>
	<ul class="nav-submenu__sublist">
	  <li class="nav-submenu__item"><a href="/catalog/pariki/ellen-wille/">Ellen Wille</a></li>
	  <? /* <li class="nav-submenu__item"><a href="/catalog/pariki/nj-creation/">NJ Creation</a></li> */ ?>
	</ul>
  </li>
</ul>
