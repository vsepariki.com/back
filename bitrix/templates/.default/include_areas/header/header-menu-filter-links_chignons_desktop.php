<ul class="nav-submenu">
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Стиль</div>
	<ul class="nav-submenu__sublist">
    <li class="nav-submenu__item"><a href="/catalog/shinony/vecherniy/">Вечерний</a></li>
    <li class="nav-submenu__item"><a href="/catalog/shinony/delovoy/">Деловой</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/shinony/povsednevnyy/">Повседневный</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Вид</div>
	<ul class="nav-submenu__sublist">
    <li class="nav-submenu__item"><a href="/catalog/shinony/babetta/">Бабетта</a></li>
    <li class="nav-submenu__item"><a href="/catalog/shinony/kosa/">Коса</a></li>
    <li class="nav-submenu__item"><a href="/catalog/shinony/nakladka-dlya-dliny/">Накладка для длины</a></li>
    <li class="nav-submenu__item"><a href="/catalog/shinony/parik/">Парик</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/shinony/khvost/">Хвост</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Категория длины</div>
	<ul class="nav-submenu__sublist">
    <li class="nav-submenu__item"><a href="/catalog/shinony/dlinniy/">Длинный</a></li>
    <li class="nav-submenu__item"><a href="/catalog/shinony/korotkiy/">Короткий</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/shinony/sredniy/">Средний</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Крепление</div>
	<ul class="nav-submenu__sublist">
    <li class="nav-submenu__item"><a href="/catalog/shinony/na-zalolkakh/">На заколках</a></li>
    <li class="nav-submenu__item"><a href="/catalog/shinony/na-klipsakh/">На клипсах</a></li>
    <li class="nav-submenu__item"><a href="/catalog/shinony/na-krabe/">На крабе</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/shinony/na-rezinke/">На резинке</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Структура</div>
	<ul class="nav-submenu__sublist">
    <li class="nav-submenu__item"><a href="/catalog/shinony/volnistye/">Волнистые</a></li>
    <li class="nav-submenu__item"><a href="/catalog/shinony/kudryavye/">Кудрявые</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/shinony/pryamye/">Прямые</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Место</div>
	<ul class="nav-submenu__sublist">
    <li class="nav-submenu__item"><a href="/catalog/shinony/na-zatylok/">На затылок</a></li>
	  <li class="nav-submenu__item"><a href="/catalog/shinony/na-makushku/">На макушку</a></li>
	</ul>
  </li>
  <li class="nav-submenu__category">
	<div class="nav-submenu__subtitle">Материал</div>
	<ul class="nav-submenu__sublist">
	  <li class="nav-submenu__item"><a href="/catalog/shinony/iskusstvennye/">Искусственные</a></li>
	</ul>
  </li>
</ul>
