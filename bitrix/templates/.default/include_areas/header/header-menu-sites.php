<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?><div class="top-menu s-hidden">
	<div class="container">
		<div class="content">
			<nav class="navigation-other-activities">
				<ul class="navigation-other-activities__list">
					<li class="navigation-other-activities__item">
						<a href="http://www.hairdesign.ru/"><span>Замещение волос</span></a>
					</li>
					<li class="navigation-other-activities__item">
						<a href="http://salonvolos.spb.ru/"><span>Салон красоты</span></a>
					</li>
					<li class="navigation-other-activities__item navigation-other-activities__item_active">
						<div class="a-repl"><span>Парики, накладки и<br>головные уборы</span></div>
					</li>
					<li class="navigation-other-activities__item">
						<a href="http://triholog.org/"><span>Лечебная косметика<br>для волос</span></a>
					</li>
					<li class="navigation-other-activities__item">
						<a href="http://zagustitelvolos.com/"><span>Загустители волос</span></a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</div>
