<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();

$curPage = $APPLICATION->GetCurPage();
$curPage = str_replace('/', '', $curPage);

$servicesArr = array(
	"o-magazine" => "О магазине",
	"o-magazine/dostavka-i-oplata" => "Доставка и оплата",
	"o-magazine/vozvrat-i-obmen/" => "Возврат и обмен",
	"o-magazine/novosti" => "Новости и акции",
	"stati" => "Все о париках",
	"konsultatsiya" => "Консультация",
	"o-magazine/kontakty" => "Контакты",
);
?>
<nav class="navigation-main">
	<ul class="navigation-main__list">
		<? foreach ($servicesArr as $serviceUrl => $servicesName): ?>
			<? if($serviceUrl == $curPage){$ifActive = ' navigation-main__item_active';}else{$ifActive = '';} ?>
			<li class="navigation-main__item<?= $ifActive ?>">
				<a href="/<?= $serviceUrl ?>/"><?= $servicesName ?></a>
			</li>
		<? endforeach; ?>
	</ul>
</nav>
