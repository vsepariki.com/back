<?
    $orderId = $arResult["ORDER_ID"];
    $products = getProductsInOrderByOrderId($orderId);
    $products = array_map("json_encode", $products);
    $products = implode(", ", $products);
?>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        dataLayer.push({
            "ecommerce": {
                "purchase": {
                    "actionField": {"id": "<?= $orderId ?>"},
                    "products": [<?= $products ?>]
                }
            }
        });
    });
</script>