<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);

$rows = $arResult['JS_DATA']["GRID"]["ROWS"];
foreach($rows as $k => $item) {
	$productId      = $item["data"]["PRODUCT_ID"];
	$productInfoRes = CCatalogProduct::GetList(array(), array("ID" => $productId), false, array());
	$productInfo    = $productInfoRes->getNext();
	$quantity       = $productInfo["QUANTITY"];

	if($quantity > 0){
		$deliveryText = '<div class="product-availability_yes">В наличии</div>';
	}else{
        $deliveryText = getDeliveryTextByProductId($productId);
		$deliveryText = '<div class="product-availability_no">'.$deliveryText.'</div>';
	}

	$arResult['JS_DATA']["GRID"]["ROWS"][$k]["data"]["DELIVERY_DAYS"] = $deliveryText;
}



