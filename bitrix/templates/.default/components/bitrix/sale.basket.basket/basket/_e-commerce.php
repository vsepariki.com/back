<script>
    document.addEventListener("DOMContentLoaded", function() {
        var deleteLink = $('form#basket_form a:contains("Удалить")');
        if(deleteLink.length){
         deleteLink.on("click", function(e){
            e.preventDefault();

            var basketItem = $(e.target).closest("tr");

            var basketItemId = basketItem.attr("id").toString();

            var basketItemName = $("h2", basketItem).text().trim();

            var basketItemQuantity = $("input#QUANTITY_INPUT_" + basketItemId).val();
            basketItemQuantity = parseInt(basketItemQuantity);

            var basketItemPrice = $("div#current_price_" + basketItemId).text().trim();
            basketItemPrice = basketItemPrice.replace(/ /, '');
            basketItemPrice = basketItemPrice.replace(/руб./, '').trim();
            basketItemPrice = parseInt(basketItemPrice);

             dataLayer.push({
                 "ecommerce": {
                     "remove": {
                         "products": [
                             {
                                 "id": basketItemId,
                                 "name": basketItemName,
                                 "price": basketItemPrice,
                                 "quantity": basketItemQuantity
                             }
                         ]
                     }
                 }
             });

             document.location.href = $(e.target).attr("href");

         });
        }
    });
</script>