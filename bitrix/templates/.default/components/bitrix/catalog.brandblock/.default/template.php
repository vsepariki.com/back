<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
if (empty($arResult["BRAND_BLOCKS"]))  return;
?>
<div class="item_info_section">
	<div class="bx_item_detail_scu">
		<span class="bx_item_section_name_gray">Цвет</span>
		<div class="bx_scu_scroller_container">
			<div class="bx_scu">
				<ul>
				<?
				foreach ($arResult["BRAND_BLOCKS"] as $arBB){
					?>
					<li class="bx_item_vidget_block"><span class="bx_item_vidget"  style="background-image: url(<?=$arBB['PICT']['SRC'];?>)"></span></li>
					<?
				}
				?>
				</ul>
			</div>
		</div>
	</div>
</div>
