<?foreach ($arItem["VALUES"] as $val => $ar): ?>
	<div class="checkbox">
		<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
			<span class="bx-filter-input-checkbox">
				<input
					type="checkbox"
					value="<? echo $ar["HTML_VALUE"] ?>"
					name="<? echo $ar["CONTROL_NAME"] ?>"
					id="<? echo $ar["CONTROL_ID"] ?>"
					<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
					onclick="smartFilter.click(this)"
				/>
				<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
				if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
					?>&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
				endif;?></span>
			</span>
		</label>
	</div>
<?endforeach?>

<select class="selectbox mobile-filter-item__select" id="property-<?= $key ?>" name="property-<?= $key ?>" multiple>
<?foreach ($arItem["VALUES"] as $val => $ar): ?>
	<? if($ar["CHECKED"]): ?>
		<option value="<?= $ar["CONTROL_ID"] ?>" id="option-<?= $ar["CONTROL_ID"] ?>" selected><?= $ar["VALUE"]; ?></option>
	<? else: ?>
		<option value="<?= $ar["CONTROL_ID"] ?>" id="option-<?= $ar["CONTROL_ID"] ?>"><?= $ar["VALUE"]; ?></option>
	<? endif; ?>
<?endforeach?>
</select>