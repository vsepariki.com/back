<div class="col-lg-12 bx-filter-parameters-box bx-active bx-filter-parameters-box--availability">

	<span class="bx-filter-container-modef"></span>

	<div class="bx-filter-parameters-box-title-wrapper">
		<div class="bx-filter-parameters-box-title" onclick="smartFilter.hideFilterProps(this)">
            <span class="bx-filter-parameters-box-hint">
            Наличие <i data-role="prop_angle" class="fa fa-angle-up"></i>
            </span>
		</div>
	</div>

	<div class="bx-filter-block" data-role="bx_filter_block">
		<div class="bx-filter-parameters-box-container">
			<div class="checkbox">
				<label class="bx-filter-param-label">
                    <span class="bx-filter-input-checkbox">
                        <input
							type="checkbox"
							value="Y"
							name="availability"
							<? if(isCurUrlConsist('availability')): ?>checked="checked"<? endif ?>
                            onclick="smartFilter.click(this)"
						>
                        <span class="bx-filter-param-text" title="В наличии">В наличии</span>
                    </span>
				</label>
			</div>
		</div>
		<div style="clear: both"></div>
	</div>

</div>
