<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$defaultProductImgSrc = '/upload/medialibrary/baa/baaa03452391641f164ed2e95b71384f.jpg';

if (!empty($arResult['ITEMS'])) {
?>
<div>

	<h2>Вам может быть интересно</h2>

	<div class="block-wrap product-similar block-wrap_wrap ">
		<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
			<?
			$titleForSite = "";
			if(empty($arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])){
				$productName = $arItem["NAME"];
			}else{
				$titleForSite = $arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
				$productName = $arItem["NAME"]." | ".$titleForSite;
			}
			?>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width3 catalog-list__item">
				<div class="catalog-product">
				<?
					if(!empty($arItem["OFFERS"])){
						 include __DIR__.'/_list-product-complex.php';
					}
					else{
						 include __DIR__.'/_list-product-simple.php';
					}
				?>
				</div>
			</div>
		<? endforeach; ?>
	</div>
</div>
<?
}
?>
