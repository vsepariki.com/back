<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$offersColors = array();

$propCode = false;
if($arItem["IBLOCK_SECTION_ID"] == WIGS_SECTION_ID){
	$propCode = "COLOR";
}
if($arItem["IBLOCK_SECTION_ID"] == HATS_SECTION_ID){
	$propCode = "COLOR_HAT";
}
if($arItem["IBLOCK_SECTION_ID"] == COVERS_SECTION_ID){
	$propCode = "COLOR_COVERS";
}
if($arItem["IBLOCK_SECTION_ID"] == SHINONY_SECTION_ID){
    $propCode = "COLOR_SHINONY";
}

$fullSeoName = $arItem["NAME"];
if($titleForSite){
    $fullSeoName .= " ".$titleForSite;
}

$pictureType = "default";
if(!empty($arItem['PREVIEW_PICTURE']['SRC'])){
    $pictureType = 'PREVIEW_PICTURE';
}elseif(!empty($arItem['DETAIL_PICTURE']['SRC'])){
    $pictureType = 'DETAIL_PICTURE';
}
?>
<div class="catalog-product__offer">

    <a class="catalog-product__image" href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
        <? if($pictureType == "default"): ?>
            <img
                src="<?= $defaultProductImgSrc ?>"
                alt=""
            >
        <? else: ?>
            <img
                src="<?= $arItem[$pictureType]['SRC'] ?>"
                alt="<?= $fullSeoName ?>"
                title="<?= $fullSeoName ?>"
            >
        <? endif ?>
    </a>

    <div class="catalog-product__name">
        <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>"><?= $productName ?></a>
    </div>

    <div class="catalog-product__prices">
        <div class="catalog-product__price">
            <?= number_format($arItem["CATALOG_PRICE_1"], 0, ',', ' ') ?>  руб.
        </div>
    </div>

</div> <!-- catalog-product__offer -->
<?
foreach($arItem["OFFERS"] as $k => $offer) {
	$offerColorCode = $offer["PROPERTIES"][$propCode]["VALUE"];
	$offerColorMap = $arResult['SKU_PROPS'][IBLOCK_PRODUCTS_ID][$propCode]["XML_MAP"][$offerColorCode];
	$offerColorArr = $arResult['SKU_PROPS'][IBLOCK_PRODUCTS_ID][$propCode]["VALUES"][$offerColorMap];
	$offerColorTitle = $offerColorArr["NAME"];
	$offerColorSrc = $offerColorArr["PICT"]["SRC"];
	$offersColors[$k] = array('title' => $offerColorTitle, 'src' => $offerColorSrc);
}
?>
<div class="catalog-product__colors">
	<div class="catalog-product-colors">
		<div class="catalog-product-colors__title">Цвет</div>
		<ul class="catalog-product-colors__list">
			<? foreach($offersColors as $k => $offersColor): ?>
				<li class="catalog-product-colors__item">
					<img
					    src="#"
					    data-src="<?= $offersColor['src'] ?>"
					    alt=""
					    title="<?= $offersColor['title'] ?>"
                    >
				</li>
			<? endforeach ?>
		</ul>
	</div>
</div>
