<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$fullSeoName = $arItem["NAME"];
if($titleForSite){
    $fullSeoName .= " ".$titleForSite;
}

?>
<a
    class="catalog-product__image"
    href="<?= $arItem['DETAIL_PAGE_URL']; ?>"
>
	<? if(!empty($arItem['PREVIEW_PICTURE']['SRC'])): ?>
		<img
		    src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>"
            alt="<?= $fullSeoName ?>"
            title="<?= $fullSeoName ?>"
        >
	<? elseif(!empty($arItem['DETAIL_PICTURE']['SRC'])): ?>
		<img
		    src="<?= $arItem['DETAIL_PICTURE']['SRC']; ?>"
            alt="<?= $fullSeoName ?>"
            title="<?= $fullSeoName ?>"
        >
	<? else: ?>
		<img src="<?= $defaultProductImgSrc ?>" alt="">
	<? endif; ?>
</a>

<div class="catalog-product__name">
	<a href="<?= $arItem['DETAIL_PAGE_URL']; ?>"><?= $productName ?></a>
</div>

<div class="catalog-product__prices">
	<div class="catalog-product__price">
		<?= $arItem["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE"] ?>
	</div>
	<? if($arItem["PRICES"]["BASE"]["DISCOUNT_DIFF"]): ?>
		<div class="catalog-product__old-price">
			<?= $arItem["PRICES"]["BASE"]["PRINT_VALUE"] ?>
		</div>
	<? endif; ?>
</div>