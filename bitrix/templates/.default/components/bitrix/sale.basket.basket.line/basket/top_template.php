<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?><div class="bx-hdr-profile">
<?if (!$compositeStub && $arParams['SHOW_AUTHOR'] == 'Y'):?>
	<div class="bx-basket-block header-basket__register">
		<?if ($USER->IsAuthorized()):
			$name = trim($USER->GetFullName());
			if (! $name)
				$name = trim($USER->GetLogin());
			if (mb_strlen($name, 'utf-8') > 15)
				$name = mb_substr($name, 0, 12, 'utf-8').'...';
			?>
			<a href="<?=$arParams['PATH_TO_PROFILE']?>"><?=htmlspecialcharsbx($name)?></a>
			/
			<a href="?logout=yes"><?=GetMessage('TSB1_LOGOUT')?></a>
		<?else:?>
			<a href="<?=$arParams['PATH_TO_REGISTER']?>?login=yes"><?=GetMessage('TSB1_LOGIN')?></a> / <a href="<?=$arParams['PATH_TO_REGISTER']?>?register=yes"><?=GetMessage('TSB1_REGISTER')?></a>
		<?endif?>
	</div>
<?endif?>


<? if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y')): ?>
	<a class="bx-basket-block header-basket__summary" href="<?= $arParams['PATH_TO_BASKET'] ?>">
			<?
			if (!$compositeStub)
			{
				if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'))
				{ ?>
					<div class="header-basket__items"><?= $arResult['NUM_PRODUCTS'].' '.$arResult['PRODUCT(S)'] ?></div><?
				}
				if ($arParams['SHOW_TOTAL_PRICE'] == 'Y'):?>
					<? if ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'):?>
						<div class="header-basket__price"><?= $arResult['TOTAL_PRICE'] ?></div>
					<?endif ?>
				<?endif;?>
				<?
			}

			if ($arParams['SHOW_PERSONAL_LINK'] == 'Y'):?>
				<div style="padding-top: 4px;">
				<span class="icon_info"></span>
				<a href="<?=$arParams['PATH_TO_PERSONAL']?>"><?=GetMessage('TSB1_PERSONAL')?></a>
				</div>
			<?endif?>

	</a>
<? endif; ?>
</div>