<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$defaultProductImgSrc = '/upload/medialibrary/baa/baaa03452391641f164ed2e95b71384f.jpg';
?>
<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6 home-other-products__item">
    <div class="block-wrap home-other-products-list block-wrap_wrap ">
        <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <?
                $titleForSite = "";
                if(empty($arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])){
                    $productName = $arItem["NAME"];
                }else{
                    $titleForSite = $arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
                    $productName = $arItem["NAME"]." | ".$titleForSite;
                }
            ?>
            <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width6 block-wrap__item_s-width3 home-other-products-list__item">
                <div class="catalog-product">
                <?
                    if(!empty($arItem["OFFERS"])){
                         include __DIR__.'/_list-product-complex.php';
                    }
                    else{
                         include __DIR__.'/_list-product-simple.php';
                    }
                ?>
                </div>
            </div>
        <? endforeach ?>
    </div>
</div>