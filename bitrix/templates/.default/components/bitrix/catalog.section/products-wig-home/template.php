<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$defaultProductImgSrc = '/upload/medialibrary/baa/baaa03452391641f164ed2e95b71384f.jpg';
?>
<div class="home-catalog__tabs-container">

	<div class="home-catalog__mobile-switch" id="home-catalog__mobile-switch"></div>

	<div class="home-catalog__tabs">
	<?
		$counter = 0;
		foreach($arResult["TABS_ITEMS"] as $key => $tabItem):
		$counter++;
		$ifActive = ($counter == 1) ? ' home-catalog__tab_active' : '';
		?>
		<div class="home-catalog__tab<?= $ifActive; ?>" data-home-catalog-tab="home-catalog-tab<?= $counter; ?>"><?= $key; ?></div>
		<?
	endforeach;
	?>
	</div>

	<div class="home-catalog__tabs-content">
	<?
	if (!empty($arResult['ITEMS']))
	{
		$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
		$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
		$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

		$counter = 0;
		foreach($arResult["TABS_ITEMS"] as $tabItem)
		{
			$counter++;
			$ifActive = ($counter == 1) ? ' home-catalog__tab-content_active' : '';
			?>
			<div class="home-catalog__tab-content<?= $ifActive; ?>" data-home-catalog-tab="home-catalog-tab<?= $counter; ?>">
				<div class="block-wrap home-catalog-list block-wrap_wrap ">
				<?
				foreach ($tabItem as $key => $arItem) {
				    $titleForSite = "";
					if(empty($arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])){
						$productName = $arItem["NAME"];
					}else{
						$titleForSite = $arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
						$productName = $arItem["NAME"]." | ".$titleForSite;
					}
					?>
					<div
					    class="
                            block-wrap__item
                            block-wrap__item_xl-width3
                            block-wrap__item_l-width3
                            block-wrap__item_m-width6
                            block-wrap__item_s-width3
                            catalog-list__item
					    "
                    >
						<div class="catalog-product"><?
							if(!empty($arItem["OFFERS"])){
								 include __DIR__.'/_list-product-complex.php';
					 		}
							else{
								 include __DIR__.'/_list-product-simple.php';
					 		} ?>
						</div>
					</div>
					<?
				}
				?>
				</div>
			</div> <!-- home-catalog__tab-content -->
			<?
			} // foreach($arResult["TABS_ITEMS"] as $tabItem)
	} //if (!empty($arResult['ITEMS']))
	?>
	</div> <!-- home-catalog__tabs-content -->

</div>
