<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$defaultProductImgSrc = '/upload/medialibrary/baa/baaa03452391641f164ed2e95b71384f.jpg';

$similarClass = "";
$similarTitle = "";

if($arParams["SECTION_ID"] == WIGS_SECTION_ID){
	$similarClass = "product-similar";
	$similarTitle = "Похожие парики";
}

if($arParams["SECTION_ID"] == HATS_SECTION_ID){
	$similarClass = "hats";
	$similarTitle = "Похожие головные уборы";
}

if($arParams["SECTION_ID"] == COVERS_SECTION_ID){
	$similarClass = "covers";
	$similarTitle = "Похожие накладки";
}

if($arParams["SECTION_ID"] == SHINONY_SECTION_ID){
	$similarClass = "shinony";
	$similarTitle = "Похожие шиньоны";
}

if (!empty($arResult['ITEMS'])) {
?>

    <?php if  (preg_match('/pariki\/.+/i',$_SERVER['REQUEST_URI'])){

         $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "main_products_section",
            array(
                "ACTION_VARIABLE" => "action",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "ADD_TO_BASKET_ACTION" => "ADD",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "BACKGROUND_IMAGE" => "-",
                "BASKET_URL" => "/personal/basket.php",
                "BROWSER_TITLE" => "-",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "N",
                "COMPATIBLE_MODE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "DETAIL_URL" => "/catalog/#SECTION_CODE#/#ELEMENT_CODE#.html",
                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_COMPARE" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "ELEMENT_SORT_FIELD" => "RAND",
                "ELEMENT_SORT_FIELD2" => "id",
                "ELEMENT_SORT_ORDER" => "asc",
                "ELEMENT_SORT_ORDER2" => "desc",
                "ENLARGE_PRODUCT" => "STRICT",
                "FILTER_NAME" => "arrFilter",
                "HIDE_NOT_AVAILABLE" => "N",
                "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                "IBLOCK_ID" => "9",
                "IBLOCK_TYPE" => "CATALOG",
                "INCLUDE_SUBSECTIONS" => "Y",
                "LAZY_LOAD" => "N",
                "LINE_ELEMENT_COUNT" => "3",
                "LOAD_ON_SCROLL" => "N",
                "MESSAGE_404" => "",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_BTN_LAZY_LOAD" => "Показать ещё",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "MESS_NOT_AVAILABLE_SERVICE" => "Недоступно",
                "META_DESCRIPTION" => "-",
                "META_KEYWORDS" => "-",
                "OFFERS_LIMIT" => "5",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Товары",
                "PAGE_ELEMENT_COUNT" => "6",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRICE_CODE" => array(
                    0 => "BASE",
                ),
                "PRICE_VAT_INCLUDE" => "Y",
                "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                "PRODUCT_ID_VARIABLE" => "id",
                "PRODUCT_PROPERTIES" => array(
                ),
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                "PRODUCT_SUBSCRIPTION" => "Y",
                "PROPERTY_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                "RCM_TYPE" => "personal",
                "SECTION_CODE" => "",
                "SECTION_ID" => $_REQUEST["SECTION_ID"],
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "SECTION_URL" => "",
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SEF_MODE" => "Y",
                "SET_BROWSER_TITLE" => "Y",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "SHOW_404" => "N",
                "SHOW_ALL_WO_SECTION" => "N",
                "SHOW_CLOSE_POPUP" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_FROM_SECTION" => "N",
                "SHOW_MAX_QUANTITY" => "N",
                "SHOW_OLD_PRICE" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "SHOW_SLIDER" => "Y",
                "TEMPLATE_THEME" => "blue",
                "USE_ENHANCED_ECOMMERCE" => "N",
                "USE_MAIN_ELEMENT_SECTION" => "N",
                "USE_PRICE_COUNT" => "N",
                "USE_PRODUCT_QUANTITY" => "N",
                "COMPONENT_TEMPLATE" => "main_products_section",
                "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[{\"CLASS_ID\":\"CondIBSection\",\"DATA\":{\"logic\":\"Equal\",\"value\":51}}]}",
                "OFFERS_SORT_FIELD" => "sort",
                "OFFERS_SORT_ORDER" => "asc",
                "OFFERS_SORT_FIELD2" => "id",
                "OFFERS_SORT_ORDER2" => "desc",
                "PROPERTY_CODE_MOBILE" => "",
                "OFFERS_FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "OFFERS_PROPERTY_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "SLIDER_INTERVAL" => "3000",
                "SLIDER_PROGRESS" => "N",
                "PRODUCT_DISPLAY_MODE" => "N",
                "ADD_PICT_PROP" => "-",
                "LABEL_PROP" => "",
                "OFFERS_CART_PROPERTIES" => array(
                )
            ),
            false
        );


    } ?>
<div class="similar-<?= $similarClass ?>">

	<h2><?= $similarTitle ?></h2>

	<div class="block-wrap product-similar block-wrap_wrap ">
        <?php foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <?php
			$titleForSite = "";
			if(empty($arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])){
				$productName = $arItem["NAME"];
			}else{
				$titleForSite = $arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
				$productName = $arItem["NAME"]." | ".$titleForSite;
			}
			?>
			<div class="block-wrap__item block-wrap__item_xl-width2 block-wrap__item_l-width2 block-wrap__item_m-width3 block-wrap__item_s-width3 product-similar__item">
				<div class="catalog-product">
                    <?php
					if(!empty($arItem["OFFERS"])){
						 include __DIR__.'/_list-product-complex.php';
					}
					else{
						 include __DIR__.'/_list-product-simple.php';
					}
				?>
				</div>
			</div>
        <?php endforeach; ?>
	</div>
</div>

    <?php
}
?>
