<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$curPage = $APPLICATION->GetCurPage();

// -> УДАЛИТЬ ВСЕ СЕКЦИЮ ПРИ ПОДКЛЮЧЕНИИ НА ПРОДАКШЕНЕ
/* global $USER;
if ($USER->GetLogin() != "rt-webdesign"){
    $arResult["SECTIONS"] = array_map(function ($section){
        if($section["CODE"] == "shinony"){
            $section["NAME"] = "Хвосты и шиньоны";
            $section["SECTION_PAGE_URL"] = "/khvosty-i-shinony/";
        }
        return $section;
    }, $arResult["SECTIONS"]);
} */
// <- УДАЛИТЬ ВСЕ СЕКЦИЮ ПРИ ПОДКЛЮЧЕНИИ НА ПРОДАКШЕНЕ

?>
<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width12 block-wrap__item_s-width6">
	<nav class="navigation-catalog">
	<?
	if (0 < $arResult["SECTIONS_COUNT"]){ ?>
		<ul class="navigation-catalog__list">
		<? foreach ($arResult['SECTIONS'] as $arSection){
			$ifActive = '';
			if($curPage == $arSection['SECTION_PAGE_URL']){
				$ifActive = ' navigation-catalog__item_active';
			}
			?>
			<li class="navigation-catalog__item<?= $ifActive; ?>">

				<a href="<?= $arSection['SECTION_PAGE_URL']; ?>"><?= $arSection['NAME']; ?></a>

				<?
				if($arSection["ID"] == WIGS_SECTION_ID){
					blockInclude("header", "header-menu-filter-links_wigs_desktop");
				}
				if($arSection["ID"] == HATS_SECTION_ID){
					blockInclude("header", "header-menu-filter-links_hats_desktop");
				}
				if($arSection["ID"] == COVERS_SECTION_ID){
					blockInclude("header", "header-menu-filter-links_covers_desktop");
				}
				if($arSection["ID"] == 17){
					blockInclude("header", "header-menu-filter-links_chignons_desktop");
				}
				?>
			</li>
		<? } ?>
		</ul>
	<? }
	?>
	</nav>
</div>
