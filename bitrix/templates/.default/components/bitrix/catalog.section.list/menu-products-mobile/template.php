<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$curPage = $APPLICATION->GetCurPage();

// -> УДАЛИТЬ ВСЕ СЕКЦИЮ ПРИ ПОДКЛЮЧЕНИИ НА ПРОДАКШЕНЕ
/* global $USER;
if ($USER->GetLogin() != "rt-webdesign"){
    $arResult["SECTIONS"] = array_map(function ($section){
        if($section["CODE"] == "shinony"){
            $section["NAME"] = "Хвосты и шиньоны";
            $section["SECTION_PAGE_URL"] = "/khvosty-i-shinony/";
        }
        return $section;
    }, $arResult["SECTIONS"]);
} */
// <- УДАЛИТЬ ВСЕ СЕКЦИЮ ПРИ ПОДКЛЮЧЕНИИ НА ПРОДАКШЕНЕ

?>
<nav class="navigation-mobile">
	<?
	if (0 < $arResult["SECTIONS_COUNT"]){ ?>
		<ul class="navigation-mobile__list">
		<? foreach ($arResult['SECTIONS'] as $arSection){
			$ifActive = '';
			if($curPage == $arSection['SECTION_PAGE_URL']){
				$ifActive = ' navigation-mobile__item_active';
			}
			?>
			<li class="navigation-mobile__item<?= $ifActive; ?>">
				<a href="<?= $arSection['SECTION_PAGE_URL']; ?>"><?= $arSection['NAME']; ?></a>
				<?
				if($arSection["ID"] == WIGS_SECTION_ID){
					$APPLICATION->IncludeFile(
						$APPLICATION->GetTemplatePath(INCLUDE_AREAS."header/header-menu-filter-links_wigs_mobile.php"),
						Array(),
						Array("MODE"=>"php")
					);
				}
				if($arSection["ID"] == HATS_SECTION_ID){
					$APPLICATION->IncludeFile(
						$APPLICATION->GetTemplatePath(INCLUDE_AREAS."header/header-menu-filter-links_hats_mobile.php"),
						Array(),
						Array("MODE"=>"php")
					);
				}
				if($arSection["ID"] == COVERS_SECTION_ID){
					$APPLICATION->IncludeFile(
						$APPLICATION->GetTemplatePath(INCLUDE_AREAS."header/header-menu-filter-links_covers_mobile.php"),
						Array(),
						Array("MODE"=>"php")
					);
				}
				if($arSection["ID"] == 17){
					$APPLICATION->IncludeFile(
						$APPLICATION->GetTemplatePath(INCLUDE_AREAS."header/header-menu-filter-links_chignons_mobile.php"),
						Array(),
						Array("MODE"=>"php")
					);
				}
				?>
			</li>
		<? } ?>
		</ul>
	<? }
	?>
</nav>
