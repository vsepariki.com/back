<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$text = (!empty($arResult["~PREVIEW_TEXT"])) ? $arResult["~PREVIEW_TEXT"] : false;
$text = (!$text && !empty($arResult["~DETAIL_TEXT"])) ? $arResult["~DETAIL_TEXT"] : $arResult["NAME"];
$previewText = getTruncatedDataForSeo($text);
$APPLICATION->SetPageProperty("description", $previewText);