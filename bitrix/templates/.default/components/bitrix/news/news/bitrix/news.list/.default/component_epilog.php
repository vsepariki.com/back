<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$curPage     = $APPLICATION->GetCurPage();
$sectionName = $arResult["SECTION"]["PATH"][0]["NAME"];

// Если Новости
if(strpos($curPage, 'o-magazine/novosti') !== false){

	// Если Пагинция
	if(!empty($_GET["PAGEN_1"])){
		$APPLICATION->SetPageProperty("title", "Новости / Страница ".$_GET["PAGEN_1"]);
		$APPLICATION->SetPageProperty("canonical", 'http://'.$_SERVER["HTTP_HOST"].$curPage);
	}
}