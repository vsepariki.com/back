<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]):
    $APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]);
else:
    $APPLICATION->SetTitle($arResult["NAME"]);
endif;

$text = (!empty($arResult["~PREVIEW_TEXT"])) ? $arResult["~PREVIEW_TEXT"] : $arResult["NAME"];
$previewText = getTruncatedDataForSeo($text);
$APPLICATION->SetPageProperty("description", $previewText);