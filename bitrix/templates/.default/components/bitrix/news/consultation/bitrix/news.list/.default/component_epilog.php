<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$curPage     = $APPLICATION->GetCurPage();
$sectionName = $arResult["SECTION"]["PATH"][0]["NAME"];

if($sectionName){
	$APPLICATION->SetTitle('Вопросы и ответы: '.$sectionName);
}

// Если Консультация
if(strpos($curPage, 'konsultatsiya') !== false){

	// Не корневой Раздел без Пагинции
	if($sectionName){
		$APPLICATION->SetPageProperty("title", $sectionName.' / Форум, вопросы и ответы');
	}

	// Если Пагинция
	if(!empty($_GET["PAGEN_1"])){
		// Если корневой Раздел
		if($curPage == '/konsultatsiya/'){
			$APPLICATION->SetPageProperty("title", "Парики - форум, вопросы и ответы / Страница ".$_GET["PAGEN_1"]);
			$APPLICATION->SetPageProperty("description", "Вопросы и ответы на тему выбора париков. Форум людей, носящих парики. Задать вопрос специалисту.");
			$APPLICATION->SetPageProperty("keywords", "форум парики вопросы ответы");
		}else{
			if($sectionName){
				$APPLICATION->SetPageProperty("title", $sectionName.' / Форум, вопросы и ответы / Страница '.$_GET["PAGEN_1"]);
			}
		}
	}else{// Если Нет Пагинции
		// Если корневой Раздел
		if($curPage == '/konsultatsiya/'){
			$APPLICATION->SetPageProperty("title", "Парики - форум, вопросы и ответы");
			$APPLICATION->SetPageProperty("description", "Вопросы и ответы на тему выбора париков. Форум людей, носящих парики. Задать вопрос специалисту.");
		}
	}
}

if(!empty($_GET["PAGEN_1"])){
	$APPLICATION->SetPageProperty("canonical", 'http://'.$_SERVER["HTTP_HOST"].$curPage);
}



