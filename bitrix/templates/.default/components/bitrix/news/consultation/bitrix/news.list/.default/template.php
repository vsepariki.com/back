<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$this->addExternalCss("/bitrix/templates/.default/styles/consultation.css");

$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "ask-block",
		"EDIT_TEMPLATE" => ""
  )
);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
  <?
  $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
  $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

  $properties       = $arItem["DISPLAY_PROPERTIES"];

  $authorInfo       = array();

  $name             = $properties["FIO"]["DISPLAY_VALUE"];
  if($name){
		$authorInfo[] = $name;
  }

  $city             = $properties["CITY"]["DISPLAY_VALUE"];
  if($city){
		$authorInfo[] = $city;
  }

  $age              = $properties["AGE"]["DISPLAY_VALUE"];
  if($age){
		$age          = $age.' '.num2word($age);
		$authorInfo[] = trim($age);
  }
  ?>
  <blockquote class="consultation-question-preview" id="<?=$this->GetEditAreaId($arItem['ID']);?>">

		<div class="consultation-question-preview-header">
			 <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="q-title"><?= $arItem["NAME"] ?></a>
		</div>

		<? if($arItem["PREVIEW_TEXT"]): ?>
			 <div class="consultation-question-body">
				  <div class="consultation-question-anons">
						<?= $arItem["~PREVIEW_TEXT"]; ?>
						<div class="consultation-question-readmore">
							<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="q-title">Читать ответ</a>
						</div>
				  </div>
				  <div class="consultation-question-author">
						<p class="consultation-question-author-name"><?= implode(', ', $authorInfo); ?></p>
						<p class="consultation-question-author-date"><?= $arItem["DISPLAY_ACTIVE_FROM"]; ?></p>
				  </div>
			 </div>
		<? else: ?>
			 <div class="consultation-question-body">
				  <div class="consultation-question-author consultation-author-no-anons">
						<p class="consultation-question-author-name">
							 <?= implode(', ', $authorInfo); ?>
							 <span class="consultation-question-author-date">
								  <?= $arItem["DISPLAY_ACTIVE_FROM"]; ?>
							 </span>
						</p>
				  </div>
			 </div>
		<? endif; ?>
  </blockquote>
<?endforeach;?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
  <br /><?=$arResult["NAV_STRING"]?>
<?endif;?>