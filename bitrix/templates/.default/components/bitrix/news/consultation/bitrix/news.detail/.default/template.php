<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

$this->addExternalCss("/bitrix/templates/.default/styles/consultation.css");


$curPage = $APPLICATION->GetCurPage();

$section     = $arResult["SECTION"]["PATH"][0];
$sectionName = strtolower($section["NAME"]);
$sectionUrl  = $section["SECTION_PAGE_URL"];

$sectionsLink = '';
$sectionsRes = CIBlockElement::GetElementGroups($arResult["ID"], true, array("NAME", "SECTION_PAGE_URL"));
while($sectionsArr = $sectionsRes->GetNext()){
    $sectionsLink .= '<a href="'.$sectionsArr["SECTION_PAGE_URL"].'">'.$sectionsArr["NAME"].'</a>, ';
}
$sectionsLink = substr($sectionsLink, 0, -2);



$properties = $arResult["PROPERTIES"];

$authorInfo       = array();

$name             = $properties["FIO"]["VALUE"];
if($name){
    $authorInfo[] = $name;
}

$city             = $properties["CITY"]["VALUE"];
if($city){
    $authorInfo[] = $city;
}

$age              = $properties["AGE"]["VALUE"];
if($age){
    $age          = $age.' '.num2word($age);
    $authorInfo[] = trim($age);
}
?>
 <blockquote class="consultation-question">

	  <h1 class="h2"><?= $arResult["NAME"] ?></h1>

		<? if($arResult["PREVIEW_TEXT"]): ?>
		 <div class="consultation-question-body">
			  <div class="consultation-question-anons" itemprop="name">
                <?= $arResult["~PREVIEW_TEXT"]; ?>
			  </div>
			  <div class="consultation-question-author">
					<p class="consultation-question-author-name"><?= implode(', ', $authorInfo); ?></p>
					<p class="consultation-question-author-date"><?= $arResult["DISPLAY_ACTIVE_FROM"]; ?></p>
			  </div>
		 </div>
		<? else: ?>
		 <div class="consultation-question-body">
			  <div class="consultation-question-author consultation-author-no-anons">
					<p class="consultation-question-author-name">
						 <?= implode(', ', $authorInfo); ?>
						 <span class="consultation-question-author-date">
							  <?= $arResult["DISPLAY_ACTIVE_FROM"]; ?>
						 </span>
					</p>
			  </div>
		 </div>
	<? endif; ?>

 </blockquote>

 <? if($arResult["~DETAIL_TEXT"]): ?>
 <article
     class="consultation-answer"
     itemprop="acceptedAnswer" itemscope itemtype="http://schema.org/Answer"
 >
	  <header class="consultation-answer-header">
			<img src="/upload/medialibrary/0fa/0fa196b06b98ef8e05610e27198985de.jpg" alt="" title="">
			<p>Отвечает: <br>
				 <a href="#" class="answer-doctor-trigger">
					  Ольга, директор центра
				 </a>
			</p>
	  </header> <!-- consultation-answer-header -->
     <div itemprop="text">
         <?= $arResult["~DETAIL_TEXT"] ?>
     </div>
 </article> <!-- consultation-answer -->
 <? endif; ?>

 <? if($sectionsLink): ?>
 <div class="consultation-currentCategory">
	  <p>Категория: <?= $sectionsLink; ?></p>
 </div>
 <? endif; ?>

 <? if($arParams['USER_PARAM_SIMILAR_QUESTIONS']): ?>
 <div class="consultation-similar">
	  <div class="h4">Похожие вопросы</div>
	  <ul class="consultation-similar-items">
			<? foreach($arParams['USER_PARAM_SIMILAR_QUESTIONS'] as $question): ?>
			<li class="consultation-similar-item">
				 <blockquote>
					  <a href="<?= $question["URL"]; ?>" class="link-secondary">
							<?= $question["TITLE"]; ?>
					  </a>
				 </blockquote>
				 <cite>
					  <?
					  if($question["FIO"]){echo $question["FIO"];}
					  if($question["CITY"]){
							if($question["FIO"]){echo ', ';}
							echo $question["CITY"];
					  } ?>
					  <? if($question["FIO"] OR $question["CITY"]){echo ' / ';} ?>
					  <?= $question["DATE"]; ?>
				 </cite>
			</li>
			<? endforeach; ?>
	  </ul>
 </div> <!-- consultation-similar -->
 <? endif; ?>

 <?
 $APPLICATION->IncludeComponent(
	  "bitrix:main.include",
	  "",
	  Array(
			"AREA_FILE_SHOW" => "page",
			"AREA_FILE_SUFFIX" => "ask-block",
			"EDIT_TEMPLATE" => ""
	  )
 );
 ?>


