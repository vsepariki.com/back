<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$sectionCode  = $arResult["VARIABLES"]["SECTION_CODE"];
$questionCode = $arResult["VARIABLES"]["ELEMENT_CODE"];

$IBLOCK_ID = 11; // указываем инфоблок с элементами

$arOrder  = Array("RAND"=>"ASC");
$arFilter = Array(
    "IBLOCK_ID"    => $IBLOCK_ID,
    "ACTIVE"       => "Y",
    "SECTION_CODE" => $sectionCode,
    "!CODE"        => $questionCode
);
$arLimit  = Array ("nTopCount" => 2);
$arSelect = Array(
    "IBLOCK_ID",
    "ID",
    "NAME",
    "DETAIL_PAGE_URL",
    "PROPERTY_FIO",
    "PROPERTY_CITY",
    "PROPERTY_AGE",
    "CREATED_DATE"
);
$res = CIBlockElement::GetList($arOrder, $arFilter, false, $arLimit, $arSelect);

$similarQuestionsArr = array();
while($q = $res->GetNext())
{
    $similarQuestion["ID"]    = $q["ID"];
    $similarQuestion["TITLE"] = $q["NAME"];
    $similarQuestion["URL"]   = $q["DETAIL_PAGE_URL"];
    $similarQuestion["FIO"]   = $q["PROPERTY_FIO_VALUE"];
    $similarQuestion["CITY"]  = $q["PROPERTY_CITY_VALUE"];
    $similarQuestion["AGE"]   = $q["PROPERTY_AGE_VALUE"];
    $similarQuestion["DATE"]  = $q["CREATED_DATE"];

    $similarQuestionsArr[] = $similarQuestion;
}
?>