<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>	<div class="header-search">
		<form class="header-search__form" action="<?=$arResult["FORM_ACTION"]?>">
			<input class="header-search__input" placeholder="Поиск по сайту" type="text" name="q" value="" size="15" maxlength="50">
			<button class="header-search__submit" type="submit" name="s"></button>
		</form>
	</div>