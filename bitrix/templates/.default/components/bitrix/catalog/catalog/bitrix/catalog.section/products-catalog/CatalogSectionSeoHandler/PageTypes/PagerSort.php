<?
namespace CatalogSectionSeoHandler\PageTypes;

class PagerSort implements PageTypesInterface
{
    public $pager;
    public $sort;

    public function __construct($pageParam, $sortParam)
    {
        $this->pager = new Pager($pageParam);
        $this->sort = new Sort($sortParam);
    }

    public function getWigsTitle(): string
    {
        return "Парики - ".$this->sort->title." / ".$this->pager->pageNumberString;
    }

    public function getWigsDescription(): string
    {
        return "Каталог из 200 моделей в наличии. Низкие цены, доставка по России. Купить хороший парики европейского качества недорого. Не отличить от своих волос - ".$this->sort->title.". ".$this->pager->pageNumberString;
    }

    public function getHatsTitle(): string
    {
        return "Головные уборы - ".$this->sort->title." / ".$this->pager->pageNumberString;
    }

    public function getHatsDescription(): string
    {
        return "Широкий выбор головных уборов после химиотерапии и при облысении. Около 100 моделей в наличии для женщин - ".$this->sort->title.". ".$this->pager->pageNumberString;
    }

    public function getCoversTitle(): string
    {
        return "Накладки для волос - ".$this->sort->title." / ".$this->pager->pageNumberString;
    }

    public function getCoversDescription(): string
    {
        return "Каталог из 50 моделей в наличии. Низкие цены, доставка по России. Купить накладку для волос европейского качества. Не отличить от своих волос - ".$this->sort->title.". ".$this->pager->pageNumberString;
    }

    public function getShinonyTitle(): string
    {
        return "Шиньоны для волос - ".$this->sort->title." / ".$this->pager->pageNumberString;
    }

    public function getShinonyDescription(): string
    {
        return "Каталог из 24 моделей в наличии. Низкие цены, доставка по России. Купить шиньон для волос европейского качества - ".$this->sort->title.". ".$this->pager->pageNumberString;
    }

    public function getSotbitTitle($sectionName, $sotbitProps): string
    {
        return $sectionName." - ".$sotbitProps." - ".$this->sort->title." / ".$this->pager->pageNumberString;
    }

    public function getDefaultTitle($sectionName, $filterParts): string
    {
        return trim($sectionName)." ".$filterParts." - ".$this->sort->title." / ".$this->pager->pageNumberString;
    }

}