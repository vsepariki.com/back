<? // --> Вкладки ?>
<div class="product-tabs js-tabs">

	<div class="product-tabs__tabs s-hidden">

		<? if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']): ?>
			<div class="product-tabs__tab product-tabs__tab_active" data-product-tab="product-tab1">Характеристики</div>
		<? endif; ?>

		<? if($arResult["SECTION"]["ID"] == WIGS_SECTION_ID): ?>
			<div class="product-tabs__tab" data-product-tab="product-tab2">Как подобрать цвет?</div>
		<? endif; ?>

		<? if($arResult["SECTION"]["ID"] == WIGS_SECTION_ID): ?>
			<div class="product-tabs__tab" data-product-tab="product-tab3">Размер</div>
		<? endif; ?>

		<div class="product-tabs__tab" data-product-tab="product-tab4">Оплата и доставка</div>

		<? if($arResult["SECTION"]["ID"] == WIGS_SECTION_ID && false): ?>
			<div class="product-tabs__tab" data-product-tab="product-tab5">Вопросы и ответы</div>
		<? endif; ?>

	</div>

	<div class="product-tabs__tabs-content">

		<? include("_tab_1.php"); ?>

		<? if($arResult["SECTION"]["ID"] == WIGS_SECTION_ID): ?>
			<!--noindex--><? include("_tab_2.php"); ?><!--/noindex-->
		<? endif; ?>

		<? if($arResult["SECTION"]["ID"] == WIGS_SECTION_ID): ?>
			<!--noindex--><? include("_tab_3.php"); ?><!--/noindex-->
		<? endif; ?>

		<!--noindex--><? include("_tab_4.php"); ?><!--/noindex-->

		<? if($arResult["SECTION"]["ID"] == WIGS_SECTION_ID && false): ?>
			<!--noindex--><? include("_tab_5.php"); ?><!--/noindex-->
		<? endif; ?>

	</div>

</div>
<? // <-- Вкладки ?>
