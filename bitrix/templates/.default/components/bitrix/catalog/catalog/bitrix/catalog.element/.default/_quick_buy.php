<a
    class="btn product-buy-one-click modal-link-buy-one-click"
    href="#buy-one-click"
    id="quickBuyBtn"
>Заказ в 1 клик</a>

<div class="modal quickBuy" id="buy-one-click">
    <a class="modal__close" href="" rel="modal:close">×</a>
    <div class="modal_body">
		<div class="modal__header">Заказать <?= $strTitle ?></div>
		<p><strong>Укажите номер телефона.</strong></p>
		<p>Перезвоним проконсультировать по товару и помочь в подборе, уточнить место доставки, выбрать удобный способ оплаты.</p>
		<form method="post" class="quick-buy validated-form submitJS" name="quick-buy">
			<div class="quick-buy__row">
        <div class="form-field form-field-phone">
  				<input type="tel" name="phone" class="validated required" placeholder="+7 (___) ___-__-__">
  				<div class="validation-msg validation-msg-required">Напишите телефон мы перезвоним и подтвердим заказ</div>
  			</div>
  			<input type="submit" value="Заказать" class="btn">
      </div>
			<div class="form-field quick-buy__agreement">
				<input type="checkbox" id="quick-buy__agreement" name="agreement" class="validated required" checked required>
        <label for="quick-buy__agreement">Нажимая на кнопку, я даю свое <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">согласие на обработку своих персональных данных</a>.</label>
			</div>
			<input type="hidden" name="productId" value="<?= $arResult["ID"]; ?>">
			<input type="hidden" name="sectionName" value="<?= $arResult["SECTION"]["NAME"]; ?>">
		</form>
	</div>
</div>
