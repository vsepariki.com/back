<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();

//////////////////////////////////////////////////////////////////
$url_parts = explode("?", $APPLICATION->GetCurPage( false ) );
$instance = \Sotbit\Seometa\SeometaUrlTable::getByNewUrl ( $url_parts[0] );

if(!empty($instance)) {
	$arResult["NAV_STRING"] = str_replace( $instance['REAL_URL'] , $instance['NEW_URL'] , $arResult["NAV_STRING"]);
}

// Получение строки установленных фильтров
global $sotbitFilterResult;
$sotbitProps = [];
if(!empty($sotbitFilterResult["ITEMS"])){
	foreach ($sotbitFilterResult["ITEMS"] as $sotbitItem){
		foreach ($sotbitItem["VALUES"] as $sotbitItemValue){
			if(!empty($sotbitItemValue["CHECKED"])){
				$sotbitProps[] = $sotbitItemValue["VALUE"];
			}
		}
	}
}
if(!empty($sotbitProps)){
	$sotbitProps = implode(" ", $sotbitProps);
}else{
	$sotbitProps = "";
}

$cp = $this->__component; // объект компонента
if (is_object($cp))
{
	$cp->arResult["sotbitProps"] = $sotbitProps;
	$cp->arResult['ITEMS_COUNT'] = $arResult['NAV_RESULT']->NavRecordCount;

	if(!empty($arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"])){
		$cp->arResult['SECTION_NAME'] = $arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'];
	}else{
		$cp->arResult['SECTION_NAME'] = $arResult['NAME'];
	}

	$cp->SetResultCacheKeys(['sotbitProps', 'ITEMS_COUNT', 'SECTION_NAME']);
	$arResult["sotbitProps"] = $cp->arResult['sotbitProps'];
}

$arParams = $component->applyTemplateModifications();
