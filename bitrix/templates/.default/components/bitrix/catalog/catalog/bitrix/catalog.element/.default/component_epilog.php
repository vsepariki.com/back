<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}
if (isset($templateData['JS_OBJ']))
{
?><script type="text/javascript">
BX.ready(BX.defer(function(){
	if (!!window.<? echo $templateData['JS_OBJ']; ?>)
	{
		window.<? echo $templateData['JS_OBJ']; ?>.allowViewedCount(true);
	}
}));
</script><?
}


// SEO
if(!empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]))
{
	$titleForSite = $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];

	//для всех товаров из раздела париков из title убрать первое слово в названии парика,
	//а вместо него добавить название модели
	$titleForSiteUpper = "";
	if($arResult["SECTION"]["NAME"] == "Парики"){
		$arrTitleForSite = explode(" ", $titleForSite);
		$arrTitleForSite[0] = $arResult["NAME"];
		$titleForSiteUpper = implode(" ", $arrTitleForSite);
	}

	if($arResult["SECTION"]["NAME"] == "Накладки"){
		$titleForSiteUpper = $arResult["NAME"]." - ".mb_lcfirst($titleForSite);
	}

	if($arResult["SECTION"]["NAME"] == "Шиньоны"){
        $arrTitleForSite = explode(" ", $titleForSite);
        $arrTitleForSite[0] = $arResult["NAME"];
        $titleForSiteUpper = implode(" ", $arrTitleForSite);
	}

	if(!$titleForSiteUpper){
		$titleForSiteUpper = $titleForSite;
	}

	$ePageTitle = $titleForSiteUpper.": цена, купить в Санкт-Петербурге";
	$APPLICATION->SetPageProperty("title", $ePageTitle);

	$ePageH1 = $arResult["NAME"]." <br> <small>".$titleForSite."</small>";
	$APPLICATION->SetTitle($ePageH1);

	$ePageKeywords = $titleForSite." купить цена доставка";
	$APPLICATION->SetPageProperty("keywords", $ePageKeywords);
}

$price = "";
if(!empty($arResult["MIN_PRICE"]["PRINT_DISCOUNT_VALUE"])){
	$price = " за ".$arResult["MIN_PRICE"]["PRINT_DISCOUNT_VALUE"];
}

$ePageDescription = "";

if($arResult["SECTION"]["NAME"] == "Парики"){
	$ePageDescription = $arResult["NAME"].$price." с доставкой по России. 200 париков в наличии. Неотличим от своих волос, надежная фиксация, европейской качество. Подберем стиль в отдельной примерочной.";
}

if($arResult["SECTION"]["NAME"] == "Головные уборы"){
	$ePageDescription = $arResult["NAME"].$price." с доставкой по России. 100 моделей в наличии. Европейское качество. Подберем стиль в отдельной примерочной.";
}

if($arResult["SECTION"]["NAME"] == "Шиньоны"){
	$ePageDescription = $arResult["NAME"].$price." с доставкой по России. 24 модели в наличии, низкие цены, европейской качество. Подберем стиль в отдельной примерочной.";
}

if(!empty($ePageDescription)){
	$APPLICATION->SetPageProperty("description", $ePageDescription);
}

?>