<div class="product-tabs__tab xl-hidden l-hidden m-hidden" data-product-tab="product-tab2">Как подобрать цвет?</div>
<div class="product-tabs__tab-content" data-product-tab="product-tab2">
	<div class="item_info_section product-properties">
		<div class="product-tabs__title">Как подобрать цвет?</div>
		<div class="product-tabs__subheader">Ассортимент цветов</div>
		<p>Каждый парик имеет свой набор оттенков (вариантов цвета). Количество доступных оттенков меняется в зависимости от
			марки (NJ Creation и Ellen Wille), коллекции или модели парика.</p>
		<ul class="list-marked">
			<li><strong>Ellen Wille</strong> - от 10 до 46 вариантов оттенков</li>
			<li><strong>NJ Creation</strong> - от 5 до 39 вариантов оттенков.</li>
		</ul>


		<div class="product-tabs__subheader">Как подобрать соответствие цвета парика к вашему цвету?</div>

		<p>Из-за индивидуальных настроек монитора и разной освещенности, настоящий цвет парика может <strong>сильно
				отличаться</strong> от того,
			что вы видите на экране.</p>

		<p>Если вы хотите подобрать цвет парика <strong>к какому-либо конкретному цвету</strong>, например, вашему натуральному цвету волос, мы
			можем предложить вам <strong>следующие варианты</strong>.</p>

		<table class="table s-hidden" >
			<tr>
				<th>С нашей помощью</th><th>Самостоятельно</th><th>На консультации</th>
			</tr>
			<tr>
				<td>
					<ul class="list-marked">
						<li>Подобрать цвет по пряди ваших волос</li>
						<li>Подобрать цвет по фотографии</li>
						<li>Подобрать цвет на основе вашего текущего цвета парика</li>
					</ul>
				</td>
				<td>
					<ul class="list-marked">
						<li>Подобрать цвет по бесплатной палитре Estel</li>
						<li>Подобрать цвет с помощью кольца цветов</li>
					</ul>
				</td>
				<td>
					<ul class="list-marked">
						<li>В нашем магазин париков</li>
						<li>На онлайн консультации (viber, whatsup или skype)</li>
					</ul>
				</td>
			</tr>
		</table>
		<table class="table xl-hidden l-hidden m-hidden" >
			<tr>
				<th>С нашей помощью</th>
			</tr>
			<tr>
				<td>
					<ul class="list-marked">
						<li>Подобрать цвет по пряди ваших волос</li>
						<li>Подобрать цвет по фотографии</li>
						<li>Подобрать цвет на основе вашего текущего цвета парика</li>
					</ul>
				</td>
			</tr>
			<tr>
        <th>Самостоятельно</th>
			</tr>
			<tr>
				<td>
					<ul class="list-marked">
						<li>Подобрать цвет по бесплатной палитре Estel</li>
						<li>Подобрать цвет с помощью кольца цветов</li>
					</ul>
				</td>
			</tr>
			<tr>
        <th>На консультации</th>
			</tr>
			<tr>
				<td>
					<ul class="list-marked">
						<li>В нашем магазин париков</li>
						<li>На онлайн консультации (viber, whatsup или skype)</li>
					</ul>
				</td>
			</tr>
		</table>
		<?
		// <p>Подробнее, в нашей статье <a href="#">"Как подобрать цвет парика?"</a></p>
		//
		// <div class="product-tabs__subheader">Какой цвет мне подойдет?</div>
		// <p>Для того чтобы выбрать цвет, который будет идеально подходить воспользуйтесь рекомендациями в нашей статье <a href="#">"Как выбрать цвет волос?"</a>.</p>
		?>
	</div>
</div>
