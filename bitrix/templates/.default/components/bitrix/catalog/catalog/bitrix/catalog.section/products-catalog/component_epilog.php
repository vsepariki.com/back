<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

include __DIR__."/CatalogSectionSeoHandler/init.php";

if(setEmptyFilterSotbitMetaData()){
    $arResult["sotbitProps"] = "empty-filter-sotbit-metadata";
}

global $sotbitSeoMetaTitle;
global $sotbitSeoMetaKeywords;
global $sotbitSeoMetaDescription;
global $sotbitSeoMetaBreadcrumbTitle;
global $sotbitSeoMetaH1;

if(!empty($sotbitSeoMetaKeywords)){
	$APPLICATION->SetPageProperty("keywords", $sotbitSeoMetaKeywords);
}

if(!empty($sotbitSeoMetaBreadcrumbTitle)){
	$APPLICATION->AddChainItem($sotbitSeoMetaBreadcrumbTitle);
}

$catalogSectionSeoHandler = new CatalogSectionSeoHandler;

$pageType = $catalogSectionSeoHandler->getPageTypeModel();
$productType = $catalogSectionSeoHandler->getProductTypeModel($arResult["ID"]);

$sotbitProps = $arResult["sotbitProps"];

// Если не Фильтр
if(!$sotbitProps){
    $title = $catalogSectionSeoHandler->getTitle($pageType, $productType);
    $description = $catalogSectionSeoHandler->getDescription($pageType, $productType);
}
// Если Фильтр
else{

    if($sotbitProps == "empty-filter-sotbit-metadata"){
        $sotbitProps = false;
    }

    if($sotbitProps){
        $title = $pageType->getSotbitTitle($productType::NAME, $sotbitProps, $sotbitSeoMetaTitle);
    }else{
        $filterParts = $catalogSectionSeoHandler->getFilterParts($this->globalFilter);
        $title = $pageType->getDefaultTitle($productType::NAME, $filterParts, $sotbitSeoMetaTitle);
    }

    $description = !empty($sotbitSeoMetaDescription) ? $sotbitSeoMetaDescription : false;
}

if($catalogSectionSeoHandler->hasDescriptionForCustomSetURLs()){
	$description = $catalogSectionSeoHandler->getDescriptionForCustomSetURLs($arResult, $sotbitSeoMetaH1);
}

$catalogSectionSeoHandler->setTitle($title);
$catalogSectionSeoHandler->setDescription($description);
$catalogSectionSeoHandler->setCanonical();
