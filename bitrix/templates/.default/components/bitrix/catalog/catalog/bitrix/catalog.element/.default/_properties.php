<?
	if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']){

		$refMainProps = [];

		if($arResult["SECTION"]["ID"] == WIGS_SECTION_ID){
			$refMainProps = [
				"MATERIAL",
				"BASE",
				"SIZE",
				"COLLECTION_WIG",
				"BRAND"
			];
		}

		if($arResult["SECTION"]["ID"] == HATS_SECTION_ID){
			$refMainProps = [
				"MATERIAL_HAT",
				"FASION_HAT",
				"SEASON_HAT",
				"COLLECTION_HAT",
				"BRAND_HAT",
			];
		}

		if($arResult["SECTION"]["ID"] == COVERS_SECTION_ID){
			$refMainProps = [
				"MATERIAL_NATURE_COVER",
				"BASE_COVER",
				"LENGTH_COVER",
				"SIZE_COVER",
				"BRAND_COVER",
			];
		}

		if($arResult["SECTION"]["ID"] == SHINONY_SECTION_ID){
			$refMainProps = [
                "MATERIAL_SHINONY",
                "FEATURE_SHINONY",
                "FIXING_SHINONY",
                "LENGTH_STRING_SHINONY",
                "BRAND_SHINONY",
			];
		}

	?>
	<div class="item_info_section product-properties"><?
		if (!empty($arResult['DISPLAY_PROPERTIES'])){
			foreach ($refMainProps as $refMainProp)
			{

				if(empty($arResult['DISPLAY_PROPERTIES'][$refMainProp])){
					continue;
				}

				$arOneProp = $arResult['DISPLAY_PROPERTIES'][$refMainProp];

				// если в подсказке свойства нет маркера "#i#" - пропуск
				if(strpos($arOneProp["HINT"], '#i#') === false){
					continue;
				}

				$arOneProp["HINT"] = str_replace('#i#', '', $arOneProp["HINT"]); // удалить маркер

			?>
			<div class="product-properties__item">

				<div class="product-properties__name">
					<? echo $arOneProp['NAME']; ?>
				</div>

				<div class="product-properties__value">

					<? if(is_array($arOneProp['DISPLAY_VALUE'])): ?>
						<?= implode(' / ', $arOneProp['DISPLAY_VALUE']) ?>
					<? else: ?>
						<?= $arOneProp['DISPLAY_VALUE'] ?>
					<? endif ?>

					<? if(!empty($arOneProp["HINT"])): ?>
						<a class="product-properties__clarification modal-link" href="#hint-<?= $arOneProp['ID'] ?>">?</a>

						<div class="modal" id="hint-<?= $arOneProp['ID'] ?>">
                            <script>document.write('<a class="modal__close" href="" rel="modal:close">×</a>')</script>
							<div class="modal_body">
								<?= $arOneProp["HINT"] ?>
							</div>
                            <script>document.write('<a class="btn" href="" rel="modal:close">Закрыть</a>')</script>
						</div>

					<? endif; ?>
				</div>
			</div><?
			}
		}
		if ($arResult['SHOW_OFFERS_PROPS']){
			?>
			<dl id="<? echo $arItemIDs['DISPLAY_PROP_DIV'] ?>" style="display: none;"></dl><?
		}
		?>
		</div> <!-- item_info_section --><?
	}
?>
