<?
include __DIR__."/PageTypes/PageTypesInterface.php";
include __DIR__."/ProductTypes/ProductTypesInterface.php";

include __DIR__."/PageTypes/Pager.php";
include __DIR__."/PageTypes/Sort.php";
include __DIR__."/PageTypes/PagerSort.php";
include __DIR__."/PageTypes/Main.php";

include __DIR__."/ProductTypes/Wigs.php";
include __DIR__."/ProductTypes/Hats.php";
include __DIR__."/ProductTypes/Covers.php";
include __DIR__."/ProductTypes/Shinony.php";

include __DIR__."/CatalogSectionSeoHandler.php";

