<?
namespace CatalogSectionSeoHandler\PageTypes;

class Sort implements PageTypesInterface
{
    public $title;

    public function __construct($sortParam)
    {
        if($sortParam == "prices_asc") {
            $this->title = "Сортировка по возрастанию цены";
        }elseif($sortParam == "prices_desc"){
            $this->title = "Сортировка по убыванию цены";
        }else{
            $this->title = "Сортировка по новинкам";
        }
    }

    public function getWigsTitle(): string
    {
        return "Парики - ".$this->title;
    }

    public function getWigsDescription(): string
    {
        return "Каталог из 200 моделей в наличии. Низкие цены, доставка по России. Купить хороший парики европейского качества недорого. Не отличить от своих волос - ".$this->title;
    }

    public function getHatsTitle(): string
    {
        return "Головные уборы - ".$this->title;
    }

    public function getHatsDescription(): string
    {
        return "Широкий выбор головных уборов после химиотерапии и при облысении. Около 100 моделей в наличии для женщин - ".$this->title;
    }

    public function getCoversTitle(): string
    {
        return "Накладки для волос - ".$this->title;
    }

    public function getCoversDescription(): string
    {
        return "Каталог из 50 моделей в наличии. Низкие цены, доставка по России. Купить накладку для волос европейского качества. Не отличить от своих волос - ".$this->title;
    }

    public function getShinonyTitle(): string
    {
        return "Шиньоны для волос - ".$this->title;
    }

    public function getShinonyDescription(): string
    {
        return "Каталог из 24 моделей в наличии. Низкие цены, доставка по России. Купить шиньон для волос европейского качества - ".$this->title;
    }

    public function getSotbitTitle($sectionName, $sotbitProps): string
    {
        return $sectionName." ".$sotbitProps." - ".$this->title;
    }

    public function getDefaultTitle($sectionName, $filterParts): string
    {
        return trim($sectionName)." ".$filterParts." - ".$this->title;
    }

}