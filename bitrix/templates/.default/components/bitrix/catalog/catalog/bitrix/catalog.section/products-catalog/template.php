<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$defaultProductImgSrc = '/upload/medialibrary/baa/baaa03452391641f164ed2e95b71384f.jpg';
$curUri  = $APPLICATION->GetCurUri();
$productNameSeoRefs = include __DIR__.'/product-name-seo-refs.php';

if(
	$curUri == "/catalog/pariki/" OR
	$curUri == "/catalog/golovnye-ubory/" OR
	$curUri == "/catalog/nakladki/" OR
	$curUri == "/catalog/shinony/"
):
	if(!empty($arResult["UF_SECTION_ANONS"])): ?>
		<div class="bx_catalog_anons">
			<?= $arResult["~UF_SECTION_ANONS"]; ?>
		</div>
	<?
	endif;
endif;

global $sotbitSeoMetaTopDesc;
if(
	!empty($sotbitSeoMetaTopDesc) &&
	empty($_GET["PAGEN_1"]) &&
	empty($_GET["sort"])
){ ?>
	<div class="bx_catalog_anons">
		<?= $sotbitSeoMetaTopDesc; ?>
	</div> <?
}

$curPage = $APPLICATION->GetCurPage();
$activeSort = ' catalog-sort__item_active';
$activeSort_2 = $activeSort_3 = $activeSort_4 = $activeSort_5 = '';

if(!empty($_GET['sort'])){
	$sort = $_GET['sort'];
	switch ($sort){
		case 'prices_asc' : $activeSort_2 = $activeSort; break;
		case 'prices_desc': $activeSort_3 = $activeSort; break;
		case 'novelty'    : $activeSort_4 = $activeSort; break;
		case 'discount'   : $activeSort_5 = $activeSort; break;
	}
}else{
	$activeSort_1 = $activeSort;
}
?>
<div class="catalog-sort s-hidden">
	<div class="catalog-sort__title">Сортировать:</div>
	<div class="catalog-sort__items">
		<a class="catalog-sort__item<?= $activeSort_1; ?>" href="<?= $curPage ?>">по популярности</a>
		<a class="catalog-sort__item<?= $activeSort_2; ?>" href="<?= $curPage ?>?sort=prices_asc">по возрастанию цены</a>
		<a class="catalog-sort__item<?= $activeSort_3; ?>" href="<?= $curPage ?>?sort=prices_desc">по убыванию цены</a>
		<a class="catalog-sort__item<?= $activeSort_4; ?>" href="<?= $curPage ?>?sort=novelty">по новинкам</a>
		<!--a class="catalog-sort__item<?//= $activeSort_5; ?>" href="<?//= $curPage ?>?sort=discount">по скидкам</a-->
	</div>
</div>

<div class="block-wrap home-other-products-list block-wrap_wrap ">

	<? if(!$arResult["ITEMS"]): ?>
        <div class="block-wrap__item block-wrap__item_xl-width12">
            <? if(isCurPageFilterResult()): ?>
                <p>
                    По выбранным вами характеристикам не найдено ни одной модели
                    <?= isSectionWigs($arResult["ORIGINAL_PARAMETERS"]["SECTION_ID"]) ? "парика" : "товара" ?>. <br>
                    Попробуйте изменить параметры или снять отметку с незначительных параметров
                </p>
			<? else: ?>
                <p>В категории пока нет товаров</p>
			<? endif ?>
        </div>
        <style>.catalog-sort{display: none;}</style>
    <? endif ?>

	<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
		<?
		    $titleForSite = "";
			if(empty($arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"])){
				$productName = $arItem["NAME"];
			}else{
				$titleForSite = $arItem["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
				$productName = $arItem["NAME"]." | ".$titleForSite;
			}

            if(!empty($productNameSeoRefs[$curPage])){
				$productName .= ' - '.$productNameSeoRefs[$curPage];
            }
		?>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width3 catalog-list__item">
			<div class="catalog-product">
			<?
				if(!empty($arItem["OFFERS"])){
					 include __DIR__.'/_list-product-complex.php';
				}
				else{
					 include __DIR__.'/_list-product-simple.php';
				}
			?>
			</div>
		</div>
	<? endforeach; ?>
</div>

<?
if ($arParams["DISPLAY_BOTTOM_PAGER"])
{
	 echo $arResult["NAV_STRING"];
}

$interestingProductsCount = 12 - count($arResult["ITEMS"]);
if($interestingProductsCount){
	include(__DIR__."/interesting-products.php");
}

if(
	$curUri == "/catalog/pariki/" OR
	$curUri == "/catalog/golovnye-ubory/" OR
	$curUri == "/catalog/nakladki/"OR
    $curUri == "/catalog/shinony/"
):
	if(
		$arParams['HIDE_SECTION_DESCRIPTION'] !== 'Y' &&
		!empty($arResult["DESCRIPTION"])
	)
	: ?>
	<div class="catalog-description">
		<?= $arResult["DESCRIPTION"]; ?>
	</div>
	<?
	endif;
endif;

global $sotbitSeoMetaBottomDesc;
if(
	!empty($sotbitSeoMetaBottomDesc) &&
	empty($_GET["PAGEN_1"]) &&
	empty($_GET["sort"])
){ ?>
	<div class="catalog-description">
		<?= $sotbitSeoMetaBottomDesc; ?>
	</div> <?
}



