<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$defaultImg = '/upload/medialibrary/5a1/5a1aae802149986dd53f3a5568c749d3.gif';
?>
<div class="container home-news">
	<div class="content">

		<div class="block-wrap  block-wrap_wrap ">
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
				<h2 class="link-header2 home-news__header">
					<a href="/o-magazine/novosti/"><span>Новости магазина</span></a>
				</h2>
			</div>
		</div>

		<div class="block-wrap  block-wrap_wrap ">
		<?
		foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3 home-news__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<figure class="home-news__figure">
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="home-news__image-wrapper">

						<? if(!empty($arItem["PREVIEW_PICTURE"])): ?>
							<img
								class="home-news__image"
								src="<?= $arItem["PREVIEW_PICTURE"]["SRC"]; ?>"
								alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"]; ?>"
								title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"]; ?>"
							 	loading="lazy"
								decoding="async"
							/>
						<? else: ?>
						<img
							class="home-news__image"
							src="<?= $defaultImg; ?>"
						 	loading="lazy"
							decoding="async"
							width="264"
							height="200"
						/>
						<? endif; ?>

					</a>
					<figcaption>
						<div class="home-news__date"><?= $arItem["DISPLAY_ACTIVE_FROM"]; ?></div>
						<a class="link-primary home-news__item-header" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?= $arItem["NAME"]; ?></a>
					</figcaption>
				</figure>

				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="home-news__readmore"></a>

			</div>
			<?
		endforeach;
		?>
	 </div>
  </div> <!-- content -->
</div> <!-- home-news -->
