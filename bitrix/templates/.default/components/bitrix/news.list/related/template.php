<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


?>
<?if(count($arResult["ITEMS"])>0):?>


     <h2>Похожие статьи</h2>
        <div class="slider article-preview">
          <div class="slider__slides swiper">
            <div class="slider__wrapper swiper-wrapper">
			
			<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			
			if(strlen($arItem["DETAIL_TEXT"])>0) {
					$articleText = $arItem["DETAIL_TEXT"];
				} else {
					$articleText = $arItem["PREVIEW_TEXT"];
				}

			$readingTime = calculateReadingTime($articleText);
			?>
			
			<? 
			if ($arItem["PREVIEW_PICTURE"]['ID']) {
			$resize_image = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]['ID'],
			Array("width" => 600, "height" => 330),
			BX_RESIZE_IMAGE_EXACT, false);}
			?> 
			
              <div class="slide swiper-slide article-preview__item">
			  <?if ($arItem["PREVIEW_PICTURE"]['ID']) {?>
				<a class="article-preview__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                  <picture>
						<img class="article-preview__image"
                      src="<?=$resize_image['src'];?>" loading="lazy" decoding="async" alt="Превью статьи" />
                  </picture>
                </a>
			  <?} ?><a class="article-preview__title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                <div class="article-info">
                  <div class="article-info__icon article-info__icon--time"><?=$readingTime;?> минут(ы)</div>
                  <div class="article-info__date"><?=FormatDateFromDB($arItem['ACTIVE_FROM'], 'DD MMMM YYYY')?></div>
                  <div class="article-info__icon article-info__icon--viewing"><?=$arItem["SHOW_COUNTER"];?></div>
                </div>
              </div>
			<?endforeach;?>
			<?if (count($arResult['ITEMS']) < 6) {
				$count_items = 6 - count($arResult['ITEMS']);
				$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_TEXT", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "ACTIVE_FROM", "SHOW_COUNTER");
				$arFilter = Array("IBLOCK_ID"=>IntVal(4), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize"=>$count_items), $arSelect);
				
				while($ob = $res->GetNextElement())
				{
					$arFields = $ob->GetFields();
					
					?>
								<?
						
						if(strlen($arFields["DETAIL_TEXT"])>0) {
								$articleText = $arFields["DETAIL_TEXT"];
							} else {
								$articleText = $arFields["PREVIEW_TEXT"];
							}

						$readingTime = calculateReadingTime($articleText);
						?>
						
						<? 
						if ($arFields["PREVIEW_PICTURE"]['ID']) {
						$resize_image = CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"]['ID'],
						Array("width" => 600, "height" => 330),
						BX_RESIZE_IMAGE_EXACT, false);}
						?> 
						
						  <div class="slide swiper-slide article-preview__item">
						  <?if ($arFields["PREVIEW_PICTURE"]['ID']) {?>
							<a class="article-preview__link" href="<?=$arFields["DETAIL_PAGE_URL"]?>">
							  <picture>
									<img class="article-preview__image"
								  src="<?=$resize_image['src'];?>" loading="lazy" decoding="async" alt="Превью статьи" />
							  </picture>
							</a>
						  <?} ?><a class="article-preview__title" href="<?=$arFields["DETAIL_PAGE_URL"]?>"><?=$arFields["NAME"]?></a>
							<div class="article-info">
							  <div class="article-info__icon article-info__icon--time"><?=$readingTime;?> минут(ы)</div>
							  <div class="article-info__date"><?=FormatDateFromDB($arFields['ACTIVE_FROM'], 'DD MMMM YYYY')?></div>
							  <div class="article-info__icon article-info__icon--viewing"><?=$arFields["SHOW_COUNTER"];?></div>
							</div>
						  </div>
						  <?
				}

			}?>

            </div>
            <div class="slider__arrow slider__arrow--prev"></div>
            <div class="slider__arrow slider__arrow--next"></div>
            <div class="slider__pagination"></div>
          </div>
        </div>
		
		<?endif?>