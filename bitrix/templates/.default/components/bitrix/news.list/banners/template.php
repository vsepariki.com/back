<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$webPHelper = new WebPHelper;

foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$link = (!empty($arItem["DISPLAY_PROPERTIES"]["LINK"]["DISPLAY_VALUE"])) ? $arItem["DISPLAY_PROPERTIES"]["LINK"]["DISPLAY_VALUE"] : '';

	?>
	<div class="home-slider__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<? if($link): ?><a href="<?= $link; ?>"><? endif; ?>
            <picture>
                <source srcset="<?= $webPHelper->getWebpOrDefaultPath($arItem["PREVIEW_PICTURE"]["SRC"]) ?>">
                <img
                    src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                    alt=""
                    title=""
										decoding="async"
				            width="<?= $arItem['PREVIEW_PICTURE']['WIDTH'] ?>"
				            height="<?= $arItem['PREVIEW_PICTURE']['HEIGHT'] ?>"
                />
            </picture>
			<? if($link): ?></a><? endif; ?>
	</div>
	<?
endforeach;
