<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <?
    foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) {continue;}
        ?>
        <div class="consultation-categories-item">
            <?if($arItem["SELECTED"]):?>
                <div class="consultation-categories-item-name active">
                    <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                </div>
            <?else:?>
                <div class="consultation-categories-item-name">
                    <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                </div>
            <?endif?>
            <div class="consultation-categories-item-count">(<?= $arItem["PARAMS"]["ELEMENT_CNT"]; ?>)</div>
        </div>
    <?endforeach?>

<?endif?>

