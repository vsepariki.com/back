<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0) {
	?>
	<div class="stati-block">
		<ul>
			<?
			$prev_level = 1;
			foreach ($arResult['ITEMS'] as $key => $arItem)
			{
				if($key AND !$arItem["ELEMENT_CNT"]){continue;}

				$sectionName = $arItem['NAME'];
				?>
				<li>
				    <?
				    if(
				        $sectionName
				        AND(
				            ($arItem['DEPTH_LEVEL'] > 1)
				            OR ($arResult["ROOT"] == "Y")
                        )
                    ): ?>
                        <div class="h4"><?= $sectionName ?></div><?
				    endif;

				    if($arItem['DEPTH_LEVEL']){
				        $priceDepthLevel = $arItem['DEPTH_LEVEL'] + 1;
				    }else{
				        $priceDepthLevel = 1;
				    }

					if($arItem['CHILD']):
						foreach($arItem['CHILD'][0] as $price)
						{
						?>
						 <div class="stati-item stati-item-level-<?= $priceDepthLevel ?>">
							  <div class="stati-item-title">
									<a href="<?= $price['DETAIL_PAGE_URL'] ?>"><?= $price['NAME'] ?></a>
							  </div>
						 </div><?
						}
					endif; ?>

				 </li>
			 	<?
            }
			?>
		</ul>

	</div>
<?
}
?>