<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$offersColors = array();

$fullSeoName = $arItem["NAME"];
if($titleForSite){
    $fullSeoName .= " ".$titleForSite;
}

$pictureType = "default";
if(!empty($arItem['PREVIEW_PICTURE']['SRC'])){
    $pictureType = 'PREVIEW_PICTURE';
}elseif(!empty($arItem['DETAIL_PICTURE']['SRC'])){
    $pictureType = 'DETAIL_PICTURE';
}

$webPHelper = new WebPHelper;
?>

<div class="catalog-product__offer">

    <a class="catalog-product__image" href="<?= $arItem['DETAIL_PAGE_URL']; ?>">

        <? if($pictureType == "default"): ?>
            <img
                src="<?= $defaultProductImgSrc ?>"
                alt=""
            >
        <? else: ?>
            <picture>
                <source
                    <? if($ifActive): ?>
                        srcset="<?= $webPHelper->getOrCreateWebp($arItem[$pictureType]['SRC']) ?>"
                    <? else: ?>
                        srcset=""
                        data-srcset="<?= $webPHelper->getOrCreateWebp($arItem[$pictureType]['SRC']) ?>"
                    <? endif ?>
                >
                <img
                    <? if($ifActive): ?>
                        src="<?= $arItem[$pictureType]['SRC'] ?>"
                    <? else: ?>
                        src=""
                        data-src="<?= $arItem[$pictureType]['SRC'] ?>"
                    <? endif ?>

                    alt="<?= $fullSeoName ?>"
                    title="<?= $fullSeoName ?>"

                    width="<?= $arItem[$pictureType]['WIDTH'] ?>"
                    height="<?= $arItem[$pictureType]['HEIGHT'] ?>"

                    loading="lazy"
                    decoding="async"
                >
            </picture>
        <? endif ?>

    </a>

    <div class="catalog-product__name">
        <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>"><?= $productName ?></a>
    </div>

    <div class="catalog-product__prices">

        <div class="catalog-product__price">

            <?= number_format($arItem["CATALOG_PRICE_1"], 0, ',', ' ') ?>  руб.
        </div>
    </div>

</div> <!-- catalog-product__offer -->

<? if(count($arItem["OFFERS"])): ?>

    <div class="catalog-product__colors">
        <div class="catalog-product-colors">
            <ul class="catalog-product-colors__list">
                <? foreach($arItem["OFFERS"] as $offer): ?>
                    <li class="catalog-product-colors__item">
                        <img
                            src="#"
                            data-src="<?= $offer['colorFileSrc'] ?>"
                            alt=""
                            title="<?= $offer['colorTitle'] ?>"
                        >
                    </li>
                <? endforeach ?>
            </ul>
        </div>
    </div>
<? endif ?>
