<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/***********
 * Разделы *
 ***********/

$iBlockId = intval($arParams["IBLOCK_ID"]);
$sectionCode = $arParams["SECTION_CODE"];


$arParentSection = false;

//если не главная Раздела, а подРаздел -> формируем крошки и добавляем SEO на страницу
if($sectionCode){
    $rsSections = CIBlockSection::GetList(
        array(),
        array(
            'IBLOCK_ID' => $iBlockId,
            '=CODE' => $sectionCode
        )
    );
    $arParentSection = $rsSections->Fetch();
    $APPLICATION->AddChainItem($arParentSection["NAME"]);

    $ipropValues          = new \Bitrix\Iblock\InheritedProperty\SectionValues($iBlockId, $arParentSection["ID"]);
    $sectionSeoProperties = $ipropValues->getValues();

    $sectionSeoTitle           = $sectionSeoProperties["SECTION_PAGE_TITLE"];
    if($sectionSeoTitle){
        $APPLICATION->SetTitle($sectionSeoTitle);
    }

    $sectionSeoMetaTitle       = $sectionSeoProperties["SECTION_META_TITLE"];
    if($sectionSeoMetaTitle){
        $APPLICATION->SetPageProperty("title", $sectionSeoMetaTitle);
    }

    $sectionSeoMetaKeywords    = $sectionSeoProperties["SECTION_META_KEYWORDS"];
    if($sectionSeoMetaKeywords){
        $APPLICATION->SetPageProperty("keywords", $sectionSeoMetaKeywords);
    }

    $sectionSeoMetaDescription = $sectionSeoProperties["SECTION_META_DESCRIPTION"];
    if($sectionSeoMetaDescription){
        $APPLICATION->SetPageProperty("description", $sectionSeoMetaDescription);
    }

}else{
    $arResult["ROOT"] = "Y";
}


// Запрос по Разделам
$arFilter = array(
    "IBLOCK_ID" => $iBlockId,
    "CHECK_PERMISSIONS" => "Y",
    "ACTIVE" => "Y",
    "<=DEPTH_LEVEL" => 2
);
if($arParentSection){
    $arFilter['>LEFT_MARGIN'] = $arParentSection['LEFT_MARGIN'];
    $arFilter['<RIGHT_MARGIN'] = $arParentSection['RIGHT_MARGIN'];
}

$arSelect = array(
    "ID",
    "IBLOCK_SECTION_ID",
    "NAME",
    "DEPTH_LEVEL",
    "DESCRIPTION"
);

$sectionsId = array();
$rs_section = CIBlockSection::GetList(Array("LEFT_MARGIN" => "ASC"), $arFilter, true, $arSelect);
while($ar_section = $rs_section->Fetch())
{
   $arResult["SECTIONS"][$ar_section["ID"]] = $ar_section;
   $sectionsId[] = $ar_section["ID"];
}

/************
 * Элементы *
 ************/
$arFilter = array(
    'IBLOCK_ID' => $iBlockId,
    'INCLUDE_SUBSECTIONS' => 'Y',
    'ACTIVE' => 'Y'
);
if($arParentSection){
    $arFilter['SECTION_ID'] = $sectionsId;
}

$arSelect = array(
    'IBLOCK_SECTION_ID',
    'NAME',
    'DETAIL_PAGE_URL',
    'PROPERTY_BASE_PRICE',
    'PROPERTY_ACTION_PRICE',
    'PROPERTY_SERVICE_LINK',
);

$rs_element = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);

$linkedServicesIdArr = array();
$ar_elements = array();
// В цикле получаем массив всех Элементов
// и массив привязанных Разделов (ID)
while($ar_element = $rs_element->GetNext(false, false))
{
    $ar_elements[] = $ar_element;
    if($ar_element["PROPERTY_SERVICE_LINK_VALUE"]){
        $linkedServicesIdArr[] = $ar_element["PROPERTY_SERVICE_LINK_VALUE"];
    }
}

// Если среди выбранных Элементов есть привязанные Разделы - расшифровуем ID в Символьный путь
if($linkedServicesIdArr)
{
    // Получаем массив связок ID раздела - символьный путь Раздела
    $rs_sections = CIBlockSection::GetList(array(), array('ID' => $linkedServicesIdArr), false, array('SECTION_PAGE_URL'));
    $linkedServicesArr = array();
    while($ar_section = $rs_sections->GetNext())
    {
        $linkedServicesArr[$ar_section["ID"]] = $ar_section["SECTION_PAGE_URL"];
    }


    // В массиве Элементов заменяем ID привязанного Раздела на его Символьный путь
    foreach($ar_elements as $key => $ar_element){
        if($ar_element["PROPERTY_SERVICE_LINK_VALUE"]){
            $serviceId = (int)$ar_element["PROPERTY_SERVICE_LINK_VALUE"];
            $ar_elements[$key]["PROPERTY_SERVICE_LINK_VALUE"] = $linkedServicesArr[$serviceId];
        }
    }
}

// Формируем результирующий массив Элементов с разбивкой по род. Разделам
foreach($ar_elements as $key => $ar_element){
    $arResult["ELEMENTS"][$ar_element["IBLOCK_SECTION_ID"]][] = $ar_element;
}


/*********************
 * Построение дерева *
 *********************/
// Если в Разделе есть Элементы - добавлем в секцию 'CHILD'
foreach($arResult["SECTIONS"] as $sectionKey => $sectionVal){
    foreach($arResult["ELEMENTS"] as $elementKey => $elementVal){
        if($sectionKey == $elementKey){
            $arResult["SECTIONS"][$sectionKey]['CHILD'][] = $elementVal;
        }
    }
}

// если элемент в корне 1-го уровня
foreach($arResult["ELEMENTS"] as $element){
    if(!$element[0]['IBLOCK_SECTION_ID']){
        $arResult["SECTIONS"][0]['CHILD'][] = $element;
    }
}


$arResult["ITEMS"] = $arResult["SECTIONS"];

$this->IncludeComponentTemplate();
?>