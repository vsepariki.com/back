<?
class Model
{
    const MAX_PRODUCTS_COUNT_BY_TAB = 8;
    const MAX_OFFERS_COUNT_BY_PRODUCT = 10;

    public function getProducts($arFilter)
    {
        $arOrder = [
            "show_counter" => "DESC",
            "active_from" => "DESC"
        ];

        $arFilter = array_merge(
            $arFilter,
            [
                "IBLOCK_ID" => IBLOCK_PRODUCTS_ID,
                "ACTIVE" => "Y",
                "GLOBAL_ACTIVE" => "Y",
                "SECTION_ID" => WIGS_SECTION_ID,
            ]
        );

        $arGroupBy = false;
        $arNavStartParams = ["nTopCount" => self::MAX_PRODUCTS_COUNT_BY_TAB];
        $arSelectFields = [
            "ID",
            "IBLOCK_ID",
            "NAME",
            "SHOW_COUNTER",
            "PREVIEW_PICTURE",
            "DETAIL_PICTURE",
            "DETAIL_PAGE_URL",
            "IBLOCK_SECTION_ID",
            "CATALOG_GROUP_1",
        ];

        $resource = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

        $products = [];
        while($product = $resource->GetNext()) {
            $products[$product["ID"]] = $product;
        }

        return $products;
    }

    public function replacePictureIdWithRealSrc($productsByTabs)
    {
        $picturesId = [];

        foreach ($productsByTabs as $products) {
            foreach ($products as $product) {

                if(!empty($product["PREVIEW_PICTURE"])){
                    $picturesId[] = $product["PREVIEW_PICTURE"];
                }

                if(!empty($product["DETAIL_PICTURE"])){
                    $picturesId[] = $product["DETAIL_PICTURE"];
                }

                if(!empty($product["OFFERS"])){
                    $offers = $product["OFFERS"];
                    foreach ($offers as $offer){
                        $picturesId[] = $offer["colorFileId"];
                    }
                }

            }
        }
        $picturesId = implode(",", $picturesId);

        $refPictureIdToPath = [];
        $fileGetListRes = \CFile::GetList([], ["@ID" => $picturesId]);
        while ($fileGetList = $fileGetListRes->GetNext()) {
            $fileGetList["SRC"] = "/upload/".$fileGetList["SUBDIR"]."/".$fileGetList["FILE_NAME"];
            $refPictureIdToPath[$fileGetList["ID"]] = $fileGetList;
        }

        foreach ($productsByTabs as $tabKey => $products) {
            foreach ($products as $productId => $product) {

                if(!empty($product["PREVIEW_PICTURE"])){
                    $previewPictureId = $product["PREVIEW_PICTURE"];
                    $productsByTabs[$tabKey][$productId]["PREVIEW_PICTURE"] = $refPictureIdToPath[$previewPictureId];
                }

                if(!empty($product["DETAIL_PICTURE"])){
                    $detailPictureId = $product["DETAIL_PICTURE"];
                    $productsByTabs[$tabKey][$productId]["DETAIL_PICTURE"] = $refPictureIdToPath[$detailPictureId];
                }

                if(!empty($product["OFFERS"])){
                    foreach($product["OFFERS"] as $offerId => $offer){
                        $offerColorFileSrc = $refPictureIdToPath[$offer["colorFileId"]]["SRC"];
                        $productsByTabs[$tabKey][$productId]["OFFERS"][$offerId]["colorFileSrc"] = $offerColorFileSrc;
                    }
                }

            }
        }

        return $productsByTabs;
    }
    public function addIPropertyValues($productsByTabs)
    {
        foreach ($productsByTabs as $tabKey => $products) {
            foreach ($products as $productId => $product) {
                $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(
                    IBLOCK_PRODUCTS_ID,
                    $productId
                );
                $productsByTabs[$tabKey][$productId]["IPROPERTY_VALUES"] = $ipropValues->getValues();
            }
        }

        return $productsByTabs;
    }

    private function getOffers($productsId)
    {
        $rawOffersAll = \CCatalogSKU::getOffersList(
            $productsId,
            IBLOCK_PRODUCTS_ID,
            [],
            ["NAME"],
            ["CODE" => ["COLOR"]]
        );

        $offers = [];
        foreach($rawOffersAll as $productId => $rawOffersByProduct){
            foreach($rawOffersByProduct as $offerId => $rawOffer){
                $offers[$productId][$offerId] = [
                    "NAME" => $rawOffer["NAME"],
                    "COLOR" => $rawOffer["PROPERTIES"]["COLOR"]["VALUE"],
                ];
            }
        }

        return $offers;
    }

    public function addOffers($productsByTabs)
    {
        $productsId = [];
        foreach ($productsByTabs as $products) {
            $productsId = array_merge($productsId, array_keys($products));
        }

        $offers = $this->getOffers($productsId);

        foreach ($productsByTabs as $tabKey => $products) {
            foreach (array_keys($products) as $productId) {
                if(!empty($offers[$productId])){
                    $allOffersByProduct = $offers[$productId];
                    $offersByProduct = array_slice($allOffersByProduct, 0, self::MAX_OFFERS_COUNT_BY_PRODUCT);
                    $productsByTabs[$tabKey][$productId]["OFFERS"] = $offersByProduct;
                }
            }
        }

        return $productsByTabs;
    }

    private function getColorFileIdByXmlId($productsByTabs)
    {
        if (!CModule::IncludeModule('highloadblock')){
            return;
        }

        $colorsXmlId = [];
        foreach ($productsByTabs as $products) {
            foreach ($products as $product) {
                foreach ($product["OFFERS"] as $offer) {
                    $colorsXmlId[] = $offer["COLOR"];
                }
            }
        }

        $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(HIGHLOAD_BLOCK_ID_COLORS)->fetch();
        $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
        $hlDataClass = $hldata['NAME'].'Table';

        $resource = $hlDataClass::getList(array(
            'select' => ['UF_FILE', 'UF_NAME', 'UF_XML_ID'],
            'order' => ['UF_NAME' =>'ASC'],
            'filter' => ["UF_XML_ID" => $colorsXmlId]
        ));


        $hlColors = [];
        while($hlColor = $resource->fetch()) {
            $hlColors[$hlColor["UF_XML_ID"]] = $hlColor;
        }

        return $hlColors;
    }

    public function addColorsImgToOffers($productsByTabs)
    {
        $hlColors = $this->getColorFileIdByXmlId($productsByTabs);

        foreach ($productsByTabs as $tabKey => $products) {
            foreach ($products as $productId => $product) {
                foreach ($product["OFFERS"] as $offerId => $offer) {
                    if(!empty($hlColors[$offer["COLOR"]])){
                        $hlColor = $hlColors[$offer["COLOR"]];
                        $productsByTabs[$tabKey][$productId]["OFFERS"][$offerId] = [
                            "colorTitle" => $hlColor["UF_NAME"],
                            "colorFileId" => $hlColor["UF_FILE"],
                        ];
                    }
                }
            }
        }

        return $productsByTabs;
    }

}