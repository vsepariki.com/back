<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock")){
	return;
}

$arComponentParameters = array(
	"PARAMETERS" => array(
		"CACHE_TIME"  => array("DEFAULT"=>36000000),
		"CACHE_FILTER" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => "CACHE_FILTER",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"CACHE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => "CACHE_GROUPS",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
	),
);