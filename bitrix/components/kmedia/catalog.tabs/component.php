<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Context;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Loader;
use Bitrix\Iblock;

if(!isset($arParams["CACHE_TIME"])){
  $arParams["CACHE_TIME"] = 36000000;
}

$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0){
  $arParams["CACHE_TIME"] = 0;
}

require __DIR__."/model.php";
$model = new Model();

//MATERIAL
define("ID_PROP_VAL_ISKUSSTVENNYY", 192);
define("ID_PROP_VAL_NATURALNYY", 241);

//LENGTH_FILTER
define("ID_PROP_VAL_KOROTKIY", 187);
define("ID_PROP_VAL_SREDNIY", 228);
define("ID_PROP_VAL_DLINNYY", 230);

//TYPE
define("ID_PROP_VAL_ZHENSKIY", 186);
define("ID_PROP_VAL_MUZHSKOY", 246);

if($this->startResultCache())
{
	if(!Loader::includeModule("iblock"))
	{
		$this->abortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

    $productsByTabs = [];

    $productsByTabs = [
        "Из моноволокна" => $model->getProducts([
            "PROPERTY_TYPE" => ID_PROP_VAL_ZHENSKIY,
            "PROPERTY_MATERIAL" => ID_PROP_VAL_ISKUSSTVENNYY
        ]),
        "Натуральные" => $model->getProducts([
            "PROPERTY_TYPE" => ID_PROP_VAL_ZHENSKIY,
            "PROPERTY_MATERIAL" => ID_PROP_VAL_NATURALNYY
        ]),
        "Короткие" => $model->getProducts([
            "PROPERTY_TYPE" => ID_PROP_VAL_ZHENSKIY,
            "PROPERTY_LENGTH_FILTER" => ID_PROP_VAL_KOROTKIY
        ]),
        "Средние" => $model->getProducts([
            "PROPERTY_TYPE" => ID_PROP_VAL_ZHENSKIY,
            "PROPERTY_LENGTH_FILTER" => ID_PROP_VAL_SREDNIY
        ]),
        "Длинные" => $model->getProducts([
            "PROPERTY_TYPE" => ID_PROP_VAL_ZHENSKIY,
            "PROPERTY_LENGTH_FILTER" => ID_PROP_VAL_DLINNYY
        ]),
        "Мужские" => $model->getProducts([
            "PROPERTY_TYPE" => ID_PROP_VAL_MUZHSKOY,
        ]),
    ];

    $productsByTabs = $model->addIPropertyValues($productsByTabs);
    $productsByTabs = $model->addOffers($productsByTabs);
    $productsByTabs = $model->addColorsImgToOffers($productsByTabs);
    $productsByTabs = $model->replacePictureIdWithRealSrc($productsByTabs);

    $arResult["TABS_ITEMS"] = $productsByTabs;

    $this->setResultCacheKeys(array(
		"TABS_ITEMS",
	));

	$this->includeComponentTemplate();
}

