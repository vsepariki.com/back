<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "catalog.tabs",
	"DESCRIPTION" => "Вывод списка продукта во вкладках",
	"ICON" => "",
	"SORT" => 20,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "content",
	),
);

?>