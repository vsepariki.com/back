<?
namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Order;
use Exception;

class LocationModel
{
    public function getLocationToName(Order $order) : string
    {
        $locationToCode = $this->getLocationToCode($order);

        if(!$arLocs = \CSaleLocation::GetByID($locationToCode)){
            throw new Exception("Ошибка при получении названия города");
        }

        return $arLocs["CITY_NAME"];
    }

    private function getLocationToCode(Order $order) : string
    {
        if($props = $order->getPropertyCollection()){
            if($locationProp = $props->getDeliveryLocation()){
                if($locationToCode = $locationProp->getValue()){
                    return $locationToCode;
                }
            }
        }

        throw new Exception("Ошибка при получении внутреннего кода города");
    }
}