<?
namespace Sale\Handlers\Delivery;

use Exception;

class RecipientModel
{
    private $requestModel;

    public function __construct()
    {
        $this->requestModel = new RequestModel();
    }

    public function getCodeRecipient(string $locationToName) : string
    {
        $cityIndex = $this->getCityIndexByCityName($locationToName);

        $requestParameters = $this->getRequestParameters($cityIndex);
        $codeRecipient = $this->requestModel->getCodeRecipient($requestParameters);

        return $codeRecipient;
    }

    private function getCityIndexByCityName(string $cityToName) : string
    {
        try{
            $russianPostModel = new DeliveryRussianPostModel();
            $index = $russianPostModel->getCityIndex($cityToName);
        }catch (Exception $exceptionRussianPostModel){
            try{
                $dbCityIndexModel = new DbCityIndexModel();
                $index = $dbCityIndexModel->getCityIndex($cityToName);
            } catch (Exception $exceptionDbCityIndexModel){
                throw new Exception("Ошибка при получении индекса города");
            }
        }

        return $index;
    }

    private function getRequestParameters(string $cityIndex) : \stdClass
    {
        $list = [];

        $listReference = new \stdClass();
        $listReference->Key = "Reference";
        $listReference->Value = "Geography";
        $listReference->ValueType = "string";
        $list[] = $listReference;

        $listSearch = new \stdClass();
        $listSearch->Key = "Search";
        $listSearch->Value = "postcode-".$cityIndex;
        $listSearch->ValueType = "string";
        $list[] = $listSearch;

        $parameters = new \stdClass();
        $parameters->Key = "Parameters";
        $parameters->List = $list;

        return $parameters;
    }

}