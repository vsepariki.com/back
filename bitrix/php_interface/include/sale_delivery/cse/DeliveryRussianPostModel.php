<?
namespace Sale\Handlers\Delivery;

class DeliveryRussianPostModel
{
	const ACCESS_TOKEN = "Z_EDrt5QtYAX3eqqopoK_6F6R4lXRh9D";
	const AUTHORIZATION_KEY = "TWFuYWdlcjFAdm9sb3N5MjQucnU6Vm9Mb1N5MjAxMA==";
	const INDEX_FROM = 190882; // индекс Санкт-Петербурга
	const PROTOCOL = "https://";
	const HOST = "otpravka-api.pochta.ru";

    public function getCityIndex(string $cityName) : string
	{
		$action = "/postoffice/1.0/by-address?address=".urlencode($cityName)."&top=5";

		$response = $this->getRemoteData($action);

		$errorMessage = "Не удалось найти индекс населенного пункта по его названию";
		if(!empty($response["error"])){
			throw new \Exception($errorMessage.$response["message"]);
		}
		if(empty($response["postoffices"])){
			throw new \Exception($errorMessage.$response["desc"]);
		}

		$cityIndex = (int)$response["postoffices"][0];

		return $cityIndex;
	}

    private function getRemoteData(string $action) : array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::PROTOCOL.self::HOST.$action);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Content-Type: application/json",
            "Accept: application/json;charset=UTF-8",
            "Authorization: AccessToken ".self::ACCESS_TOKEN,
            "X-User-Authorization: Basic ".self::AUTHORIZATION_KEY
        ]);

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

}


