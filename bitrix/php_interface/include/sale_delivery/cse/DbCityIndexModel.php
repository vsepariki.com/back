<?
namespace Sale\Handlers\Delivery;

use Exception;

class DbCityIndexModel
{
    const TABLE_NAME_CITY_NAME = "zag_sale_location_city_lang";
    const TABLE_NAME_LOCATION = "zag_sale_location";
    const TABLE_NAME_INDEXES = "zag_sale_loc_ext";

    private $db;

    public function __construct()
    {
        global $DB;
        $this->db = $DB;
    }

    public function getCityIndex(string $cityName) : string
    {
        $cityId = $this->getCityId($cityName);
        $locationId = $this->getLocationId($cityId);
        $cityIndex = $this->getCityIndexByLocationId($locationId);

        return $cityIndex;
    }

    private function getCityId(string $cityName) : int
    {
        $query = 'SELECT `CITY_ID` FROM  `'.self::TABLE_NAME_CITY_NAME.'` WHERE `NAME` = "'.$cityName.'"';
        if(!$result = $this->db->Query($query)->Fetch()){
            throw new Exception("Не найден Id города по его названию");
        }

        $cityId = (int)$result["CITY_ID"];

        return $cityId;
    }

    private function getLocationId(int $cityId) : int
    {
        $query = 'SELECT `ID` FROM  `'.self::TABLE_NAME_LOCATION.'` WHERE `CITY_ID` = "'.$cityId.'"';
        if(!$result = $this->db->Query($query)->Fetch()){
            throw new Exception("Не найден Location Id города по его Id");
        }

        $locationId = (int)$result["ID"];

        return $locationId;
    }

    private function getCityIndexByLocationId(int $locationId) : string
    {
        $query = 'SELECT `XML_ID` FROM  `'.self::TABLE_NAME_INDEXES.'` WHERE `LOCATION_ID` = "'.$locationId.'" ORDER BY `XML_ID` ASC';
        if(!$result = $this->db->Query($query)->Fetch()){
            throw new Exception("Не найден Index города по его Location Id");
        }

        $cityIndex = (int)$result["XML_ID"];

        return $cityIndex;
    }

}