<?
namespace Sale\Handlers\Delivery;

use Exception;

class RequestModel
{
    const LOGIN = "ЦДВОЛОС";
    const PASSWORD = "1234567890";
    const WSDL_PATH = "http://web.cse.ru/1c/ws/Web1C.1cws?wsdl";

    private $client;

    public function __construct()
    {
        $this->client = new \SoapClient(self::WSDL_PATH, ["trace" => true]);
    }

    public function getCodeRecipient(\stdClass $requestParameters) : string
    {
        $params = [
            "login" => self::LOGIN,
            "password" => self::PASSWORD,
            "parameters" => $requestParameters
        ];

        $response = $this->client->GetReferenceData($params);

        if(empty($response->return->List)){
            throw new Exception("Ошибка при получение географии города доставки");
        }

        $keyGeography = $response->return->List->Key;

        return $keyGeography;
    }

    public function getTariffs(\stdClass $requestData, \stdClass $requestParameters)
    {
        $params = [
            "login" => self::LOGIN,
            "password" => self::PASSWORD,
            "data" => $requestData,
            "parameters" => $requestParameters,
        ];

        try {
            $response = $this->client->Calc($params);
            $tariffs = $response->return->List->List;

            if(!$tariffs){
                throw new Exception("Ошибка при получении тарифов");
            }

            if(!is_array($tariffs)){
                $tariffs = [$tariffs];
            }

        }catch (Exception $e){
            throw new Exception($this->client->__getLastResponse());
        }

        return $tariffs;
    }


}