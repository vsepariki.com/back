<?
namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Shipment;

class DeliveryCseModel
{
    const CODE_SENDER = "cf862f77-442d-11dc-9497-0015170f8c09"; // Санкт-Петербург
    const TYPE_OF_CARGO = "4aab1fc6-fc2b-473a-8728-58bcd4ff79ba"; // Вид груза: Груз
    const URGENCY = "18c4f207-458b-11dc-9497-0015170f8c09"; // Вид срочности: Срочная
    const DEFAULT_WEIGHT = 0.2;

    const PRICE_OF_PACKAGE = 100;

    private $requestModel;
    private $tariffModel;
    private $locationModel;
    private $recipientModel;


    public function __construct()
    {
        $this->requestModel = new RequestModel();
        $this->tariffModel = new TariffModel();
        $this->locationModel = new LocationModel();
        $this->recipientModel = new RecipientModel();
    }

    public function getPrice(Shipment $shipment) : int
    {
        $order = $shipment->getCollection()->getOrder();
        $locationToName = $this->locationModel->getLocationToName($order);
        $codeRecipient = $this->recipientModel->getCodeRecipient($locationToName);

        $requestData = $this->getRequestData($codeRecipient);
        $requestParameters = $this->getRequestParameters();
        $tariffs = $this->requestModel->getTariffs($requestData, $requestParameters);

        $price = $this->tariffModel->getMinPrice($tariffs);
        $price += self::PRICE_OF_PACKAGE;

        return $price;
    }

    private function getRequestData(string $codeRecipient) : \stdClass
    {
        $fieldsData = [
            [
                "Key" => "SenderGeography",
                "Value" => self::CODE_SENDER,
                "ValueType" => "string",
            ],
            [
                "Key" => "RecipientGeography",
                "Value" => $codeRecipient,
                "ValueType" => "string",
            ],
            [
                "Key" => "TypeOfCargo",
                "Value" => self::TYPE_OF_CARGO,
                "ValueType" => "string",
            ],
            [
                "Key" => "Weight",
                "Value" => self::DEFAULT_WEIGHT,
                "ValueType" => "float",
            ],
            [
                "Key" => "Qty",
                "Value" => "1",
                "ValueType" => "int",
            ],
            [
                "Key" => "URGENCY",
                "Value" => self::URGENCY,
                "ValueType" => "string",
            ],
        ];

        $fields = [];

        foreach ($fieldsData as $fieldData) {
            $obj = new \stdClass();
            foreach ($fieldData as $fieldDataKey => $fieldDataVal) {
                $obj->$fieldDataKey = $fieldDataVal;
            }
            $fields[] = $obj;
        }

        $list = new \stdClass();
        $list->Key = "Destinations";
        $list->Fields = $fields;


        $data = new \stdClass();

        $data->Key = "Destinations";
        $data->List = $list;

        return $data;
    }

    private function getRequestParameters() : \stdClass
    {
        $list = new \stdClass();

        $list->Key = "ipaddress";
        $list->Value = $_SERVER["SERVER_ADDR"];
        $list->ValueType = "string";

        $parameters = new \stdClass();

        $parameters->Key = "Parameters";
        $parameters->List = $list;

        return $parameters;
    }

}