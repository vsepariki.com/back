<?
namespace Sale\Handlers\Delivery;

use Exception;

class TariffModel
{
    public function getMinPrice(array $tariffs) : int
    {
        $tariffs = $this->prepareTariffs($tariffs);
        $properTariff = $this->getTariffWithMinCost($tariffs);
        $cost = $properTariff["cost"];

        return $cost;
    }

    private function prepareTariffs(array $tariffs) : array
    {
        $result = [];

        foreach ($tariffs as $tariff) {
            $result[] = [
                "name" => $this->getTariffValue($tariff, "Service"),
                "cost" => $this->getTariffValue($tariff, "Total"),
                "urgency" => $this->getTariffValue($tariff, "UrgencyName"),
                "minPeriod" => $this->getTariffValue($tariff, "MinPeriod"),
                "maxPeriod" => $this->getTariffValue($tariff, "MaxPeriod"),
            ];
        }

        return $result;
    }

    private function getTariffValue(\stdClass $tariff, string $key) : string
    {
        $funcIsFieldKeyEqualArgKey = function (\stdClass $tariffField) use ($key) : bool {
            return ($tariffField->Key == $key) ? true : false;
        };
        $tariffFields = array_filter($tariff->Fields, $funcIsFieldKeyEqualArgKey);

        if(empty($tariffFields)){
            throw new Exception("Не удалось найти свойство тарифа доставки по коду свойства");
        }
        $tariffField = reset($tariffFields);

        return $tariffField->Value;
    }

    private function getTariffWithMinCost(array $tariffs) : array
    {
        $properTariff = reset($tariffs);

        foreach ($tariffs as $tariff) {
            if($tariff["cost"] < $properTariff["cost"]){
                $properTariff = $tariff;
            }
        }

        return $properTariff;
    }

}