<? namespace Sale\Handlers\Delivery;

require_once __DIR__."/DeliveryRussianPostModel.php";
require_once __DIR__."/DbCityIndexModel.php";
require_once __DIR__."/DeliveryCseModel.php";
require_once __DIR__."/TariffModel.php";
require_once __DIR__."/LocationModel.php";
require_once __DIR__."/RecipientModel.php";
require_once __DIR__."/RequestModel.php";

use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;
use Bitrix\Sale\Shipment;
use Exception;
use Bitrix\Currency\CurrencyManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

class CseHandler extends Base
{
    public static function getClassTitle() : string
    {
        return "Доставка курьером (КСЭ)";
    }

    protected function calculateConcrete(Shipment $shipment) : CalculationResult
    {
        $result = new CalculationResult();

       try{
            $deliveryCseModel = new DeliveryCseModel();
            $price = $deliveryCseModel->getPrice($shipment);
        }catch (Exception $e){
            $price = 0;
        }

        $result->setDeliveryPrice($price);

        return $result;
    }

    protected function getConfigStructure() : array
    {
        $currency = $this->currency;

        if (Loader::includeModule('currency')) {
            $currencyList = CurrencyManager::getCurrencyList();
            if (isset($currencyList[$this->currency]))
                $currency = $currencyList[$this->currency];
            unset($currencyList);
        }

        $result = array(
            "MAIN" => array(
                "TITLE" => Loc::getMessage("SALE_DLVR_HANDL_SMPL_TAB_MAIN"),
                "DESCRIPTION" => Loc::getMessage("SALE_DLVR_HANDL_SMPL_TAB_MAIN_DESCR"),
                "ITEMS" => array(

                    "CURRENCY" => array(
                        "TYPE" => "DELIVERY_READ_ONLY",
                        "NAME" => Loc::getMessage("SALE_DLVR_HANDL_SMPL_CURRENCY"),
                        "VALUE" => $this->currency,
                        "VALUE_VIEW" => $currency
                    ),

                    0 => array(
                        "TYPE" => "NUMBER",
                        "MIN" => 0,
                        "NAME" => Loc::getMessage("SALE_DLVR_HANDL_SMPL_DEFAULT")
                    )
                )
            )
        );

        return $result;
    }

}