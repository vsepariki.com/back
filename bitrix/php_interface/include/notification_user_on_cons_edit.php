<?php
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", ["EventChangeConsultation", "OnSetActive"]);

class EventChangeConsultation
{
	const IBLOCK_ID_CONSULT = 11;
	const SITE_CODE_ID = "s1";
	const EMAIL_EVENT_NAME = "NEW_ANSWER";

	public function OnSetActive(&$arFields)
	{
		foreach($arFields["PROPERTY_VALUES"] as $propId => $prop){
			if(self::getCodeValue($propId) === "IS_SEND_EMAIL"){

				$keys = array_keys($prop);
				$propertyValueId = $keys[0];
				$propertyValue = $prop[$propertyValueId]["VALUE"];

				// если установлена пометка "послать письмо"
				if($propertyValue == "Y"){

					// снимаем пометку о необходимости послать письмо
					$arFields["PROPERTY_VALUES"][$propId][$propertyValueId]["VALUE"] = "N";

					// шлем письмо
					self::notificationUserOnConsEdit($arFields);

					break;
				}

			}
		}
	}

	private static function notificationUserOnConsEdit(&$arFields)
	{
		if(!$GLOBALS['prod']){
			return true; // выход -> не продакшн
		}

		if(!$arFields["IBLOCK_ID"] == self::IBLOCK_ID_CONSULT){
			return true; // выход -> не Консультация
		}

		foreach($arFields["PROPERTY_VALUES"] as $propId => $prop){
			if(self::getCodeValue($propId) === "EMAIL"){
				$email = self::getPropertyValue($prop);
				break;
			}
		}

		$arEventFields = [
			"DATE" => date('Y/m/d H:i'),
			"QUESTION_TITLE" => $arFields["NAME"],
			"ANSWER" => $arFields["DETAIL_TEXT"],
		];

		if(empty($email)){
			return true; // выход -> не найден E-mail пользователя
		}
		$arEventFields["EMAIL"] = $email;

		return CEvent::Send(self::EMAIL_EVENT_NAME, self::SITE_CODE_ID, $arEventFields);
	}

	// Получение кода свойства
	private static function getCodeValue($propId = '')
	{
		if($propId === ''){return false;}

		$propInfoRes = CIBlockProperty::GetByID($propId);
		$propInfo = $propInfoRes->GetNext();

		return $propInfo['CODE'];
	}

	private static function getPropertyValue($prop){
		$keys = array_keys($prop);
		$propertyValueId = $keys[0];
		return $prop[$propertyValueId]["VALUE"];
	}

}