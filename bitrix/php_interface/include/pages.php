<?
function getCurPage()
{
	global $APPLICATION;
	$curPage = explode('?', $APPLICATION->GetCurPage());
	$curPage = reset($curPage);

	return $curPage;
}

function isHomePage()
{
	$curPage = getCurPage();
	return $curPage == "/";
}

function isInnerPage()
{
	return !isHomePage();
}

function isProductPage()
{
	return isCurUrlConsist('catalog') && isCurUrlConsist('html');
}

function isCurUrlConsist($entry)
{
	global $APPLICATION;
	return strpos($APPLICATION->GetCurUri(), $entry) !== false;
}
