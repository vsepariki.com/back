<?php
AddEventHandler("main", "OnBeforeEventAdd", "OnSaleDeliveryOrderSendEmail");
function OnSaleDeliveryOrderSendEmail (&$event, &$lid, &$arFields, &$message_id)
{
    if ($event=="SALE_NEW_ORDER") {

        if ($arFields['ORDER_ID'] > 0) {

            $order = CSaleOrder::GetByID($arFields['ORDER_ID']);

            $arFields["PLUS_FIELD_DELIVERY"] = getDelivery($order);
            $arFields["PLUS_FIELD_PAY_SYSTEM"] = getPaySystem($order);
            $arFields["PLUS_FIELD_COMMENT"] = getComment($order);
            $arFields["PLUS_FIELD_LOCATION"] = getLocation($order);
            $arFields["PLUS_FIELD_PHONE"] = getPhone($order);
		}

    }
}
/******************************************************************************************************/
function getDeliveryNameByCode($code){
	$deliveriesAr = \Bitrix\Sale\Delivery\Services\Manager::getActiveList();
	foreach($deliveriesAr as $delivery){
	    if($delivery["CODE"] == $code){
	    	return $delivery["NAME"];
	    }
	}
	return false;
}

function getDelivery($order){
	if(!empty($order["DELIVERY_ID"])){
		$deliveryCode = $order["DELIVERY_ID"];
		$deliveryName = getDeliveryNameByCode($deliveryCode);
		if($deliveryName){
			$deliveryName = str_replace(":", " -", $deliveryName);
			return "<strong>Способ доставки: </strong><br>".$deliveryName;
		}
	}
	return false;
}

function getPaySystem($order){
	if(!empty($order["PAY_SYSTEM_ID"])){
		$paySystemId = $order["PAY_SYSTEM_ID"];
		$paySystemAr = CSalePaySystem::GetByID($paySystemId);
		if(!empty($paySystemAr["NAME"])){
			$paySystemName = $paySystemAr["NAME"];
			$paySystemName = str_replace(":", " -", $paySystemName);
			return "<strong>Способ оплаты: </strong><br>".$paySystemName;
		}
	}
	return false;
}

function getComment($order){
	if(!empty($order["USER_DESCRIPTION"])){
		$comment = $order["USER_DESCRIPTION"];
		return "<strong>Комментарии к заказу: </strong><br>".$comment;
	}
	return false;
}

function getLocation($order){

	if($order["DELIVERY_ID"] == 3){ // если "Самовывоз из магазина в Санкт-Петербурге" (id = 3) -> адрес не выводим
		return "";
	}

	$location = [];
	$db_props = CSaleOrderPropsValue::GetOrderProps($order["ID"]);
	while ($arProps = $db_props->Fetch())
	{
		if($arProps["CODE"] == "LOCATION"){
			$locationValue = $arProps["VALUE"];
			$arLocs = CSaleLocation::GetByID($locationValue);
			if(!empty($arLocs["COUNTRY_NAME"])){
				$location[] = $arLocs["COUNTRY_NAME"];
			}
			if(!empty($arLocs["REGION_NAME"])){
				$location[] = $arLocs["REGION_NAME"];
			}
			if(!empty($arLocs["CITY_NAME_ORIG"])){
				$location[] = $arLocs["CITY_NAME_ORIG"];
			}
		}

		if($arProps["CODE"] == "CITY" && !empty($arProps["VALUE"])){
			$location[] = $arProps["VALUE"];
		}
		if($arProps["CODE"] == "ADDRESS" && !empty($arProps["VALUE"])){
			$location[] = $arProps["VALUE"];
		}

	}
	if(!empty($location)){
		$location = implode(", ", $location);
		return "<strong>Адрес доставки: </strong><br>".$location;
	}
	return false;
}

function getPhone($order){
	$db_props = CSaleOrderPropsValue::GetOrderProps($order["ID"]);
	while ($arProps = $db_props->Fetch())
	{
		if($arProps["CODE"] == "PHONE" && !empty($arProps["VALUE"])){
			$phone = $arProps["VALUE"];
			return "<strong>Телефон: </strong><br>".$phone;
		}
	}
	return false;
}

