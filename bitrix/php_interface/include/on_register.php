<?
AddEventHandler("main", "OnBeforeUserRegister", ["UserRegisterHandler", "onBeforeUserRegister"]);
AddEventHandler("main", "OnAfterUserAdd", ["UserRegisterHandler", "onAfterUserAdd"]);

class UserRegisterHandler
{
    const PATH_REGISTRATION_ACCEPTED = "/bitrix/logs/registrationAccepted/";
    const PATH_REGISTRATION_REJECTED = "/bitrix/logs/registrationRejected/";

    public function onBeforeUserRegister($args)
    {
        global $APPLICATION;
        $errorMessage = "Ошибка во время регистрации";

        // Если спам-робот заполнил поле которое обычные пользователи не видят
        // (название поля "NAME" - чтоб робот его обязательно заполнил)
        if(!empty($_REQUEST["NAME"])){
            $APPLICATION->ThrowException($errorMessage." (NAME)");
            self::saveRejectedRegistration($args);
            return false;
        }

        // Если не заполнено поле которое добавлено через JS
        // (возможно спам-робот не имеет JS-модуля)
        if(empty($_REQUEST["REG_REQUIRED"])){
            $APPLICATION->ThrowException($errorMessage." (REG_REQUIRED)");
            self::saveRejectedRegistration($args);
            return false;
        }

        return true;
    }


    public function onAfterUserAdd($args)
    {
        if(!$args["RESULT"]){
            return true;
        }

        self::saveAcceptedRegistration($args);

        return true;
    }

    private static function saveAcceptedRegistration($saveData)
    {
        self::saveRegistration(self::PATH_REGISTRATION_ACCEPTED, $saveData);
    }

    private static function saveRejectedRegistration($saveData)
    {
        self::saveRegistration(self::PATH_REGISTRATION_REJECTED, $saveData);
    }

    private static function saveRegistration($savePath, $saveData)
    {
        $logFileName = date("Y.m.d_H-i-s").".php";
        $savePath = $_SERVER["DOCUMENT_ROOT"].$savePath.$logFileName;

        $saveData = [
            "user_id" => $saveData["ID"],
            "login" => $saveData["LOGIN"],
            "name" => $saveData["NAME"],
            "last_name" => $saveData["LAST_NAME"],
            "email" => $saveData["EMAIL"],
            "user_ip" => $saveData["USER_IP"],
            "anti-spam_field-js_added" => !empty($_REQUEST["REG_REQUIRED"]) ? $_REQUEST["REG_REQUIRED"] : "",
            "anti-spam_field-should_not_be_filled" => !empty($_REQUEST["NAME"]) ? $_REQUEST["NAME"] : "",
        ];
        $saveData = "<? return [".var_export($saveData, true)."];";

        file_put_contents($savePath, $saveData);
    }

}


