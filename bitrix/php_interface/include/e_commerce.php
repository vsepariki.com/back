<?
function getProductsInOrderByOrderId($orderId)
{
    $arOrder = [];
    $arFilter = ["ORDER_ID" => $orderId];
    $arGroupBy = false;
    $arNavStartParams = false;
    $arSelectFields = [];

    $resourceBasket = CSaleBasket::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

    $arProducts = [];
    while ($arBasket = $resourceBasket->Fetch())
    {
        $arProducts[] = [
            "id" => $arBasket['PRODUCT_ID'],
            "name" => $arBasket['NAME'],
            "price" => (int)$arBasket['PRICE'],
            "quantity" => (int)$arBasket['QUANTITY']
        ];
    }


    return $arProducts;
}

function isAllCartProductsAvailable($cartProducts)
{
    $cartProductsId = [];
    foreach ($cartProducts as $cartProduct) {
        $cartProductsId[] = $cartProduct["id"];
    }

    $arOrder  = [];
    $arFilter = [
        "IBLOCK_ID" => IBLOCK_OFFERS_ID,
        "ID" => $cartProductsId,
        "<CATALOG_QUANTITY" => 0,
    ];
    $arGroupBy = false;
    $arNavStartParams  = [];
    $arSelectFields = [
        "IBLOCK_ID",
        "ID",
    ];
    $res_elements = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

    if($res_elements->Fetch()){
        return false;
    }

    return true;
}
