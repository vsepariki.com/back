<?
class EmptyFilterSotbitMetaData
{
    public function getMetaData()
    {
        $realCurPage = $GLOBALS["sotbitFilterResult"]["FORM_ACTION"];

        $query = "
            SELECT * 
            FROM `b_sotbit_seometa_chpu` as chpu 
            INNER JOIN `b_sotbit_seometa` as cond
            ON chpu.CONDITION_ID = cond.ID  
            WHERE `REAL_URL` = '".$realCurPage."'"
        ;

        global $DB;
        $result = $DB->Query($query);
        $result = $result->Fetch();

        if(!$result || empty($result["META"])){
            return false;
        }

        $metaData = unserialize($result["META"]);

        return $metaData;
    }

    public function setTitle($metaData)
    {
        if(!empty($metaData["ELEMENT_TITLE"])){
            global $sotbitSeoMetaTitle;
            $sotbitSeoMetaTitle = $metaData["ELEMENT_TITLE"];
        }
    }

    public function setKeywords($metaData)
    {
        if(!empty($metaData["ELEMENT_KEYWORDS"])){
            global $sotbitSeoMetaKeywords;
            $sotbitSeoMetaKeywords = $metaData["ELEMENT_KEYWORDS"];
        }
    }

    public function setDescription($metaData)
    {
        if(!empty($metaData["ELEMENT_DESCRIPTION"])){
            global $sotbitSeoMetaDescription;
            $sotbitSeoMetaDescription = $metaData["ELEMENT_DESCRIPTION"];
        }
    }

    public function setBreadcrumbTitle($metaData)
    {
        if(!empty($metaData["ELEMENT_BREADCRUMB_TITLE"])){
            global $sotbitSeoMetaBreadcrumbTitle;
            $sotbitSeoMetaBreadcrumbTitle = $metaData["ELEMENT_BREADCRUMB_TITLE"];
        }
    }

    public function setH1($metaData)
    {
        if(!empty($metaData["ELEMENT_PAGE_TITLE"])){
            global $sotbitSeoMetaH1;
            $sotbitSeoMetaH1 = $metaData["ELEMENT_PAGE_TITLE"];
        }
    }

    public function topDescription($metaData)
    {
        if(!empty($metaData["ELEMENT_TOP_DESC"])){
            global $sotbitSeoMetaTopDesc;
            $sotbitSeoMetaTopDesc = $metaData["ELEMENT_TOP_DESC"];
        }
    }

    public function bottomDescription($metaData)
    {
        if(!empty($metaData["ELEMENT_BOTTOM_DESC"])){
            global $sotbitSeoMetaBottomDesc;
            $sotbitSeoMetaBottomDesc = $metaData["ELEMENT_BOTTOM_DESC"];
        }
    }

}

function setEmptyFilterSotbitMetaData()
{
    global $isEmptyFilterResult;

    if(empty($isEmptyFilterResult)){
        return false;
    }

    $sotbit = new EmptyFilterSotbitMetaData;

    $metaData = $sotbit->getMetaData();

    $sotbit->setTitle($metaData);
    $sotbit->setKeywords($metaData);
    $sotbit->setDescription($metaData);
    $sotbit->setBreadcrumbTitle($metaData);
    $sotbit->setH1($metaData);
    $sotbit->topDescription($metaData);
    $sotbit->bottomDescription($metaData);

    return true;
}