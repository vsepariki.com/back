<?
class WebPHelper
{
	public function getOrCreateWebp($defaultPath)
	{
		$webpPath = $this->getWebpPath($defaultPath);

		if(!$this->isWebpPathExist($webpPath)){
			$this->saveWebPImage($defaultPath, $webpPath);
		}

		return $webpPath;
	}

	private function saveWebPImage($defaultPath, $webpPath)
	{
		$defaultPath = $_SERVER["DOCUMENT_ROOT"]."/".$defaultPath;
		$webpPath = $_SERVER["DOCUMENT_ROOT"]."/".$webpPath;

		if ($this->isImgPng($defaultPath)) {
			$img = imagecreatefrompng($defaultPath);
			imagepalettetotruecolor($img);
			imagealphablending($img, true);
			imagesavealpha($img, true);
		} elseif ($this->isImgJpeg($defaultPath)) {
			$img = imagecreatefromjpeg($defaultPath);
			imagepalettetotruecolor($img);
		} else {
			return false;
		}

		imagewebp($img, $webpPath, 100);

		//x00 webp generation fix
		$fpr = fopen($webpPath, "a+");
		fwrite($fpr, chr(0x00));
		fclose($fpr);

		imagedestroy($img);

		return true;
	}

	public function getWebpOrDefaultPath($defaultPath): string
	{
		$webpPath = $this->getWebpPath($defaultPath);

		if($this->isWebpPathExist($webpPath)){
			return $webpPath;
		}

		return $defaultPath;
	}

	private function isWebpPathExist($webpPath): bool
	{
		return file_exists($_SERVER["DOCUMENT_ROOT"]."/".$webpPath);
	}

	private function getWebpPath($defaultPath): string
	{
		return str_replace([".jpg", ".jpeg", ".png"], ".webp", strtolower($defaultPath));
	}

	private function isImgPng($path): bool
	{
		return exif_imagetype($path) == IMAGETYPE_PNG;
	}

	private function isImgJpeg($path): bool
	{
		return exif_imagetype($path) == IMAGETYPE_JPEG;
	}

}
