<?php

use Ajax\ChangeXmlAfter;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();

if($_SERVER['REMOTE_ADDR'] != "127.0.0.1") {
    $GLOBALS['prod'] = true;
}

if(strpos($_SERVER["HTTP_HOST"], "beta.vsepariki.com") !== false){
    $GLOBALS['beta_rem'] = true;
}

if(strpos($_SERVER["HTTP_HOST"], "vsepariki.loc") !== false){
    $GLOBALS['beta_loc'] = true;
}

/*********** Mobile_Detect  ***********/
include_once "include/Mobile_Detect.php";
$detect = new Mobile_Detect;
if($detect->isMobile()){
    define('IS_MOBILE', true);
}else{
    define('IS_MOBILE', false);
}
/*********** Mobile_Detect  ***********/

$curPage = $APPLICATION->GetCurPage();

define('WIGS_SECTION_ID', 16);
define('HATS_SECTION_ID', 19);
define('COVERS_SECTION_ID', 18);
define('SHINONY_SECTION_ID', 17);
define('TEST_SECTION_ID', 50);

define('IBLOCK_QUICK_BUY_ID', 8);
define('IBLOCK_PRODUCTS_ID', 9);
define('IBLOCK_OFFERS_ID', 10);
define('IBLOCK_PRODUCTS_ADD_TO_LISTING_ID', 12);

define('WIGS_PROP_BRAND_ELLEN_WILLE', 185);
define('WIGS_PROP_BRAND_NJ_CREATION', 256);

define('HIGHLOAD_BLOCK_ID_COLORS', 2);

define('INCLUDE_AREAS',  "/bitrix/templates/.default/include_areas/");

define("BRAND_NAME_ELLEN_WILLE", "Ellen Wille");
define("BRAND_NAME_NJ_CREATION", "NJ Creation");
define('DELIVERY_DAYS_WIGS_BRAND_NJ_CREATION', "20");
define('DELIVERY_DAYS_NOT_WIGS_BRAND_NOT_NJ_CREATION', "2 до 4");
define('ON_ORDER_WIGS_BRAND_NJ_CREATION',  "Под заказ: срок поставки на склад магазина <strong>от ".DELIVERY_DAYS_WIGS_BRAND_NJ_CREATION."</strong> рабочих дней");
define('ON_ORDER_NOT_WIGS_BRAND_NOT_NJ_CREATION',  "Под заказ: срок поставки на склад магазина <strong>от ".DELIVERY_DAYS_NOT_WIGS_BRAND_NOT_NJ_CREATION."</strong> недель");


include_once "include/new_order_mail.php";
include_once "include/on_register.php";
include_once "include/e_commerce.php";
include_once "include/notification_user_on_cons_edit.php";
include_once "include/sotbit_meta_data.php";
include_once "include/pages.php";
include_once "include/webp.php";


if (CModule::IncludeModule("iblock") && CModule::IncludeModule("catalog")) {
    $arFilterElements = [
        "IBLOCK_ID" => IBLOCK_PRODUCTS_ID,
        "ACTIVE" => "Y",
        "INCLUDE_SUBSECTIONS" => "Y"
    ];

    $arSelectElement = ["ID", "NAME"]; // Выбираем только необходимые поля

    $resElements = \CIBlockElement::GetList([], $arFilterElements, false, false, $arSelectElement);
    $totalArrProduct = [];
    $emptyArrProduct = [];
    while ($element = $resElements->Fetch()) {
        $totalArrProduct[] = $element;
    }

    foreach ($totalArrProduct as $product) {
        if (!empty($product['ID'])){
            $id = '';
            $arOptimalPrice = \CCatalogProduct::GetOptimalPrice($product['ID'], 1, [1, 2, 3, 4, 5], 'N', 's1');
            $discountPrice = $arOptimalPrice['RESULT_PRICE']['DISCOUNT_PRICE'];
            if ($arOptimalPrice['RESULT_PRICE']['DISCOUNT'] > 0){
                $id = $product['ID'];

                CIBlockElement::SetPropertyValuesEx($product['ID'], IBLOCK_PRODUCTS_ID, array(139 => $arOptimalPrice['RESULT_PRICE']['DISCOUNT_PRICE']));

            } else {
                CIBlockElement::SetPropertyValuesEx($product['ID'], false, IBLOCK_PRODUCTS_ID,  array(139 => ''));

            }

            if ($arOptimalPrice['RESULT_PRICE']['DISCOUNT'] == 0){
                $emptyArrProduct[] = $product['ID'];


            }

        }


    }


}



AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);
function _Check404Error(){
    if (defined('ERROR_404') && ERROR_404 == 'Y') {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php';
        include $_SERVER['DOCUMENT_ROOT'] . '/404.php';
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php';
    }
}

if(isHomePage()){
    include __DIR__. "/include/removeSystemAssets.php";
}

function blockInclude($section, $filename, $simpleInclude = true)
{
    $includePath  = '/bitrix/templates/.default/include_areas/';
    $includePath .= $section.'/'.$filename.'.php';

    global $APPLICATION;

    if($simpleInclude){
        include($_SERVER["DOCUMENT_ROOT"].$includePath);
    }else{
        $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => $includePath
            ),
            false
        );
    }

    return false;
}


function getPageWrapperClass()
{
    global $APPLICATION;
    $result = '';
    if($pageWrapperClass = $APPLICATION->GetPageProperty('page-wrapper-class')){
        $result = ' '.$pageWrapperClass;
    }
    return $result;
}

function getTruncatedDataForSeo($text, $charsLimit = 250)
{
    $text = strip_tags($text);
    $text = str_replace(array("\n", "\r", "\n\r", "\r\n", ), '', $text);

    $text = html_entity_decode($text);
    $text = trim($text);
    $text = preg_replace('/[ ]{2,}/', " ", $text);

    if(mb_strlen($text) > $charsLimit){
        $text = mb_substr($text, 0, $charsLimit, 'UTF-8');
        $position = mb_strrpos($text, ' ', 'UTF-8');
        $text = mb_substr($text, 0, $position, 'UTF-8');
    }

    $text = rtrim($text, '.,:-—');
    $text = trim($text);

    return $text;
}

// -> Срок поставки для Товаров
function getDeliveryDaysByProductId($productId)
{
    $productInfo  = getProductInfoByProductId($productId);
    $iblockId     = (int)$productInfo["IBLOCK_ID"];
    $deliveryDays = $productInfo["PROPERTY_DELIVERY_DAYS_VALUE"];

    if(!empty($deliveryDays)){ // если Срок поставки указан в св-ах Товара - вернуть и выйти
        return $deliveryDays;
    }

    // если Срок поставки не указан в св-ах Товара
    $mxResult = CCatalogSku::GetProductInfo($productId);
    if (is_array($mxResult)) // если это Торговое предложение => получить Срок поставки для основного товара
    {
        $productInfo = getProductInfoByProductId($mxResult["ID"]);
        if(!empty($productInfo["PROPERTY_DELIVERY_DAYS_VALUE"])){ // если Срок поставки у основного Товара указан
            $deliveryDays = $productInfo["PROPERTY_DELIVERY_DAYS_VALUE"];
        }else{ // иначе получить Срок поставки по умолчанию у основного Товара
            $deliveryDays = getDeliveryDaysByDefault($mxResult["IBLOCK_ID"], $mxResult["ID"]);
        }
    }
    else // если это основной товар => получить Срок поставки по умолчанию
    {
        $deliveryDays = getDeliveryDaysByDefault($iblockId, $productId);
    }

    return $deliveryDays;

}

function getProductInfoByProductId($productId){
    $arSelect   = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_DELIVERY_DAYS");
    $arFilter   = Array("ID" => $productId);
    $resProduct = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    $ob = $resProduct->GetNextElement();

    $arFields = false;
    if($ob){
        $arFields = $ob->GetFields();
    }

    return $arFields;
}

function getDeliveryDaysByDefault($iblockId, $productId){
    $db_props = CIBlockElement::GetProperty(
        $iblockId, $productId, Array(), Array("CODE" => "DELIVERY_DAYS")
    );
    $ar_props = $db_props->GetNext();
    $deliveryDays = $ar_props["DEFAULT_VALUE"];
    return $deliveryDays;
}
// <- Срок поставки для Товаров

function AddOrderProperty($propCode, $value, $orderId)
{
    if (!strlen($propCode)) {
        return false;
    }

    if (CModule::IncludeModule('sale')) {
        $db_vals = CSaleOrderPropsValue::GetList(array(), array('ORDER_ID' => $orderId, 'CODE' => $propCode));
        $arVals = $db_vals->Fetch();

        return CSaleOrderPropsValue::Update(
            $arVals['ID'],
            array(
                'NAME' => $arVals['NAME'],
                'CODE' => $arVals['CODE'],
                'ORDER_PROPS_ID' => $arVals['ORDER_PROPS_ID'],
                'ORDER_ID' => $arVals['ORDER_ID'],
                'VALUE' => $value,
            )
        );
    }
}

AddEventHandler("sale", "OnOrderSave", "OnOrderSaveHandler");
function OnOrderSaveHandler($orderId, $arFields, $arOrder)
{
    $orderNote = "";
    foreach($arOrder["BASKET_ITEMS"] as $basketItem)
    {
        $productInfoRes = CCatalogProduct::GetList(array(), array("ID" => $basketItem["PRODUCT_ID"]), false, array());
        $productInfo    = $productInfoRes->getNext();
        $quantityStore  = $productInfo["QUANTITY"]; // Количество товара на складе после покупки
        $quantityBasket = $basketItem["QUANTITY"]; // Количество товара в заказе
        $quantityBefore = $quantityStore + $quantityBasket; // Количество товара до покупки

        if($quantityBefore < 1){

            $productWigsBrand = getProductWigsBrandByProductId($basketItem["PRODUCT_ID"]);
            if($productWigsBrand == BRAND_NAME_NJ_CREATION){
                $deliveryDays = DELIVERY_DAYS_WIGS_BRAND_NJ_CREATION;
            }else{
                $deliveryDays = DELIVERY_DAYS_NOT_WIGS_BRAND_NOT_NJ_CREATION;
            }

            $color = false;
            if(!empty($basketItem["PROPS"]["COLOR"]["VALUE"])){
                $color = $basketItem["PROPS"]["COLOR"]["VALUE"];
            }
            else
            {
                $color = array();
                $res = CIBlockElement::GetProperty(IBLOCK_PRODUCTS_ID, $basketItem["PRODUCT_ID"], "sort", "asc", array("CODE" => "COLOR"));
                while ($ob = $res->GetNext()){
                    $color[] = $ob['VALUE'];
                }
                if(is_array($color) AND !empty($color)){
                    $color = implode(', ', $color);
                }
            }

            if($orderNote){$orderNote .= "<br>";}
            $orderNote .= $basketItem["NAME"].", + цвет (".$color."), срок поставки на склад магазина ".$deliveryDays." дней.";
        }

    }

    if($orderNote){
        addOrderProperty("DELIVERY_DAYS_ORDER", $orderNote, $orderId);
    }

    return true;
}

function num2word($num)
{
    // пришла строка
    if (!preg_match('/^[\d]+$/', $num)){
        return false;
    }

    $words = array('год', 'года', 'лет');

    $num = $num % 100;
    if ($num > 19) {
        $num = $num % 10;
    }
    switch ($num) {
        case 1: {
            return($words[0]);
        }
        case 2: case 3: case 4: {
        return($words[1]);
    }
        default: {
            return($words[2]);
        }
    }
}

function mb_lcfirst($str, $encoding = "UTF-8") {
    $first_letter = mb_strtolower(mb_substr($str, 0, 1, $encoding), $encoding);
    $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
    $str = $first_letter . $str_end;
    return $str;
}

function getProductByIdForSearchPage($productId)
{
    $arSelect = [
        "IBLOCK_ID",
        "ID",
        "NAME",
        "DETAIL_PAGE_URL",
        "DETAIL_TEXT",
        "PREVIEW_PICTURE"
    ];

    $arFilter = [
        "ID" => $productId,
        "ACTIVE " => "Y",
        "SECTION_GLOBAL_ACTIVE " => "Y",
        "IBLOCK_ID" => IBLOCK_PRODUCTS_ID
    ];

    $res = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
    $ob = $res->GetNext();

    if(!$ob){
        return false;
    }

    $productTitle = $ob["NAME"];
    $productUrl = $ob["DETAIL_PAGE_URL"];
    $productPhotoSrc = CFile::GetPath($ob["PREVIEW_PICTURE"]);
    $productDescription = $ob["~DETAIL_TEXT"];

    $body = '<a href="'.$productUrl.'"><img src="'.$productPhotoSrc.'"></a>';
    $body = '<p>'.$body.'</p>';
    $body .= $productDescription;

    return [
        "ITEM_ID" => $productId,
        "TITLE_FORMATED" => $productTitle,
        "BODY_FORMATED" => $body,
        "URL" => $productUrl,
    ];

}

function L($Message)
{
    if (is_array($Message)) {
        $Message = print_r($Message,1);
    }
    $file_path = $_SERVER['DOCUMENT_ROOT'].'/!log.txt';
    $handle = fopen($file_path, 'a+');
    @flock($handle, LOCK_EX);
    fwrite($handle, '['.date('d.m.Y H:i:s').'] '.$Message."\r\n");
    @flock($handle, LOCK_UN);
    fclose($handle);
}

function getProductWigsBrandByProductId($productId)
{
    $arOrder = [];
    $arFilter = ["ID" => $productId];
    $arGroupBy = false;
    $arLimit  = [];
    $arSelect = [
        "IBLOCK_ID",
        "ID",
        "NAME",
        "PROPERTY_BRAND",
    ];
    $resource = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arLimit, $arSelect);

    if(!$product = $resource->GetNext()){
        return false;
    }

    if($product["IBLOCK_ID"] == IBLOCK_OFFERS_ID){
        $baseProduct = CCatalogSku::GetProductInfo($productId);
        if (is_array($baseProduct)) {
            return getProductWigsBrandByProductId($baseProduct["ID"]);
        }
    }

    if($product["PROPERTY_BRAND_VALUE"]){
        return $product["PROPERTY_BRAND_VALUE"];
    }

    return false;
}

// Если Товар Парик и Бренд NJ Creation - срок поставки 20 дней, в остальных случаях 2 дня
function getDeliveryTextByProductId($productId)
{
    $productWigsBrand = getProductWigsBrandByProductId($productId);
    if($productWigsBrand == BRAND_NAME_NJ_CREATION){
        return ON_ORDER_WIGS_BRAND_NJ_CREATION;
    }

    return ON_ORDER_NOT_WIGS_BRAND_NOT_NJ_CREATION;
}

function isCurPageFilterResult()
{
    global $curPage;

    if(strpos($curPage, 'clear/apply')){
        return false;
    }

    if (!empty(strpos($curPage, "price-base"))){
        return false;
    }

    if(!empty($GLOBALS["sotbitFilterResult"]["FORM_ACTION"])) {
        $realCurUrl = $GLOBALS["sotbitFilterResult"]["FORM_ACTION"];
        if (!empty(strpos($realCurUrl, "filter"))){
            return true;
        }
    }

    if (!empty(strpos($curPage, "filter"))){
        return true;
    }

    return false;
}

function isSectionWigs($sectionId)
{
    return $sectionId == WIGS_SECTION_ID;
}

function isSectionHats($sectionId)
{
    return $sectionId == HATS_SECTION_ID;
}

function isSectionCovers($sectionId)
{
    return $sectionId == COVERS_SECTION_ID;
}

function isSectionShinony($sectionId)
{
    return $sectionId == SHINONY_SECTION_ID;
}

function filterCode($code)
{
    $codeExplode = explode('/', $code);
    $code = end($codeExplode);
    $codeExplode = explode('?', $code);
    $code = reset($codeExplode);

    return $code;
}

function getWigsProductsIdWithAvailableOffers()
{
    $arFilter = ["IBLOCK_ID" => IBLOCK_PRODUCTS_ID, "ACTIVE" => "Y", "IBLOCK_SECTION_ID" => $sectionId];
    $resource = \CIBlockElement::GetList([], $arFilter, false, false, ["IBLOCK_ID", "ID"]);
    $productsId = [];
    while($element = $resource->Fetch()){
        $productsId[] = $element["ID"];
    }

    $offers = \CCatalogSKU::getOffersList(
        $productsId,
        IBLOCK_PRODUCTS_ID,
        ["ACTIVE" => "Y", ">CATALOG_QUANTITY" => 0],
    );

    return array_keys($offers);
}


function getWigsProductsIdWithAvailableProductNotOffer()
{
    $arFilter = ["IBLOCK_ID" => IBLOCK_PRODUCTS_ID, "ACTIVE" => "Y", ">CATALOG_QUANTITY" => 0, "IBLOCK_SECTION_ID" => $sectionId];
    $resource = \CIBlockElement::GetList([], $arFilter, false, false, ["IBLOCK_ID", "ID"]);
    $productsId = [];
    $product_dilter_ids = [];
    while($element = $resource->Fetch()){
        $productsId[] = $element["ID"];
        $product_ids[$element["ID"]] = $element["ID"];
    }

    $offers = \CCatalogSKU::getOffersList(
        $productsId,
        IBLOCK_PRODUCTS_ID,
        ["ACTIVE" => "Y", ">CATALOG_QUANTITY" => 0],
    );
    //$offers = array_merge($product_ids, $offers);
    $offers_key = array_keys($offers);
    $product_key = array_keys($product_ids);
    $product_all = array_merge($offers_key, $product_key);
    return $product_all;
}

function isProductAvailableInRussia($arResult): bool
{
    if($arResult["PROPERTIES"]["COLLECTION_WIG"]["VALUE"] == "Prime Power"){return false;}
    if($arResult["PROPERTIES"]["COLLECTION_WIG"]["VALUE"] == "Pure Power 2015"){return false;}
    return true;
}

function pluralDeclension(int $count, string $form_1, string $form_234, string $form_others): string
{

    if(in_array($count, [11, 12, 13, 14])){
        return $form_others;
    }

    switch($count % 10){
        case 1:
            $result = $form_1; break;
        case 2:
        case 3:
        case 4:
            $result = $form_234; break;
        default:
            $result = $form_others;
    }

    return $result;
}

function getInterestingProductsId()
{
    $curPage = getCurPage();
    $arFilter = ["IBLOCK_ID" => IBLOCK_PRODUCTS_ADD_TO_LISTING_ID, "ACTIVE" => "Y", "PROPERTY_URL" => $curPage];
    $resource = \CIBlockElement::GetList([], $arFilter, false, false, ["IBLOCK_ID", "ID", "PROPERTY_PRODUCTS"]);
    $productsId = [];
    while($element = $resource->Fetch()){
        $productsId[] = $element["PROPERTY_PRODUCTS_VALUE"];
    }

    if(!$productsId){
        return false;
    }

    return $productsId;
}

function calculateReadingTime($text, $wordsPerMinute = 200) {
    // Разделяем текст на отдельные слова
    $words =  count(preg_split('/\s+/', strip_tags($text)));

    // Вычисляем ориентировочное время чтения
    $readingTime = ceil($words / $wordsPerMinute);

    return $readingTime;
}
